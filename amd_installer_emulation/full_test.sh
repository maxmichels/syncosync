#!/bin/bash -e
# Absolute path to this script. /home/user/bin/foo.sh
SCRIPT=$(readlink -f $0)
# Absolute path this script is in. /home/user/bin
SCRIPTPATH=$(dirname $SCRIPT)
cd $SCRIPTPATH

startpath=`pwd`

target='bullseye'

# Usage info
show_help() {
  cat <<EOF
Usage: ${0##*/}
Full test on a target (bullseye / buster)

    -t          target dir (defaults to "buster")
EOF
}

download_logs()
{   
    echo Downloading syncosync logs to ${1}
    scp  -o StrictHostKeyChecking=no -i robotkey -P 2222 root@localhost:/tmp/syncosync/syncosync.log ${1}/syncosync_alice.log
    scp  -o StrictHostKeyChecking=no -i robotkey -P 2223 root@localhost:/tmp/syncosync/syncosync.log ${1}/syncosync_bob.log
}

OPTIND=1 # Reset is necessary if getopts was used previously in the script.  It is a good idea to make this local in a function.
while getopts "t:h" opt; do
  case "$opt" in
  t) target=$OPTARG;;
  h)
    show_help
    exit 0
    ;;
  '?')
    show_help >&2
    exit 1
    ;;
  esac
done

if [ ! -d $target ]; then
    echo $target not existing
    exit 1
fi

res1=$(date +%s)
echo Starting full test

if [ -c /dev/kvm ]
 then
    echo kvm device found
 else
    banner "NO KVM!!!"
    echo Everything will be slow like hell!!!
 fi

echo removing old host keys
ssh-keygen -f ~/.ssh/known_hosts -R "[localhost]:2222" || echo host key for 2222 not found
ssh-keygen -f ~/.ssh/known_hosts -R "[localhost]:2223" || echo host key for 2223 not found
ssh-keygen -f "/home/sosuser/.ssh/known_hosts" -R "[localhost]:2222"  || echo host key for 2222 for sosuser not found
ssh-keygen -f "/home/sosuser/.ssh/known_hosts" -R "[localhost]:2223"  || echo host key for 2223 for sosuser not found

./stop_img.sh

./init_test_img.sh -t $target
./gen_testdrives.sh -t $target
./start_img.sh -t $target
date
echo "Waiting for image to come up"
#if [ -c /dev/kvm ]
#then
#    echo Sleeping for 30s
#    sleep 30
#else
#    echo We have no /dev/kvm, so we wait for 5 min
#    sleep 300
#fi
while ( ! echo quit | telnet 127.0.0.1 2222 2>/dev/null | grep Connected )
do
    sleep 3
    sshcheck=$((sshcheck+1))
    if [ $sshcheck -gt 100 ]
    then
        echo "Waited too long for ssh to come up"
        ./stop_img.sh
        exit 1
    fi
done    
echo "Waited for $sshcheck * 3 seconds for test_img sshd start"
date
sleep 10
./install_local_deb.sh
ssh-keygen -f ~/.ssh/known_hosts -R "[localhost]:2222" || echo host key for 2222 not found
ssh-keygen -f ~/.ssh/known_hosts -R "[localhost]:2223" || echo host key for 2223 not found
ssh-keygen -f "/home/sosuser/.ssh/known_hosts" -R "[localhost]:2222"  || echo host key for 2222 for sosuser not found
ssh-keygen -f "/home/sosuser/.ssh/known_hosts" -R "[localhost]:2223"  || echo host key for 2223 for sosuser not found

echo "Waiting for debian package to be started"
banner=`sshpass -p "foo" ssh -tt -p 2222 -o StrictHostKeyChecking=no foobar@localhost || true`
echo 
echo after this sshpass: "$banner"
banner=""
bannercheck=0
while [[ ! "$banner" == *"syncosync intranet - closed"* ]]
do 
    banner=`sshpass -p "foo" ssh -tt -p 2222 -o StrictHostKeyChecking=no foobar@localhost 2>&1 || true`
    echo banner is: "$banner"
    sleep 3
    bannercheck=$((bannercheck+1))
    if [ $bannercheck -gt 100 ]
    then
        echo "Waited too long for ssh banner"
        ./stop_img.sh
        exit 1
    fi
done
echo "Waited for $bannercheck * 3 seconds for syncsoync sshd start"
sleep 10
if [ ! -z "$DISPLAY" ]
then
    # shellcheck disable=SC1073
    robot -C on --skiponfailure knownfailure --xunit xunit.xml -d $target -v TARGET:$target robot_initial robot_cli robot_ui || { echo "errors in test" ; download_logs $target ; ./stop_img.sh ; exit 1; }
else
    # only with no DISPLAY we should also check if we are on a slow machine with no kvm
    if [ -c /dev/kvm ]
    then
        robot -C on --skiponfailure knownfailure --xunit xunit.xml -d $target -v HEADLESS:True  -v TARGET:$target robot_initial robot_cli robot_ui || { echo "errors in test" ; download_logs $target ;./stop_img.sh ; exit 1; }
    else
        robot -C on --skiponfailure knownfailure --xunit xunit.xml -d $target -v HEADLESS:True  -v TARGET:$target -v SLOW:True robot_initial robot_cli robot_ui || { echo "errors in test" ; download_logs $target ; ./stop_img.sh ; exit 1; }
    fi
fi
# now copy the logs to save them as artifacts
download_logs $target
./stop_img.sh

# Calculate the runtime
res2=$(date +%s)
dt=$((res2 - res1))
dd=$((dt/86400))
dt2=$((dt-86400*dd))
dh=$((dt2/3600))
dt3=$((dt2-3600*dh))
dm=$((dt3/60))
ds=$((dt3-60*dm))

LC_NUMERIC=C printf "Total runtime: %02d:%02d:%02d\n" $dh $dm $ds
