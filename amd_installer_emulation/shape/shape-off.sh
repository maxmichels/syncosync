#!/bin/bash
# old version
#tc qdisc del dev eth0 root
#tc qdisc add dev eth0 root handle 1:0 htb default 2
#tc class add dev eth0 parent 1:0 classid 1:1 htb rate 5Mbit
#tc qdisc add dev eth0 parent 1:1 sfq
#tc filter add dev eth0 parent 1:0 protocol ip u32 match ip dport 55056 0xffff flowid 1:1

# new version outgoing
tc qdisc del dev eth0 root

# new version incoming
tc qdisc del dev eth0 ingress
tc qdisc del dev ifb6682 root
ip link set dev ifb6682 down
ip link delete ifb6682
rmmod ifb
