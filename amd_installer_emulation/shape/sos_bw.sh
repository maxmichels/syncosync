#!/bin/bash
res1=$(date +%s%N)
oldin=`tc -s class show dev ifb6682 | grep Sent | awk  'BEGIN { ORS=" " }; { print $2 }'`
oldout=`tc -s class show dev eth0 | grep Sent | awk  '{ print $2 }'`
while true
do
    sleep 5
    in=`tc -s class show dev ifb6682 | grep Sent | awk  'BEGIN { ORS=" " }; { print $2 }'`
    out=`tc -s class show dev eth0 | grep Sent | awk  '{ print $2 }'`
    res2=$(date +%s%N)
    outmbit=$((((out-oldout)*8)/((res2-res1)/1000)))
    inmbit=$((((in-oldin)*8)/((res2-res1)/1000)))
    res1=$res2
    oldin=$in
    oldout=$out
    LC_NUMERIC=C printf "Out: % 3d Mbits/s   In: % 3d Mbits/s\n" $outmbit $inmbit
done
