#!/bin/bash
# old version
#tc qdisc del dev eth0 root
#tc qdisc add dev eth0 root handle 1:0 htb default 2
#tc class add dev eth0 parent 1:0 classid 1:1 htb rate 5Mbit
#tc qdisc add dev eth0 parent 1:1 sfq
#tc filter add dev eth0 parent 1:0 protocol ip u32 match ip dport 55056 0xffff flowid 1:1

# new version outgoing
tc qdisc add dev eth0 root handle 1a1a: htb default 1
tc class add dev eth0 parent 1a1a: classid 1a1a:142 htb rate 10000Mbit
tc qdisc add dev eth0 parent 1a1a:142 handle 2f1b: netem
tc filter add dev eth0 protocol ip parent 1a1a: prio 5 u32 match ip dst 0.0.0.0/0 match ip src 0.0.0.0/0 match ip dport 55056 0xffff flowid 1a1a:142

# new version incoming
modprobe ifb
ip link add ifb6682 type ifb
ip link set dev ifb6682 up
tc qdisc add dev eth0 ingress
tc filter add dev eth0 parent ffff: protocol ip u32 match u32 0 0 flowid 1a1a: action mirred egress redirect dev ifb6682
tc qdisc add dev ifb6682 root handle 1a1a: htb default 1
tc class add dev ifb6682 parent 1a1a: classid 1a1a:209 htb rate 10000Mbit
tc qdisc add dev ifb6682 parent 1a1a:209 handle 2ab2: netem
tc filter add dev ifb6682 protocol ip parent 1a1a: prio 5 u32 match ip dst 0.0.0.0/0 match ip src 0.0.0.0/0 match ip dport 55055 0xffff flowid 1a1a:209
