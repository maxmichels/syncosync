*** Settings ***
Documentation          Tests for Disaster Swap

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections


*** Test Cases ***

disaster swap
    [Documentation]               Default to Disaster Swap
                                  Switch Connection  alice
    ${content}                    Execute Command  sysstate.py -g
                                  Log   ${content}

    ${content}   ${stderr}        Execute Command    sysstate.py --disaster_swap -a sosadmin  return_stderr=True
                                  Log   ${content}
                                  Log   ${stderr}
    Should Contain                ${content}    Switching to Disaster Swap successful
    Should Contain                ${content}    Restored account: bob_ta1
    Check For File               /mnt/local/syncosync/bob_ta1/data/bob_ta1_0.data

switch back to default
    [Documentation]               Disaster Swap to default
                                  Switch Connection  alice
    ${content}                    Execute Command    sysstate.py --default -a sosadmin
    Should Contain               ${content}    successful
    Should Contain               ${content}    Restored account: ali_ta1
    Check For File               /mnt/local/syncosync/ali_ta1/data/ali_ta1_0.data


swap again and make persistent
    [Documentation]               persistent disaster swap from alice to bob
                                  Switch Connection  alice
    ${content}                    Execute Command  sysstate.py -g
                                  Log   ${content}

    ${content}   ${stderr}        Execute Command    sysstate.py --disaster_swap -a sosadmin  return_stderr=True
                                  Log   ${content}
                                  Log   ${stderr}
    Should Contain                ${content}    Switching to Disaster Swap successful
    Should Contain                ${content}    Restored account: bob_ta1
    Check For File               /mnt/local/syncosync/bob_ta1/data/bob_ta1_0.data
    ${content}   ${stderr}        Execute Command    sysstate.py --default --persistent  return_stderr=True
                                  Log   ${content}
                                  Log   ${stderr}
    Should Contain                ${content}    Switching to Default successful
    Check For File               /mnt/local/syncosync/bob_ta1/data/bob_ta1_0.data

reswap
     ${content}   ${stderr}        Execute Command    sysstate.py --disaster_swap -a sosadmin  return_stderr=True
                                  Log   ${content}
                                  Log   ${stderr}
    Should Contain                ${content}    Switching to Disaster Swap successful
    Should Contain                ${content}    Restored account: ali_ta1
    Check For File               /mnt/local/syncosync/ali_ta1/data/ali_ta1_0.data
    ${content}   ${stderr}        Execute Command    sysstate.py --default --persistent  return_stderr=True
                                  Log   ${content}
                                  Log   ${stderr}
    Should Contain                ${content}    Switching to Default successful
    Check For File               /mnt/local/syncosync/ali_ta1/data/ali_ta1_0.data
