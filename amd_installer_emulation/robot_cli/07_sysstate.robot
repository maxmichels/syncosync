*** Settings ***
Documentation          sysstate switch testing

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections


*** Test Cases ***
check for sysstate
    [Documentation]               Check for actual sysstate
                                  Switch Connection  alice  
    ${content}                    Execute Command    configbackup.py -b
    Should Contain                ${content}         backup successful to file 
    ${match} 	${filepath} =    Should Match Regexp  ${content}    .*to file (.*)
                                  SSHLibrary.File Should Exist  ${filepath}
    ${content}                    Execute Command    sysstate.py --default -c '${filepath}'
    
