*** Settings ***
Documentation          Tests for Disaster Swap

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections

*** Variables ***
${CLI_HELP_PATH}      ../syncosync-manuals/${LANG}/syncosync_cli_commands/help_raw_text

*** Test Cases ***

get help text
    [Documentation]               Get help texts for all cli commands
                                  Switch Connection  alice
    ${content}                    Execute Command  configbackup.py -h
    Create File                 ${CLI_HELP_PATH}/configbackup.raw   ${content}                            
    ${content}                    Execute Command  drivemanager.py -h
    Create File                 ${CLI_HELP_PATH}/drivemanager.raw   ${content}                            
    ${content}                    Execute Command  factory_reset.py -h
    Create File                 ${CLI_HELP_PATH}/factory_reset.raw   ${content}                            
    ${content}                    Execute Command  hostname.py -h
    Create File                 ${CLI_HELP_PATH}/hostname.raw   ${content}                            
    ${content}                    Execute Command  mail.py -h
    Create File                 ${CLI_HELP_PATH}/mail.raw   ${content}                            
    ${content}                    Execute Command  nameserver.py -h
    Create File                 ${CLI_HELP_PATH}/nameserver.raw   ${content}                            
    ${content}                    Execute Command  nicconfig.py -h
    Create File                 ${CLI_HELP_PATH}/nicconfig.raw   ${content}                            
    ${content}                    Execute Command  remote_recovery.py -h
    Create File                 ${CLI_HELP_PATH}/remote_recovery.raw   ${content}                            
    ${content}                    Execute Command  remotehost.py -h
    Create File                 ${CLI_HELP_PATH}/remotehost.raw   ${content}                            
    ${content}                    Execute Command  sosaccounts.py -h
    Create File                 ${CLI_HELP_PATH}/sosaccounts.raw   ${content}                            
    ${content}                    Execute Command  sosadmin_passwd.py -h
    Create File                 ${CLI_HELP_PATH}/sosadmin_passwd.raw   ${content}                            
    ${content}                    Execute Command  sosconfig.py -h
    Create File                 ${CLI_HELP_PATH}/sosconfig.raw   ${content}                            
    ${content}                    Execute Command  soshousekeeper.py -h
    Create File                 ${CLI_HELP_PATH}/soshousekeeper.raw   ${content}                            
    ${content}                    Execute Command  soskey.py -h
    Create File                 ${CLI_HELP_PATH}/soskey.raw   ${content}                            
    ${content}                    Execute Command  soskeyx.py -h
    Create File                 ${CLI_HELP_PATH}/soskeyx.raw   ${content}                            
    ${content}                    Execute Command  sshdmanager.py -h
    Create File                 ${CLI_HELP_PATH}/sshdmanager.raw   ${content}                            
    ${content}                    Execute Command  syncstat.py -h
    Create File                 ${CLI_HELP_PATH}/syncstat.raw   ${content}                            
    ${content}                    Execute Command  sysstate.py -h
    Create File                 ${CLI_HELP_PATH}/sysstate.raw   ${content}                            
    ${content}                    Execute Command  timezone.py -h
    Create File                 ${CLI_HELP_PATH}/timezone.raw   ${content}                            
    ${content}                    Execute Command  trafficshape.py -h
    Create File                 ${CLI_HELP_PATH}/trafficshape.raw   ${content}                            

