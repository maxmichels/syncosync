*** Settings ***
Documentation          Test miscenalleous config stuff

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections

*** Test Cases ***

start default mode
    [Documentation]               Start default no sync mode
                                  Switch Connection  alice  
    ${stdout}                     Execute Command   sysstate.py --default_no_sync
    Should Contain                ${stdout}       successful
                                  Switch Connection  bob  
    ${stdout}                     Execute Command   sysstate.py --default_no_sync
    Should Contain                ${stdout}       successful

add a nameserver
    [Documentation]               Add a secondary nameserver
                                  Switch Connection  alice  
    ${stdout}                     Execute Command   nameserver.py -s '{"second_nameserver": "8.8.8.8"}'
    ${rc}=                        Execute Command   grep "nameserver 8.8.8.8" /etc/resolv.conf      return_stdout=False      return_rc=True
    Should Be Equal               ${rc}             ${0}
    ${content}                    Execute Command   nameserver.py -g
    Should Contain                ${content}        8.8.8.8

change hostname
    [Documentation]               Change the hostname
                                  Switch Connection  alice  
    ${stdout}                     Execute Command   hostname.py -s '{"hostname": "testit"}'
    ${rc}=                        Execute Command   grep "testit" /etc/hostname      return_stdout=False      return_rc=True
    Should Be Equal               ${rc}             ${0}
    ${content}                    Execute Command   hostname.py -g
    Should Contain                ${content}        testit
    ${stdout}                     Execute Command   hostname.py -s '{"hostname": "alice"}'

get the network interfaces
    [Documentation]               Get the network interfaces list
                                  Switch Connection  alice  
    ${stdout}                     Execute Command   nicconfig.py -g
    Should Contain                ${stdout}       eth0

check if a configuration backup exists
    [Documentation]               We have changed the systype, so there should be at least one backup
                                  Switch Connection  alice  
    ${content}                    Execute Command   ls -1 -t /mnt/local/syncosync/syncosync/configbackup/sosconf_*.gpg | head -1
    Should Contain                ${content}        sosconf

# change sosadmin passwort
#     [Documentation]               Change the password of sosadmin to joshua
#                                   Switch Connection  alice
#                                   Execute Command  sosadmin_passwd.py -s '{"password": "$6$okzrR3cS3nzyCGGP$1xUjHqJlV71VMg2uj741drYLJrIp18Begv4ivoeBrt8r/YPrSg9/n60kQe45zh4MdmcZCqInB1nRpVjhafvCS0"}'
#                                   Open Connection   localhost  port=2222  timeout=60
#     Run Keyword If 	${SLOW} == True  BuiltIn.Sleep  40
#                                   Login  sosadmin  password=joshua  delay=10  allow_agent=False  look_for_keys=False
#     Run Keyword If 	${SLOW} == True  BuiltIn.Sleep  30
#     ${output}=                    Execute Command   echo Hello SSHLibrary!
#     Should Be Equal               ${output}          Hello SSHLibrary!
#                                   Close Connection
#                                   Switch Connection  alice 
#                                   Execute Command  sosadmin_passwd.py -s '{"password": "$6$6v4Y5NaXpVdqfBz7$tWyOfO1f2UAigVQyXPFeeFxvsk8H4ootwNUG/LYvhm4Hd43ewGk6lKxQ3M9JwTeDKH5HyCAAy5GVuGTS31qz./"}'
    