# amd64 Installer and Emulation

The emulation allows easy testing and image preparation without influencing the developer's host.

## Setup:

If you want your own public key to be added to the images (for testing and development!) copy it inside this directory with the name: developer_key.pub

Now you should have all prerequisite stuff installed. As we try to have the gitlab CI/CD chain always in working condition, take all prerequisites from the dockerfile there:

https://gitlab.com/syncosync/syncosync-cicd-containers/-/blob/master/sos-qemu-runner-docker/Dockerfile
(run them as root, you do not have to add groups and users)

The main scripts here are: 

 * init_base_img.sh: This downloads the debian buster netinst.iso and bulds a qemu syncosync.qcow image from that, using the preseed.cfg file.

 * init_test.img.sh: This takes the syncosync.qcow image, adds ssh keys, caches this sos_test.qcow and derices the alice.qcow and bob.qcow images from there.

 * gen_testdrives.sh creates empty images to be mounted to the qemus for testing.

     - alice.img, bob.img - These are really plain empty images and should be used for all run tests except disksetup testing
     - linux.img - a simple ext3 formatted partition on it
     - dos.img - an image containing a dos partition
     - epmty.img - a plain empty image

The linux, epmty and dos image are useful to test ui and core presentation of non sos disks at installation time.

Now use `start_img.sh` to start alice and bob. Login to alice with `ssh -p 2222 sosadmin@localhost` and to bob via port 2223. Password is sosadmin.
Better login as root with your developer key.

Note: at this point, you have already the repository for syncosync installed, but the packet isn't installed yet. You can now either install the latest package from the repo with `apt update && apt install syncosync` or install a package you have build on your side with `install_local_deb.sh`

If you want to push your changes to master, please run `full_test.sh` before and check if all robot test cases work fine.

## Testing

there are many robot tests. CI/CD uses them for a full test (see full_test.sh)
With an empty image and fresh installed test_img, you can use robot_initial
robut_ui and robot_cli are idempotent you can use them regulary
If a test fails read the logs (firefox log.html) and understand what happened.
