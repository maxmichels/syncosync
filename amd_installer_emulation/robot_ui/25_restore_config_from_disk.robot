*** Settings ***
Documentation          Restore Configuration from Disk

Resource               ../syncosync-keywords.robot

Library                SeleniumLibrary
Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections

*** Variables ***
${URL ALICE}           http://localhost:5555/en-US/
${BROWSER}             Chrome
${DELAY}               1
${TIMEOUT}             20

*** Test Cases ***

create configbackup
                                Configbackup Alice

reset system
    [Documentation]             Reset the system and restart syncosync service
                                Switch Connection  alice
    ${content}                  Execute Command  factory_reset.py -r -y
                                Log   ${content}
                                Execute Command  service syncosync restart
    ${content}                  Wait For State   Setup Disks

Valid Login
    ${alice_win}                Open Chrome          ${URL ALICE}
                                Set Selenium Speed   ${DELAY}
                                Set Selenium Timeout  ${TIMEOUT}
                                Page Should Contain  Login
                                Input Text           password  sosadmin
                                Click Button         Login
                                Log Location
                                Page Should Contain Element  xpath: //button[@id="logout-button"]

Disk Setup  
                                Page Should Contain  Disk Setup
                                Page Should Contain  This box is in factory state
                                Page Should Contain  QEMU HARDDISK
                                Page Should Contain  Select a syncosync volume group
                                Capture Page Screenshot   ${SCREENSHOT_PATH}/disksetup_with_disk_to_select.png


Select disk
                                Select Checkbox      xpath: //*[@id='select[0:0:1:0]']
                                Capture Page Screenshot   ${SCREENSHOT_PATH}/disksetup_selected_disk.png
                                Click Button         Restore Configuration from selected

Start config restore
                                Page Should Contain  syncosync admin password for configuration restore needed
                                Input Text           password  sosadmin
                                Capture Page Screenshot   ${SCREENSHOT_PATH}/select_disk_password_dialog.png
                                Click Button         Submit

Config succsessfully restored
                                Wait Until Page Contains  Configuration successfully restored
                                Capture Page Screenshot   ${SCREENSHOT_PATH}/configuration_sucessfully_restored.png
                                Click Button          OK


Default Page   
                                Wait Until Page Contains  Accounts
                                Page Should Contain       ali_ta1