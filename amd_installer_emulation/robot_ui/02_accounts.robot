*** Settings ***
Documentation          Account Manipulation

Resource               ../syncosync-keywords.robot

Library                SeleniumLibrary
Suite Setup            Open WebUI And SSH
Suite Teardown         Close WebUI And SSH


*** Test Cases ***

UI Login
    Switch Browser       alice
    Valid Login
    Switch Browser       bob
    Valid Login

Add Account ali_ta1 on alice
    Switch Browser       alice
    Click Element       addAccount
    Wait Until Element Is Visible  addAccountForm
    Capture Page Screenshot   ${SCREENSHOT_PATH}/account_add_empty.png
    #Wait Until Keyword Succeeds  2min  5s  Element Should Contain        xpath: //form[@id="addAccount"]    Add Account
    Input Text           xpath: //input[@id="accountname"]    ali_ta1
    Input Text           xpath: //input[@id="longName"]    Alice Test Account 1
    Wait Until Element Is Visible   passwordOrPubKeyRequiredAlert
    ${fingerprint}                Run                ssh-keygen -l -E md5 -f ali_ta1.pub | awk '{ print substr($2,5) }'
    ${content}           Run   ls -1 -t -d $PWD/ali_ta1.pub
    Choose File          xpath: //input[@id="sshKey"]   ${content}
    Wait Until Page Contains  ${fingerprint}
    Input Text           xpath: //input[@id="password"]    Joshua20
    Input Text           xpath: //input[@id="passwordCheck"]    Joshua20
    Input Text           xpath: //input[@id="mailAddress"]    ali_ta1@example.org

    Wait Until Element Is Enabled  xpath: //button[@type="submit"]
    Capture Page Screenshot   ${SCREENSHOT_PATH}/account_add_filled.png
    Click Element        xpath: //button[@type="submit"]
    Wait Until Element Is Not Visible  addAccountForm
    Wait Until Element Is Visible  ali_ta1_drop
    Click Element                ali_ta1_drop
    Wait Until Page Contains  Alice Test Account 1
    ${present}             Run Keyword And Return Status    Element Should Be Visible   ali_ta1_details
    Run Keyword If       ${present}     Click Element        ali_ta1_drop

Mod Account ali_ta1 on alice
    [Documentation]      Modify account ali_ta1
    Switch Browser       alice
    Click Element        ali_ta1_drop
    Click Element        ali_ta1_modAccount
    Wait Until Element Is Visible  modAccountForm
    Capture Page Screenshot   ${SCREENSHOT_PATH}/account_mod.png
    #Wait Until Keyword Succeeds  2min  5s  Element Should Contain  xpath: //form[@id="modAccount"]    Modify Account
    Input Text           xpath: //input[@id="longName"]    ALICE TEST ACCOUNT One
    Click Element        xpath: //button[@type="submit"]
    Wait Until Page Contains  ALICE TEST ACCOUNT One
    ${present}             Run Keyword And Return Status    Element Should Be Visible   ali_ta1_details
    Run Keyword If       ${present}     Click Element        ali_ta1_drop

# Check Mod Account Without Password Impossible If Key Deleted
#     [Documentation]      Try modifying account ali_ta1 by deleting the SSH key set. This should not be possible
#     Click Element        ali_ta1_drop
#     Click Element        ali_ta1_modAccount
#     Wait Until Element Is Visible  modAccountForm
#     Input Text           xpath: //input[@id="longName"]    ALICE TEST ACCOUNT One Impossible
#     Click Element        xpath: //i[@id="deleteSshKeyButton"]
#     Wait Until Element Is Visible   passwordOrPubKeyRequiredAlert
#     Element Should be Disabled    id=submitEditForm
#     Click Element        xpath: //button[@id="cancelEditForm"]
#     ${present}             Run Keyword And Return Status    Element Should Be Visible   ali_ta1_details
#     Run Keyword If       ${present}     Click Element        ali_ta1_drop

Add Account ali_ta2 on alice 
    Switch Browser       alice
    Click Element       addAccount
    Wait Until Element Is Visible  addAccountForm
    #Wait Until Keyword Succeeds  2min  5s  Element Should Contain        xpath: //form[@id="addAccount"]    Add Account
    Input Text           xpath: //input[@id="accountname"]    ali_ta2
    Input Text           xpath: //input[@id="longName"]    Da Robot Made It
    Wait Until Element Is Visible   passwordOrPubKeyRequiredAlert
    Input Text           xpath: //input[@id="password"]    Joshua20
    Input Text           xpath: //input[@id="passwordCheck"]    Joshua20
    Wait Until Element Is Enabled  xpath: //button[@type="submit"]
    Click Element        xpath: //button[@type="submit"]
    Wait Until Element Is Not Visible  addAccountForm
    Wait Until Element Is Visible  ali_ta2_drop
    Click Element                ali_ta2_drop
    Wait Until Page Contains  Da Robot Made It
    ${present}             Run Keyword And Return Status    Element Should Be Visible   ali_ta2_details
    Run Keyword If       ${present}     Click Element        ali_ta2_drop

Delete Account
    [Documentation]      Delete account ali_ta2
    Switch Browser       alice
    Wait Until Element Is Visible  ali_ta2_drop
    Capture Page Screenshot   ${SCREENSHOT_PATH}/account_list.png
    Click Element                ali_ta2_drop
    Capture Page Screenshot   ${SCREENSHOT_PATH}/account_drop.png
    Wait Until Element Is Visible  ali_ta2_delAccount
    Click Element        ali_ta2_delAccount   
    Wait Until Element Is Visible  xpath: //button[@id="delConfirm"]
    Capture Page Screenshot   ${SCREENSHOT_PATH}/account_del_confirmation.png
    Click Element        xpath: //button[@id="delConfirm"]
    Wait Until Page Does Not Contain  ali_ta2

Add Account bob_ta1 on bob 
    Switch Browser       bob
    Click Element       addAccount
    Wait Until Element Is Visible  addAccountForm
    #Wait Until Keyword Succeeds  2min  5s  Element Should Contain        xpath: //form[@id="addAccount"]    Add Account
    Input Text           xpath: //input[@id="accountname"]    bob_ta1
    Input Text           xpath: //input[@id="longName"]    Bob Test Account 1
    Wait Until Element Is Visible   passwordOrPubKeyRequiredAlert
    Input Text           xpath: //input[@id="password"]    Bobta1pwd
    Input Text           xpath: //input[@id="passwordCheck"]    Bobta1pwd
    Wait Until Element Is Enabled  xpath: //button[@type="submit"]
    Click Element        xpath: //button[@type="submit"]
    Wait Until Element Is Not Visible  addAccountForm
    Wait Until Element Is Visible  bob_ta1_drop
    Click Element               bob_ta1_drop
    Wait Until Page Contains  Bob Test Account 1
    ${present}             Run Keyword And Return Status    Element Should Be Visible   bob_ta1_details
    Run Keyword If       ${present}     Click Element        bob_ta1_drop


UI Logout
    Switch Browser       alice
    Valid Logout
    Switch Browser       bob
    Valid Logout
