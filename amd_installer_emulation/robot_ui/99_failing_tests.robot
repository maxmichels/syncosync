*** Settings ***
Documentation          Tests known to fail now: FIX THEM SOON!

Resource               ../syncosync-keywords.robot

Library                SeleniumLibrary

*** Test Cases ***

UI Login
    Open Connection And Log In
    Open Chrome    ${URL_ALICE}   alice
    Switch Browser       alice
    Valid Login


UI Logout
    [Tags]               knownfailure
    Switch Browser       alice
    Valid Logout
    Close WebUI And SSH
