*** Settings ***
Documentation          Setup: System

Resource               ../syncosync-keywords.robot

Library                SeleniumLibrary
Suite Setup            Open WebUI And SSH
Suite Teardown         Close WebUI And SSH

*** Test Cases ***

UI Login
    Switch Browser       alice
    Valid Login

Setup
    Setup

Admin Password
    [Documentation]    Change admin password to joshua
    Switch Connection  alice  
    Click Element      system_setup
    Capture Page Screenshot   ${SCREENSHOT_PATH}/system_setup.png
    Click Element      admin_password_tab
    Input Text         xpath: //input[@id="password"]    joshua
    Input Text         xpath: //input[@id="passwordCheck"]    joshua
    Wait Until Element Is Enabled   xpath: //button[@type="submit"]
    Click Element        xpath: //button[@type="submit"]
    Capture Page Screenshot   ${SCREENSHOT_PATH}/admin_password_setup.png
    Leave Setup

UI Logout For Relogin
    Valid Logout

UI Login Again
    Valid Login       password=joshua
    Setup

Admin Password Reset
    [Documentation]    Change admin password to sosadmin
    Click Element      system_setup
    Click Element      admin_password_tab
    Input Text         xpath: //input[@id="password"]    sosadmin
    Input Text         xpath: //input[@id="passwordCheck"]    sosadmin
    Wait Until Element Is Enabled   xpath: //button[@type="submit"]
    Click Element        xpath: //button[@type="submit"]

Timezone
    [Documentation]    Change Timezone
    Click Element      system_setup
    Click Element      timezone_tab
    Select From List By Value   xpath: //select[@name="timezone_select"]    Europe/Berlin
    Click Element        xpath: //button[@type="submit"]
    Wait For Result    timezone.py -g   \"timezone\": \"Europe/Berlin\"
    Capture Page Screenshot   ${SCREENSHOT_PATH}/timezone_setup.png

Leave Setup
    Leave Setup

Download Configbackup
    Configbackup Alice

UI Logout
    Valid Logout
    
    

