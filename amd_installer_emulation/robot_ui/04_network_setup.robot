*** Settings ***
Documentation          Setup: Networking

Resource               ../syncosync-keywords.robot

Library                SeleniumLibrary
Suite Setup            Open WebUI And SSH
Suite Teardown         Close WebUI And SSH

*** Test Cases ***

UI Login
    Switch Browser       alice
    Switch Connection  alice
    Valid Login

Setup
    Setup
    Capture Page Screenshot   ${SCREENSHOT_PATH}/setup.png


Hostname
    [Documentation]    Change Hostname from UI
    Click Element      network_setup
    Capture Page Screenshot   ${SCREENSHOT_PATH}/network_setup.png
    Click Element      hostname_tab
    Input Text         xpath: //input[@id="hostname"]    caroline
    Click Element        xpath: //button[@type="submit"]
    Wait For Result    hostname.py -g   \"hostname\": \"caroline\"
    Click Element      hostname_tab
    Input Text         xpath: //input[@id="hostname"]    alice
    Click Element        xpath: //button[@type="submit"]
    Wait For Result    hostname.py -g   \"hostname\": \"alice\"
    Capture Page Screenshot   ${SCREENSHOT_PATH}/hostname_setup.png


Nameserver
    [Documentation]    Change a nameserver from UI
    Click Element      network_setup
    Click Element      nameserver_tab
    Input Text         xpath: //input[@id="firstNameserver"]    8.8.8.8
    Click Element        xpath: //button[@type="submit"]
    Wait For Result    nameserver.py -g   \"first_nameserver\": \"8.8.8.8\"
    Click Element      nameserver_tab
    Input Text         xpath: //input[@id="secondNameserver"]    9.9.9.9
    Click Element        xpath: //button[@type="submit"]
    Wait For Result    nameserver.py -g   \"second_nameserver\": \"9.9.9.9\"
    Capture Page Screenshot   ${SCREENSHOT_PATH}/nameserver_setup.png

    # Clear Element Text  xpath: //input[@id="secondNameserver"]
    # Clear Element Text  xpath: //input[@id="firstNameserver"]
    # Click Element        xpath: //button[@type="submit"]
    # Wait For Result    nameserver.py -g   \"first_nameserver\": \"\"


Switch DHCP Off
    [Documentation]     Switch DHCP off
    Click Element      network_setup
    Click Element      interfaces_tab
    Element Should Be Disabled  xpath: //button[@type="submit"]
    Click Element       eth0_v4Dhcp
    Element Should Be Enabled   xpath: //button[@type="submit"]
    Click Element       xpath: //button[@type="submit"]



Remote SSH Port
    [Documentation]    Change a remote SSH Port
    Click Element      network_setup
    Click Element      remote_ssh_tab
    Input Text         xpath: //input[@id="remote_listening_port"]    60060
    Click Element        xpath: //button[@type="submit"]
    Wait For Result    sshdmanager.py -g   \"port\": 60060
    Click Element      remote_ssh_tab
    Input Text         xpath: //input[@id="remote_listening_port"]    55055
    Click Element        xpath: //button[@type="submit"]
    Wait For Result    sshdmanager.py -g   \"port\": 55055
    Capture Page Screenshot   ${SCREENSHOT_PATH}/remote_ssh_port_setup.png


Interfaces
    [Documentation]     Nework interfaces (not changing, just looking)
    Click Element      network_setup
    Click Element      interfaces_tab
    Page Should Contain   Network Interfaces Configuration
    Page Should Contain   eth0
    Capture Page Screenshot   ${SCREENSHOT_PATH}/interfaces_setup.png

Leave Setup
    Switch Browser       alice
    Leave Setup

UI Logout
    Switch Browser       alice
    Valid Logout
    
    

