*** Settings ***
Documentation          Setup: Sync

Resource               ../syncosync-keywords.robot

Library                SeleniumLibrary
Suite Setup            Open WebUI And SSH
Suite Teardown         Close WebUI And SSH

*** Test Cases ***

UI Login
    Switch Browser       alice
    Valid Login

Setup
    Setup

Change Shaping Bitrates
    [Documentation]      Open Traffic Shaping
    Switch Connection    alice
    Click Element        sync_setup
    Capture Page Screenshot   ${SCREENSHOT_PATH}/sync_setup.png
    Click Element        trafficshaping_tab
    Input Text           xpath: //input[@id="bandwithUpDay"]    1000
    Input Text           xpath: //input[@id="bandwithDownDay"]    2000
    Input Text           xpath: //input[@id="bandwithUpNight"]    3000
    Input Text           xpath: //input[@id="bandwithDownNight"]    4000
    Click Element        xpath: //button[@type="submit"]
    Wait For Result      trafficshape.py -g   \"in_day\": 2000
    ${content}           Execute Command  trafficshape.py -g
    Should Contain       ${content}   \"out_day\": 1000 
    Should Contain       ${content}   \"out_night\": 3000 
    Should Contain       ${content}   \"in_night\": 4000 
    Input Text           xpath: //input[@id="bandwithUpDay"]    10000
    Input Text           xpath: //input[@id="bandwithDownDay"]   10000
    Input Text           xpath: //input[@id="bandwithUpNight"]    10000
    Input Text           xpath: //input[@id="bandwithDownNight"]    10000
    Click Element        xpath: //button[@type="submit"]
    Capture Page Screenshot   ${SCREENSHOT_PATH}/trafficshaping_setup.png


Sync Speed Selection
    Leave Setup
    Wait For Result      trafficshape.py -c   \"day\": true
    Click Element        xpath: //button[@id="shaping-button"]
    Capture Page Screenshot   ${SCREENSHOT_PATH}/syncspeed_selection.png
    Wait For Result      trafficshape.py -c   \"day\": true
    Click Element        xpath: //label[@for="night"]
    Wait For Result      trafficshape.py -c   \"day\": false
    Click Element        xpath: //button[@id="shaping-button"]
    Click Element        xpath: //label[@for="no_sync"]
    Wait For State       Default No Sync
    Capture Page Screenshot   ${SCREENSHOT_PATH}/no_sync_mode.png
    Click Element        xpath: //button[@id="shaping-button"]
    Click Element        xpath: //label[@for="day"]
    Wait For State       Default


UI Logout
    Switch Browser       alice
    Valid Logout
    
    

