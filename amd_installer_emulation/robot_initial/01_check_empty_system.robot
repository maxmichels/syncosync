*** Settings ***
Documentation          Check parameters for an empty system

Resource               ../syncosync-keywords.robot

Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections

*** Test Cases ***
Check sysstate
    [Documentation]         Check Sysstate
                            Switch Connection  alice
    ${output}               Execute Command    sysstate.py -g
                            Log  ${output}

Execute Command And Verify Output  
    [Documentation]          Just test, if echo works
                             Switch Connection  alice
    ${output}=               Execute Command    echo Hello SSHLibrary!
    Should Be Equal          ${output}          Hello SSHLibrary!

set hostnames
                             Switch Connection  alice
    ${output}=               Execute Command    hostname.py -s '{"hostname": "alice"}'
    ${output}=               Execute Command    hostnamectl
    Should Contain           ${output}          alice
                             Switch Connection  bob
    ${output}=               Execute Command    hostname.py -s '{"hostname": "bob"}'
    ${output}=               Execute Command    hostnamectl
    Should Contain           ${output}         bob


syncosync service started
    [Documentation]          Is syncosync service running?
                             Switch Connection  alice
    ${output}=               Execute Command    systemctl is-active syncosync.service
    Should Be Equal          ${output}          active
chroot env generated
    [Documentation]          Is the sos chroot environment generated?
                             Switch Connection  alice
    ${output}=               Execute Command    ls -1 /var/lib/syncosync/chroot/bin/
    Should Contain           ${output}          rsync
    Should Contain           ${output}          bash
disk recognized
    [Documentation]          Is there a disk available?
                             Switch Connection  alice
    ${output}=               Execute Command    drivemanager.py -g
    Should Contain           ${output}          "device": "sdb" 
disk empty
    [Documentation]          Is the disk empty?
                             Switch Connection  alice
    ${output}=               Execute Command    drivemanager.py -g
    Should Contain           ${output}          "sos_found": false
list accounts should return empty list
    [Documentation]          We should get an empty list of sos accounts
                             Switch Connection  alice
    ${stdout}                Execute Command    sosaccounts.py -l
    Should Be Equal          ${stdout}          []
check sosconfig
    [Documentation]          check sosconfig set by postinst
                             Switch Connection  alice
                             SSHLibrary.File Should Exist         /etc/syncosync/sosconfig.json

check initial sysstate
    [Documentation]          sysstate should be setup_disks initially
                             Switch Connection  alice
                             Wait For State  Setup Disks


