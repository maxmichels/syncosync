# SosUI
The frontend for syncosync

## Development setup
Since Angular can run its own small webserver for development,
you can debug/run/build the UI on your local machine.
However, the Syncosync Core and UI-Python part need to run somewhere as well.
This may be a VM, an actual SOS-Box (be it production or for whatever purpose) 
or a local execution of the Python-code. All you need is the SosUI Python port accessible. 
This currently is port 5000. 

### Initial setup
Install Node.js by using Google ;)
Then just run
```bash
npm install -g @angular/cli
npm install --save-dev @angular-devkit/build-angular
npm install
```

#### Proxy Config
In order to use a different host running syncosync, you will need to copy the proxy config example
```bash
cp proxy.conf.json.example proxy.conf.json
```
and edit it to direct requests to the host you are running syncosync on.
All you need to do is edit line 3: `"target": "http://192.168.246.129:5000/",`
to point to the correct IP-address. 
This will essentially forward all requests to `/api/` towards the host specified.

If you want to run with the proxy enabled you need to execute `ng serve --proxy-config proxy.conf.json`.
This can also be configured in IntelliJ (and any Jetbrains tool) by adding an Angular CLI Server configuration referencing `start-proxied` as the script.


## Development server

Run `ng serve` for a dev server or `ng serve --proxy-config proxy.conf.json`. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.
Alternatively, use the contextmenu of IntelliJ -> New -> Angular Schematic

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
