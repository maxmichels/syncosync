import {Injectable} from '@angular/core';
import {InMemoryDbService} from 'angular-in-memory-web-api';
import {Status} from "@syncosync_common";


@Injectable({
    providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

    createDb() {
        const status: Status = new Status();
// {"Date":"1572374694","Down":"0.02","RemoteDownSince":"","RemoteHost":"true","SyncFromRemote":"idle",
// "SyncFromRemoteSince":"26.10.2019 16:05:28","SyncToRemote":"idle","SyncToRemoteSince":"29.10.2019 04:45:03","Up":"1.30",
// "admin_last_mail":"","aktfile":"","availableSpace":"2687772928","backup_period1":"7","backup_period2":"7","drivestatus":"[1,0] success",
// "info_period1":"7","info_period2":"7","iused":"7275","key_fingerprint1":"4a:cd:17:85:44:fc:a5:7b:23:f3:b1:1a:82:50:87:d9",
// "key_fingerprint2":"88:29:6c:d3:c0:83:69:5a:0d:07:01:64:62:bf:ec:3a","last_access1":"2019-10-29T04:45:27+0100",
// "last_access2":"2018-06-10T07:01:08+0200","last_mail1":"1970-01-01T01:00:00.000000000+01:00",
// "last_mail2":"2019-10-25T06:25:19+0200","mail_address1":"webmaster@mueskro.de","mail_address2":"till@mueskro.de",
// "mail_info_level1":"error","mail_info_level2":"error","max_space_allowed1":"90","max_space_allowed2":"100",
// "name1":"homeserverConfigHome","name2":"pokemapVps","overdue1":"0:0","overdue2":"484:1","percent":"0","remind_duration1":"22",
// "remind_duration2":"22","rlongname1":"Homeserver configs and home","rlongname2":"Pokemap VPS","sda":"down",
// "space_used1":"182337668","space_used2":"6547740","systype":"sosbp","tocheck":"0","totalSize":"2876717744",
// "usedPercentage":"7%","usedSpace":"188944816","users":"2"}

        // populate mock data
        /*status.Date = Date.now();
        status.adminLastMail = new Date();

        status.syncInfo.RemoteDownSince = '';
        status.syncInfo.RemoteHost = true;
        status.syncInfo.SyncFromRemote = 'idle';
        status.syncInfo.SyncToRemote = 'idle';
        status.syncInfo.SyncFromRemoteSince = new Date('26.10.2019 16:05:28');
        status.syncInfo.SyncToRemoteSince = new Date('29.10.2019 04:45:03');

        const testUser = new User();
        testUser.name = 'till';
        testUser.backupPeriod = 22;
        testUser.infoPeriod = 50;
        testUser.keyFingerprint = '4a:cd:17:85:44:fc:a5:7b:23:f3:b1:1a:82:50:87:d9';
        testUser.lastMail = new Date('1970-01-01T01:00:00.000000000+01:00');
        // testUser.lastAccess = new Date('2019-10-29T04:45:27+0100');
        testUser.lastAccess = new Date(1571777345 * 1000);
        testUser.longname = 'Till Laptop';
        testUser.remindDuration = 10;
        testUser.maxSpaceAllowed = 90;
        testUser.mailAddress = 'till@mueskro.de';
        testUser.mailInfoLevel = MailInfoLevel.info;
        testUser.overdue = '0:0';
        testUser.spaceUsed = 182337668;
        status.users.push(testUser);

        status.deviceState.availableSpace = 2687772928;
        status.deviceState.drivestatus = '[1,0] success';
        status.deviceState.percent = 0;
        status.deviceState.sda = Drivestate.down;
        status.deviceState.systype = 'sosbp';
        status.deviceState.totalSize = 2876717744;
        status.deviceState.usedPercentage = 7;
        status.deviceState.usedSpace = 188944816;*/

        // tslint:disable-next-line:variable-name

        return {status};
    }
}
