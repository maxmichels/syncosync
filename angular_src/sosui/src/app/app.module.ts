import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SyncosyncModule } from './pages/syncosync/syncosync.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { SyncosyncCommonModule } from '../syncosync_modules/common/syncosync-common.module';
import { JwtInterceptor } from './helpers/interceptors/jwt.interceptor';
import { RestErrorInterceptor } from './helpers/interceptors/rest-error-interceptor.service';
import { RemoteRecoveryComponent } from './pages/remote-recovery/remote-recovery.component';
import { RemoteRecoveryModule } from './pages/remote-recovery/remote-recovery.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [AppComponent, RemoteRecoveryComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SyncosyncModule,
    HttpClientModule,
    SyncosyncCommonModule,
    RemoteRecoveryModule,
    NgbModule,
    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    /*HttpClientInMemoryWebApiModule.forRoot(
          InMemoryDataService, { dataEncapsulation: false }
        ),*/
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: RestErrorInterceptor, multi: true },
    Title,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
