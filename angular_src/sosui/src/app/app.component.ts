import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Hostname } from '@syncosync_common';
import { NetworkStatusService } from '@syncosync_public';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  public hostname: Hostname = new Hostname();

  public constructor(
    private titleService: Title,
    public networkStatusService: NetworkStatusService
  ) {}

  public setTitle(newTitle: string): void {
    this.titleService.setTitle(newTitle);
  }

  ngOnInit(): void {
    this.networkStatusService.getHostname().subscribe((hostname) => {
      if (hostname !== undefined) {
        this.hostname = hostname;
        this.titleService.setTitle('syncosync - ' + hostname.hostname);
      }
    });
  }
}
