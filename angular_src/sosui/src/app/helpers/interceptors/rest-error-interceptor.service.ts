import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {
  AuthService,
  GenericUiResponse,
  InfoLevel,
  SyncosyncModalService,
} from '@syncosync_common';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class RestErrorInterceptor implements HttpInterceptor {
  private previousModal: NgbModalRef = null;

  constructor(
    private authService: AuthService,
    private modalService: SyncosyncModalService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((err) => {
        if (err.status === 401) {
          // auto logout if 401 response returned from api
          console.log('Logging out');
          this.authService.logout();
        } else if (err.status === 500) {
          // An internal error occurred, we can try to read the body, maybe a generic response was given that we can work with
          const genericResponse: GenericUiResponse = new GenericUiResponse();
          try {
            Object.assign(genericResponse, err.error);
          } catch (e) {
            console.log('Failed reading response...');
          } finally {
            this.showInfoModal(genericResponse);
          }
        }
        if (err.error === undefined) {
          return throwError(err.statusText);
        }
        const error = err.error.message || err.statusText;
        return throwError(error);
      })
    );
  }

  private showInfoModal(genericResponse: GenericUiResponse) {
    if (this.previousModal !== null) {
      // TODO: Abort showing new modal or update existing by appending data?
      this.previousModal.close();
    }
    this.previousModal = this.modalService.showInfoModal(
      InfoLevel.ERROR,
      genericResponse.localized_reason,
      genericResponse.additional_information
    );
  }
}
