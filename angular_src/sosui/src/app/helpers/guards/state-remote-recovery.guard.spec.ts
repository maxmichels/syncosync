import { TestBed } from '@angular/core/testing';

import { StateRemoteRecoveryGuard } from './state-remote-recovery.guard';

describe('StateRemoteRecoveryGuard', () => {
  let guard: StateRemoteRecoveryGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(StateRemoteRecoveryGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
