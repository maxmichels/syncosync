import { TestBed } from '@angular/core/testing';

import { StateSetupDisksGuard } from './state-setup-disks.guard';

describe('DiskSetupGuardGuard', () => {
  let guard: StateSetupDisksGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(StateSetupDisksGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
