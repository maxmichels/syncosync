import { TestBed } from '@angular/core/testing';

import { StateVolumeSetupGuard } from './state-volume-setup.guard';

describe('StateVolumeSetupGuard', () => {
  let guard: StateVolumeSetupGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(StateVolumeSetupGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
