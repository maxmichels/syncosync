import { TestBed } from '@angular/core/testing';

import { StateShutdownGuard } from './state-shutdown.guard';

describe('StateShutdownGuard', () => {
  let guard: StateShutdownGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(StateShutdownGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
