import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { SysstateService } from '@syncosync_public';
import { SystemMode } from '@syncosync_common';

@Injectable({
  providedIn: 'root',
})
export class StateVolumeSetupGuard implements CanActivate {
  // In case a state other than DiskSetup is present, the user should get redirected accordingly...
  constructor(
    private router: Router,
    private sysstateService: SysstateService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const latestSysstate = this.sysstateService.getLatestSysstate();

    if (latestSysstate.actmode == SystemMode.SETUP_VOLUMES) {
      this.router.navigate(['/volumesetup'], {});
      return false;
    } else {
      // Not in disk setup mode, redirect accordingly...
      return true;
    }
  }
}
