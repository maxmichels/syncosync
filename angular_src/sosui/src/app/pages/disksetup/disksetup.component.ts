import { Component, OnInit } from '@angular/core';
import { DrivesetupDriveConfiguration, SystemMode } from '@syncosync_common';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SysstateService } from '@syncosync_public';
import { ProcessingSpinnerStateService } from '../../../syncosync_modules/common/services/processing-spinner-state.service';

@Component({
  selector: 'app-disksetup',
  templateUrl: './disksetup.component.html',
  styleUrls: ['./disksetup.component.css'],
})
export class DisksetupComponent implements OnInit {
  constructor(
    private router: Router,
    private sysstateService: SysstateService,
    private processingSpinnerStateService: ProcessingSpinnerStateService
  ) {
    //
  }

  ngOnInit(): void {
    //
  }

  handleDrivesetupResult(result: DrivesetupDriveConfiguration): void {
    if (result !== undefined && result !== null) {
      this.processingSpinnerStateService.addSpinner();
      const unsubscribe: Subject<void> = new Subject();
      this.sysstateService.latestSysstateObservable$
        .pipe(takeUntil(unsubscribe))
        .subscribe((res) => {
          if (res.actmode == SystemMode.SETUP_DISKS) {
            return true;
          } else if (res.actmode == SystemMode.SETUP_VOLUMES) {
            unsubscribe.next();
            this.router.navigate(['/volumesetup'], {}).then((r) => {
              if (!r) {
                console.log('Failed navigating to volumesetup.');
              }
            });
            this.processingSpinnerStateService.removeSpinner();
            return false;
          } else if (res.actmode == SystemMode.DEFAULT) {
            unsubscribe.next();
            this.router.navigate(['/syncosync'], {}).then((r) => {
              if (!r) {
                console.log('Failed navigating to status.');
              }
            });
            this.processingSpinnerStateService.removeSpinner();
            return false;
          } else {
            console.log('Did not get one of the expected states.');
            this.processingSpinnerStateService.removeSpinner();
            return false;
          }
        });
    }
  }
}
