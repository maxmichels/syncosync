import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DisksetupComponent } from './disksetup.component';
import { StatusbarComponent } from './statusbar/statusbar.component';
import { SyncosyncAuthenticatedFormsModule } from '../../../syncosync_modules/authenticated/forms/syncosync-authenticated-forms.module';
import { DisksetupRoutes } from './disksetup.routes';
import { DrivesetupPartialComponent } from './components/drivesetup-partial/drivesetup-partial.component';
import { DrivesetupComponent } from './components/drivesetup/drivesetup.component';
import { DriveboxComponent } from './components/drivesetup/drivebox/drivebox.component';
import { SyncosyncCommonModule } from '../../../syncosync_modules/common/syncosync-common.module';
import { SyncosyncPublicModule } from '../../../syncosync_modules/public/syncosync-public.module';
import { SyncosyncComponentsModule } from '../../../syncosync_modules/authenticated/components/syncosync-components.module';
import { SyncosyncAuthenticatedGraphsModule } from '../../../syncosync_modules/authenticated/graphs/syncosync-authenticated-graphs.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    SyncosyncCommonModule,
    DisksetupRoutes,
    SyncosyncAuthenticatedFormsModule,
    SyncosyncComponentsModule,
    NgbModule,
    SyncosyncPublicModule,
    SyncosyncAuthenticatedGraphsModule,
  ],
  declarations: [
    DisksetupComponent,
    StatusbarComponent,
    DrivesetupPartialComponent,
    DrivesetupComponent,
    DriveboxComponent,
  ],
  exports: [StatusbarComponent],
})
export class DisksetupModule {}
