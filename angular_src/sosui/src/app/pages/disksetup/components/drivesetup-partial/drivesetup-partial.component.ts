import { Component } from '@angular/core';
import {
  DrivesetupDriveConfiguration,
  SetupAssistantDirective,
} from '@syncosync_common';

@Component({
  selector: 'app-drivesetup-partial',
  templateUrl: './drivesetup-partial.component.html',
  styleUrls: ['./drivesetup-partial.component.css'],
})
export class DrivesetupPartialComponent extends SetupAssistantDirective<DrivesetupDriveConfiguration> {
  constructor() {
    super();
  }
}
