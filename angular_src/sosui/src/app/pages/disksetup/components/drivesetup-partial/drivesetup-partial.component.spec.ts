import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrivesetupPartialComponent } from './drivesetup-partial.component';

describe('DrivesetupPartialComponent', () => {
  let component: DrivesetupPartialComponent;
  let fixture: ComponentFixture<DrivesetupPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DrivesetupPartialComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrivesetupPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
