import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriveboxComponent } from './drivebox.component';

describe('DriveboxComponent', () => {
  let component: DriveboxComponent;
  let fixture: ComponentFixture<DriveboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DriveboxComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriveboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
