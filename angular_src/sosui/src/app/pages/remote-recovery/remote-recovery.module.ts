import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusbarComponent } from './statusbar/statusbar.component';
import {
  NgbProgressbarModule,
  NgbTooltipModule,
} from '@ng-bootstrap/ng-bootstrap';
import { RemoteRecoveryInProgressComponent } from './remote-recovery-in-progress/remote-recovery-in-progress.component';
import { RemoteRecoveryRoutes } from './remote-recovery.routes';
import { SyncosyncCommonModule } from '../../../syncosync_modules/common/syncosync-common.module';
import { SyncosyncPublicModule } from '../../../syncosync_modules/public/syncosync-public.module';
import { RemoteRecoveryFinishedComponent } from './remote-recovery-finished/remote-recovery-finished.component';
import { SyncosyncAuthenticatedGraphsModule } from '../../../syncosync_modules/authenticated/graphs/syncosync-authenticated-graphs.module';
@NgModule({
  declarations: [
    StatusbarComponent,
    RemoteRecoveryInProgressComponent,
    RemoteRecoveryFinishedComponent,
  ],
  imports: [
    RemoteRecoveryRoutes,
    CommonModule,
    SyncosyncCommonModule,
    NgbTooltipModule,
    NgbProgressbarModule,
    SyncosyncAuthenticatedGraphsModule,
    SyncosyncPublicModule,
  ],
  exports: [
    StatusbarComponent,
    RemoteRecoveryInProgressComponent,
    RemoteRecoveryFinishedComponent,
  ],
})
export class RemoteRecoveryModule {}
