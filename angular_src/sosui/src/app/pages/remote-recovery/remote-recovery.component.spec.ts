import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoteRecoveryComponent } from './remote-recovery.component';

describe('RemoteRecoveryComponent', () => {
  let component: RemoteRecoveryComponent;
  let fixture: ComponentFixture<RemoteRecoveryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RemoteRecoveryComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoteRecoveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
