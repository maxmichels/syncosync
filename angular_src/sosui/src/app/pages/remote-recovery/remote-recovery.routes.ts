import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RemoteRecoveryComponent } from './remote-recovery.component';

const routes: Routes = [
  {
    path: '',
    component: RemoteRecoveryComponent,
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RemoteRecoveryRoutes {}
