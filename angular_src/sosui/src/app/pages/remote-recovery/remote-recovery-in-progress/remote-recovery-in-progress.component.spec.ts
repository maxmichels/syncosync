import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoteRecoveryInProgressComponent } from './remote-recovery-in-progress.component';

describe('RemoteRecoveryInProgressComponent', () => {
  let component: RemoteRecoveryInProgressComponent;
  let fixture: ComponentFixture<RemoteRecoveryInProgressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RemoteRecoveryInProgressComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoteRecoveryInProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
