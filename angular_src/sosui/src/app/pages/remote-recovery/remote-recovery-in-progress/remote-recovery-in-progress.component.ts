import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, Subject, timer } from 'rxjs';
import { RemoteRecoveryService } from '../../../../syncosync_modules/authenticated/services/remote-recovery.service';
import { switchMap, takeUntil } from 'rxjs/operators';
import {
  ConfirmationModalResult,
  GenericUiResponse,
  GenericUiResponseStatus,
  InfoLevel,
  LocalizedMessage,
  SosToCheck,
  SyncosyncModalService,
} from '@syncosync_common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-remote-recovery-in-progress',
  templateUrl: './remote-recovery-in-progress.component.html',
  styleUrls: ['./remote-recovery-in-progress.component.css'],
})
export class RemoteRecoveryInProgressComponent implements OnInit, OnDestroy {
  private progressRequestTimer: Observable<number> = timer(0, 5000);
  latestSosToCheckSubject: BehaviorSubject<SosToCheck> =
    new BehaviorSubject<SosToCheck>(new SosToCheck());
  protected unsubscribe: Subject<void> = new Subject();
  latestSosToCheckObservable$: Observable<SosToCheck> =
    this.latestSosToCheckSubject.asObservable();

  constructor(
    private remoteRecoveryService: RemoteRecoveryService,
    private modalService: SyncosyncModalService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.progressRequestTimer
      .pipe(
        takeUntil(this.unsubscribe),
        switchMap(() => this.remoteRecoveryService.getProgress())
      )
      .subscribe((res) => {
        if (res !== undefined && res !== null) {
          this.latestSosToCheckSubject.next(res);
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
  }

  cancelRemoteRecovery(): void {
    // For now, we will trigger a factory reset if confirmed by the user
    this.modalService
      .showConfirmationModal(
        LocalizedMessage.REMOTE_RECOVERY_CANCEL_TITLE,
        LocalizedMessage.REMOTE_RECOVERY_CANCEL_CONTENT
      )
      .subscribe((userDecision) => {
        if (userDecision == ConfirmationModalResult.CONFIRM) {
          this.callCancel();
        }
      });
  }

  private callCancel(): void {
    // Actually trigger the request to cancel the remote recovery (triggering a factory reset)
    const resultEmitter: EventEmitter<GenericUiResponse> =
      new EventEmitter<GenericUiResponse>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined) {
        if (result.status == GenericUiResponseStatus.OK) {
          this.router.navigate(['/disksetup'], {});
        } else {
          this.modalService.showInfoModal(
            InfoLevel.WARNING,
            result.localized_reason,
            result.additional_information
          );
        }
      }
    });
    this.modalService.showProcessingModal<GenericUiResponse>(
      LocalizedMessage.REMOTE_RECOVERY_CANCEL_TITLE,
      LocalizedMessage.REMOTE_RECOVERY_CANCEL_IN_PROGRESS_TITLE,
      this.remoteRecoveryService.cancelRemoteRecovery(),
      null,
      resultEmitter
    );
  }
}
