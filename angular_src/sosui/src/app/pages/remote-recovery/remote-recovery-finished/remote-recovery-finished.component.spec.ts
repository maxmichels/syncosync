import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoteRecoveryFinishedComponent } from './remote-recovery-finished.component';

describe('RemoteRecoveryFinishedComponent', () => {
  let component: RemoteRecoveryFinishedComponent;
  let fixture: ComponentFixture<RemoteRecoveryFinishedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RemoteRecoveryFinishedComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoteRecoveryFinishedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
