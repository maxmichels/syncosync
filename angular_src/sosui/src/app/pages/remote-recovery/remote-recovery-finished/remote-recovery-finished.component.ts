import { Component, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import {
  AbstractEditableFormDirective,
  InfoLevel,
  LocalizedMessage,
  SyncosyncModalService,
  SysStateTransport,
  SystemMode,
} from '@syncosync_common';
import { SysStateResult } from 'src/syncosync_modules/common/model/sysState';
import { SwitchAction } from 'src/syncosync_modules/common/model/sosEnums';
import { ProcessingSpinnerStateService } from '../../../../syncosync_modules/common/services/processing-spinner-state.service';
import { RemoteRecoveryService } from '../../../../syncosync_modules/authenticated/services/remote-recovery.service';
import { Router } from '@angular/router';
import { ModeManagementService } from '../../../../syncosync_modules/authenticated/services/mode-management.service';
import { takeUntil } from 'rxjs/operators';
import { SysstateService } from '@syncosync_public';

@Component({
  selector: 'app-remote-recovery-finished',
  templateUrl: './remote-recovery-finished.component.html',
  styleUrls: ['./remote-recovery-finished.component.css'],
})
export class RemoteRecoveryFinishedComponent extends AbstractEditableFormDirective<SysStateResult> {
  constructor(
    private remoteRecoveryService: RemoteRecoveryService,
    protected syncosyncModalService: SyncosyncModalService,
    private modeManagementService: ModeManagementService,
    private router: Router,
    protected processingSpinnerStateService: ProcessingSpinnerStateService,
    private sysstateService: SysstateService
  ) {
    super(syncosyncModalService, processingSpinnerStateService);
  }

  onSubmit(): void {
    this.switchDefault();
  }

  protected createFormGroup(): void {
    //
  }

  private switchDefault() {
    const sstmodel = new SysStateTransport();
    sstmodel.state_desired = SystemMode.DEFAULT;
    sstmodel.switch_action = SwitchAction.DEFAULT;
    const resultEmitter: EventEmitter<SysStateResult> =
      new EventEmitter<SysStateResult>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined && result !== null) {
        if (result.mode == SystemMode.DEFAULT) {
          // Success
          this.formFinished.emit(this.model);
          this.sysstateService.latestSysstateObservable$
            .pipe(takeUntil(this.unsubscribe))
            .subscribe((res) => {
              if (res.actmode == SystemMode.RR_FINAL) {
                return true;
              } else if (res.actmode == SystemMode.DEFAULT) {
                this.unsubscribe.next();
                this.router.navigate(['/syncosync/status']);
              }
            });
        } else {
          this.syncosyncModalService.showInfoModal(
            InfoLevel.ERROR,
            LocalizedMessage.DEFAULT_MODE_FAILED_GENERIC_MESSAGE,
            result.report.toString()
          );
        }
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.UNKNOWN_ERROR
        );
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.PROCESSING_DEFAULT_REQUEST_CONTENT,
      this.modeManagementService.setMode(sstmodel),
      null,
      resultEmitter
    );
  }

  protected modelRequest(): Observable<SysStateResult> {
    console.log('Requesting RRResult');
    return this.remoteRecoveryService.getRRResult();
  }

  protected getInitModel(): SysStateResult {
    return new SysStateResult();
  }
}
