import { Component, OnInit } from '@angular/core';
import { AuthService, HelpStateService } from '@syncosync_common';

@Component({
  selector: 'remote-recovery-statusbar',
  templateUrl: './statusbar.component.html',
  styleUrls: ['./statusbar.component.css'],
})
export class StatusbarComponent implements OnInit {
  constructor(
    private authService: AuthService,
    public helpStateService: HelpStateService
  ) {}

  logout(): void {
    this.authService.logout();
  }

  ngOnInit(): void {
    //
  }
}
