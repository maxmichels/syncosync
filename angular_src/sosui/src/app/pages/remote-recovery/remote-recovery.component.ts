import { Component, OnInit } from '@angular/core';
import { SysstateService } from '@syncosync_public';
import { SystemMode } from '@syncosync_common';

@Component({
  selector: 'app-remote-recovery',
  templateUrl: './remote-recovery.component.html',
  styleUrls: ['./remote-recovery.component.css'],
})
export class RemoteRecoveryComponent implements OnInit {
  systemMode = SystemMode;
  constructor(public sysstateService: SysstateService) {}

  ngOnInit(): void {
    //
  }
}
