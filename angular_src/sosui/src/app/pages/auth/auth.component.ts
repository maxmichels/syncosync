import { Component, HostListener, OnInit } from '@angular/core';
import { AuthService, SystemMode } from '@syncosync_common';
import { Router } from '@angular/router';
import { SysstateService } from '@syncosync_public';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
})
export class AuthComponent implements OnInit {
  systemModeEnum = SystemMode;
  showSidebar = false;
  screenHeight = 0;
  screenWidth = 0;

  constructor(
    private router: Router,
    private authService: AuthService,
    public sysstateService: SysstateService
  ) {
    // redirect to home if already logged in
    if (this.authService.userValue) {
      console.log('Already logged in');
      // TODO: Redirect to ESA / disaster recovery helper / whatever assistant if required to here or after successful auth
      this.router.navigate(['/syncosync/sosui']);
    }
    this.onResize();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    this.evaluateScreenSize();
  }

  ngOnInit() {}

  toggleClass(wrapper: HTMLDivElement, classToToggle: string) {
    const hasClass = wrapper.classList.contains(classToToggle);
    this.showSidebar = !this.showSidebar;

    if (hasClass) {
      wrapper.classList.remove(classToToggle);
    } else {
      wrapper.classList.add(classToToggle);
    }
  }

  private evaluateScreenSize() {
    this.showSidebar = this.screenWidth >= 720;
  }
}
