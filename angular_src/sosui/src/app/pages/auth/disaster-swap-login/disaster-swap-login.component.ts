import { Component, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractEmptyFormDirective } from '../../../../syncosync_modules/common/components/abstract-empty-form-directive/abstract-empty-form.directive';
import {
  InfoLevel,
  LocalizedMessage,
  SyncosyncModalService,
  SysStateTransport,
  SystemMode,
} from '@syncosync_common';
import { SysstateService } from '@syncosync_public';
import { SysStateResult } from '../../../../syncosync_modules/common/model/sysState';

@Component({
  selector: 'app-disaster-swap-login',
  templateUrl: './disaster-swap-login.component.html',
  styleUrls: ['./disaster-swap-login.component.css'],
})
export class DisasterSwapLoginComponent extends AbstractEmptyFormDirective<SysStateTransport> {
  loading = false;
  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sysstateService: SysstateService,
    protected syncosyncModalService: SyncosyncModalService
  ) {
    super();
  }

  onSubmit(): void {
    const resultEmitter: EventEmitter<SysStateResult> =
      new EventEmitter<SysStateResult>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined && result !== null) {
        if (result.mode == SystemMode.DISASTER_SWAP) {
          // Swapped successfully
          this.formFinished.emit(this.model);
          window.location.reload();
        } else {
          this.syncosyncModalService.showInfoModal(
            InfoLevel.INFO,
            LocalizedMessage.DISASTER_SWAP_FAILED_GENERIC_MESSAGE
          );
        }
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.UNKNOWN_ERROR
        );
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.PROCESSING_DISASTER_SWAP_REQUEST_CONTENT,
      this.sysstateService.setMode(this.model),
      null,
      resultEmitter
    );
  }

  protected createFormGroup(): void {
    //
  }

  protected getInitModel(): SysStateTransport {
    const model = new SysStateTransport();
    model.state_desired = SystemMode.DISASTER_SWAP;
    model.config_backup_password = '';
    return model;
  }
}
