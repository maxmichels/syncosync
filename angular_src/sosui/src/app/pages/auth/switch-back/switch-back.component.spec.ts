import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchBackComponent } from './switch-back.component';

describe('SwitchBackComponent', () => {
  let component: SwitchBackComponent;
  let fixture: ComponentFixture<SwitchBackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SwitchBackComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchBackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
