import { Component, EventEmitter } from '@angular/core';
import { AbstractEmptyFormDirective } from '../../../../syncosync_modules/common/components/abstract-empty-form-directive/abstract-empty-form.directive';
import {
  InfoLevel,
  LocalizedMessage,
  SyncosyncModalService,
  SysStateTransport,
  SystemMode,
} from '@syncosync_common';
import { ActivatedRoute, Router } from '@angular/router';
import { SysstateService } from '@syncosync_public';
import { SysStateResult } from '../../../../syncosync_modules/common/model/sysState';

@Component({
  selector: 'app-switch-back',
  templateUrl: './switch-back.component.html',
  styleUrls: ['./switch-back.component.css'],
})
export class SwitchBackComponent extends AbstractEmptyFormDirective<SysStateTransport> {
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sysstateService: SysstateService,
    protected syncosyncModalService: SyncosyncModalService
  ) {
    super();
  }

  onSubmit(): void {
    const resultEmitter: EventEmitter<SysStateResult> =
      new EventEmitter<SysStateResult>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined && result !== null) {
        if (result.mode == SystemMode.DEFAULT) {
          // Swapped successfully
          this.formFinished.emit(this.model);
          window.location.reload();
        } else {
          this.syncosyncModalService.showInfoModal(
            InfoLevel.INFO,
            LocalizedMessage.SWITCH_BACK_FAILED_GENERIC_MESSAGE
          );
        }
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.UNKNOWN_ERROR
        );
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.PROCESSING_SWITCH_BACK_REQUEST_CONTENT,
      this.sysstateService.setMode(this.model),
      null,
      resultEmitter
    );
  }

  protected createFormGroup(): void {
    //
  }

  protected getInitModel(): SysStateTransport {
    const model = new SysStateTransport();
    model.state_desired = SystemMode.DEFAULT;
    model.config_backup_password = '';
    return model;
  }
}
