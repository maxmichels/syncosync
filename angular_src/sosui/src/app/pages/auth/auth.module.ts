import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutes } from './auth.routes';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './login/login.component';
import { AuthComponent } from './auth.component';
import { StatusbarComponent } from './statusbar/statusbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DisasterSwapLoginComponent } from './disaster-swap-login/disaster-swap-login.component';
import { SyncosyncPublicModule } from '../../../syncosync_modules/public/syncosync-public.module';
import { SwitchBackComponent } from './switch-back/switch-back.component';

import { SyncosyncAuthenticatedGraphsModule } from '../../../syncosync_modules/authenticated/graphs/syncosync-authenticated-graphs.module';
@NgModule({
  declarations: [
    AuthComponent,
    LoginComponent,
    StatusbarComponent,
    DisasterSwapLoginComponent,
    SwitchBackComponent,
  ],
  exports: [],
  imports: [
    CommonModule,
    AuthRoutes,
    ReactiveFormsModule,
    NgbModule,
    FormsModule,
    SyncosyncPublicModule,
    SyncosyncAuthenticatedGraphsModule,
  ],
  providers: [NgbActiveModal],
})
export class AuthModule {}
