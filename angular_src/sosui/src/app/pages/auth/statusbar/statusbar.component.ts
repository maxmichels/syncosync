import {Component, OnInit} from '@angular/core';
import {Hostname} from "@syncosync_common";
import {NetworkStatusService} from "@syncosync_public";


@Component({
    selector: 'app-statusbar',
    templateUrl: './statusbar.component.html',
    styleUrls: ['./statusbar.component.css']
})
export class StatusbarComponent implements OnInit {

    public hostname: Hostname = new Hostname();

    constructor(public networkStatusService: NetworkStatusService) {
    }

    ngOnInit(): void {
        this.networkStatusService.getHostname()
            .subscribe(hostname => {
                if (hostname !== undefined) {
                    this.hostname = hostname
                }
            })
    }

}
