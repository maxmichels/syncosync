import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SyncosyncComponent} from './syncosync.component';

describe('SyncosyncComponent', () => {
    let component: SyncosyncComponent;
    let fixture: ComponentFixture<SyncosyncComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SyncosyncComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SyncosyncComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
