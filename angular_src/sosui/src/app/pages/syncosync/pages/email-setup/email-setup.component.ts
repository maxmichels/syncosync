import {Component, OnInit} from '@angular/core';
import {HelpStateService} from "@syncosync_common";

@Component({
    selector: 'app-email-setup',
    templateUrl: './email-setup.component.html',
    styleUrls: ['./email-setup.component.css']
})
export class EmailSetupComponent implements OnInit {

    constructor(public helpStateService: HelpStateService) {
    }

    ngOnInit(): void {
    }

}
