import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NetworkSetupComponent} from './network-setup.component';

describe('NetworkSetupComponent', () => {
    let component: NetworkSetupComponent;
    let fixture: ComponentFixture<NetworkSetupComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NetworkSetupComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NetworkSetupComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
