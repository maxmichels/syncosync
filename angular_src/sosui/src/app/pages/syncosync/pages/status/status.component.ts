import { Component } from '@angular/core';
import { StatusService } from '@syncosync_public';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css'],
})
export class StatusComponent {
  constructor(public statusService: StatusService) {}
}
