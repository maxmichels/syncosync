import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceCollectionComponent } from './maintenance-collection.component';

describe('MaintenanceCollectionComponent', () => {
  let component: MaintenanceCollectionComponent;
  let fixture: ComponentFixture<MaintenanceCollectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MaintenanceCollectionComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceCollectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
