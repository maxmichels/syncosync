import { Component } from '@angular/core';

@Component({
  selector: 'app-maintenance-collection',
  templateUrl: './maintenance-collection.component.html',
  styleUrls: ['./maintenance-collection.component.css'],
})
export class MaintenanceCollectionComponent {}
