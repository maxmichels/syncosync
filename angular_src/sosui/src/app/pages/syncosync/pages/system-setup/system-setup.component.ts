import { Component, OnInit } from '@angular/core';
import { SysstateService } from '@syncosync_public';
import { SystemMode } from '@syncosync_common';

@Component({
  selector: 'app-system-setup',
  templateUrl: './system-setup.component.html',
  styleUrls: ['./system-setup.component.css'],
})
export class SystemSetupComponent implements OnInit {
  systemMode = SystemMode;
  constructor(public sysstateService: SysstateService) {}

  ngOnInit(): void {
    //
  }
}
