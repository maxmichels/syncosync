import { Component } from '@angular/core';
import {
  HelpStateService,
  SetupAssistantDirective,
  SystemSetupData,
} from '@syncosync_common';

@Component({
  selector: 'app-sync-setup',
  templateUrl: './sync-setup.component.html',
  styleUrls: ['./sync-setup.component.css'],
})
export class SyncSetupComponent extends SetupAssistantDirective<SystemSetupData> {
  constructor(public helpStateService: HelpStateService) {
    super();
  }
}
