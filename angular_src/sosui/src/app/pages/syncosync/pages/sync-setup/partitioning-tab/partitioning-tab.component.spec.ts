import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartitioningTabComponent } from './partitioning-tab.component';

describe('PartitioningTabComponent', () => {
  let component: PartitioningTabComponent;
  let fixture: ComponentFixture<PartitioningTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PartitioningTabComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartitioningTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
