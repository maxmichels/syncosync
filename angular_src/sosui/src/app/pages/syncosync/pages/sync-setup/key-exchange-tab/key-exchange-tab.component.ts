import { Component } from '@angular/core';
import {
  HelpStateService,
  SetupAssistantDirective,
  SosKey,
  SystemSetupData,
} from '@syncosync_common';
import {
  KeyExchangeButtonsComponent,
  SyncSetupRemoteConfigComponent,
} from '@syncosync_authenticated';

@Component({
  selector: 'app-key-exchange-tab',
  templateUrl: './key-exchange-tab.component.html',
  styleUrls: ['./key-exchange-tab.component.css'],
})
export class KeyExchangeTabComponent extends SetupAssistantDirective<SystemSetupData> {
  displayRemoteSettings = false;

  constructor(public helpStateService: HelpStateService) {
    super();
  }

  enableRemoteSettings($event: SosKey): void {
    /**
     * Enables the inputs of the remote partner settings as well as syncing up.
     */
    if ($event !== undefined && $event !== null) {
      // TODO: Toggle stuff for remote settings
      this.displayRemoteSettings = true;
    }
  }

  enableKeyExchangeButtons(
    keyExchangeButtons: KeyExchangeButtonsComponent,
    syncSetupRemoteConfigComponent: SyncSetupRemoteConfigComponent
  ): void {
    if (
      syncSetupRemoteConfigComponent.model != null &&
      syncSetupRemoteConfigComponent.model.hostname?.length > 0 &&
      syncSetupRemoteConfigComponent.model.port > 0 &&
      syncSetupRemoteConfigComponent.model.port <= 65535
    ) {
      keyExchangeButtons.disableButtons = false;
    }
  }
}
