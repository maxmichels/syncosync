import { Component, EventEmitter, Output } from '@angular/core';
import { AuthService, HelpStateService } from '@syncosync_common';
import { SetupUiSwitchingService } from '../../../../syncosync_modules/common/services/setup-ui-switching.service';

@Component({
  selector: 'app-statusbar',
  templateUrl: './statusbar.component.html',
  styleUrls: ['./statusbar.component.css'],
})
export class StatusbarComponent {
  @Output()
  toggleNavbar = new EventEmitter<void>();
  constructor(
    private authService: AuthService,
    public setupUiSwitchingService: SetupUiSwitchingService,
    public helpStateService: HelpStateService
  ) {}

  logout(): void {
    this.authService.logout();
  }
}
