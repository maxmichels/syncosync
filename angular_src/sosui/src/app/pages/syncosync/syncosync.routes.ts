import { NgModule } from '@angular/core';
import { StatusComponent } from './pages/status/status.component';
import { RouterModule, Routes } from '@angular/router';
import { NetworkSetupComponent } from './pages/network-setup/network-setup.component';
import { SyncosyncComponent } from './syncosync.component';
import { SyncSetupComponent } from './pages/sync-setup/sync-setup.component';
import { AdminPasswordComponent } from './pages/admin-password/admin-password.component';
import { EmailSetupComponent } from './pages/email-setup/email-setup.component';
import { SystemSetupComponent } from './pages/system-setup/system-setup.component';

const routes: Routes = [
  {
    path: '',
    component: SyncosyncComponent,
    children: [
      { path: '', component: StatusComponent },
      { path: 'status', component: StatusComponent },
      { path: 'networksetup', component: NetworkSetupComponent },
      { path: 'syncsetup', component: SyncSetupComponent },
      { path: 'adminpassword', component: AdminPasswordComponent },
      { path: 'mailsetup', component: EmailSetupComponent },
      { path: 'systemsetup', component: SystemSetupComponent },
    ],
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SyncosyncRoutes {}
