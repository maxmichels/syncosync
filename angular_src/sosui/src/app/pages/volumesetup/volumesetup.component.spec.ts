import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VolumesetupComponent } from './volumesetup.component';

describe('DisksetupComponent', () => {
  let component: VolumesetupComponent;
  let fixture: ComponentFixture<VolumesetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VolumesetupComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VolumesetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
