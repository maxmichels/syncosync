import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './helpers/guards/auth.guard';
import { StateSetupDisksGuard } from './helpers/guards/state-setup-disks.guard';
import { StateRemoteRecoveryGuard } from './helpers/guards/state-remote-recovery.guard';
import { StateVolumeSetupGuard } from './helpers/guards/state-volume-setup.guard';

const syncosyncModule = () =>
  import('./pages/syncosync/syncosync.module').then((x) => x.SyncosyncModule);
const authModule = () =>
  import('./pages/auth/auth.module').then((x) => x.AuthModule);
const disksetupModule = () =>
  import('./pages/disksetup/disksetup.module').then((x) => x.DisksetupModule);
const remoteRecoveryModule = () =>
  import('./pages/remote-recovery/remote-recovery.module').then(
    (x) => x.RemoteRecoveryModule
  );
const volumeSetupModule = () =>
  import('./pages/volumesetup/volumesetup.module').then(
    (x) => x.VolumesetupModule
  );
/*const esaInitialModule = () =>
  import('./pages/esa-initial/esa-initial.module').then(
    (x) => x.EsaInitialModule
  );
*/
const routes: Routes = [
  { path: 'auth', loadChildren: authModule },
  {
    path: 'syncosync',
    loadChildren: syncosyncModule,
    canActivate: [AuthGuard, StateSetupDisksGuard, StateRemoteRecoveryGuard],
  },
  {
    path: 'remote-recovery',
    loadChildren: remoteRecoveryModule,
    canActivate: [AuthGuard, StateSetupDisksGuard, StateVolumeSetupGuard],
  },
  {
    path: 'disksetup',
    loadChildren: disksetupModule,
    canActivate: [AuthGuard, StateRemoteRecoveryGuard, StateVolumeSetupGuard],
  },
  {
    path: 'volumesetup',
    loadChildren: volumeSetupModule,
    canActivate: [AuthGuard, StateSetupDisksGuard, StateRemoteRecoveryGuard],
  },
  // otherwise redirect to home
  { path: '**', redirectTo: 'auth' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
