import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SyncosyncInfoModalButtonComponent } from './syncosync-info-modal-button.component';

describe('InfoModalButtonComponent', () => {
  let component: SyncosyncInfoModalButtonComponent;
  let fixture: ComponentFixture<SyncosyncInfoModalButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SyncosyncInfoModalButtonComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SyncosyncInfoModalButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
