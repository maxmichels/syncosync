import { Component } from '@angular/core';
import {
  AbstractEditableFormDirective,
  SupportedLanguage,
  SyncosyncModalService,
  UiLanguage,
} from '@syncosync_common';
import { Observable } from 'rxjs';
import { UiLanguageService } from '../../services/ui-language.service';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';

@Component({
  selector: 'app-language-switch',
  templateUrl: './language-switch.component.html',
  styleUrls: ['./language-switch.component.css'],
})
export class LanguageSwitchComponent extends AbstractEditableFormDirective<UiLanguage> {
  supportedLanguages = SupportedLanguage;

  constructor(
    private uiLanguageService: UiLanguageService,
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService
  ) {
    super(syncosyncModalService, processingSpinnerStateService);
  }

  protected getInitModel(): UiLanguage {
    return new UiLanguage();
  }

  protected modelRequest(): Observable<UiLanguage> {
    return this.uiLanguageService.getLanguage();
  }

  onSubmit(): void {
    this.uiLanguageService.changeLanguage(this.model);
  }
}
