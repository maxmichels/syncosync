import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../services/notification.service';
import { SystemMode } from '@syncosync_common';

@Component({
  selector: 'app-single-line-notification',
  templateUrl: './single-line-notification.component.html',
  styleUrls: ['./single-line-notification.component.css'],
})
export class SingleLineNotificationComponent implements OnInit {
  systemModeEnum = SystemMode;

  constructor(public notificationService: NotificationService) {}

  ngOnInit(): void {
    //
  }
}
