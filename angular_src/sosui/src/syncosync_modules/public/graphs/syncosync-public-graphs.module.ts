import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NetspeedGraphComponent } from '@syncosync_public';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [NetspeedGraphComponent],
  imports: [CommonModule, ChartsModule, NgbModule],
  exports: [NetspeedGraphComponent],
})
export class SyncosyncPublicGraphsModule {}
