import { Injectable } from '@angular/core';
import { SystemMode } from '@syncosync_common';
import { SysstateService } from '@syncosync_public';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  private permanentSingleLineNotification = null;

  constructor(private sysstateService: SysstateService) {
    this.sysstateService.getSysstateObservable().subscribe((latestSysstate) => {
      this.setStateNotification(latestSysstate.actmode);
    });
  }

  public setStateNotification(state: SystemMode): void {
    this.permanentSingleLineNotification = state;
  }

  public getPermanentSingleLineNotification(): SystemMode {
    return this.permanentSingleLineNotification;
  }
}
