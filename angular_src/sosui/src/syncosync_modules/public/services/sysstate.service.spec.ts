import { TestBed } from '@angular/core/testing';

import { SysstateService } from './sysstate.service';

describe('SysstateService', () => {
  let service: SysstateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SysstateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
