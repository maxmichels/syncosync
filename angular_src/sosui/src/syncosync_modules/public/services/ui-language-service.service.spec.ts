import {TestBed} from '@angular/core/testing';

import {UiLanguageService} from './ui-language.service';

describe('UiLanguageServiceService', () => {
    let service: UiLanguageService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(UiLanguageService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
