import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, timer } from 'rxjs';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError, switchMap, tap } from 'rxjs/operators';
import {
  AuthService,
  SosServiceBaseUnauthenticatedService,
  SysStateModel,
  SysStateTransport,
} from '@syncosync_common';
import { SysStateResult } from '../../common/model/sysState';

@Injectable({
  providedIn: 'root',
})
export class SysstateService extends SosServiceBaseUnauthenticatedService {
  newSysstateSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
    new SysStateModel()
  );
  latestSysstateObservable$: Observable<SysStateModel> =
    this.newSysstateSubject.asObservable();
  sysstateInterval: Observable<number> = null;
  protected serviceName = 'SysstateService';
  private latestSysstate: SysStateModel = new SysStateModel();

  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);

    // We never pause this
    this.sysstateInterval = timer(0, 5000);
    this.sysstateInterval
      .pipe(switchMap(() => this.getSysstate()))
      .subscribe((res) => {
        if (res === undefined) {
          return;
        }

        Object.assign(this.latestSysstate, res);
        this.newSysstateSubject.next(res);
      });
  }

  public getLatestSysstate(): SysStateModel {
    return this.latestSysstate;
  }

  public getSysstateObservable(): Observable<SysStateModel> {
    return this.latestSysstateObservable$;
  }

  private getSysstate(): Observable<SysStateModel> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .get<SysStateModel>(syncosyncEndpoints.public.sysstateUrl, options)
      .pipe(
        tap((_) => this.log('Got sysstate')),
        catchError(this.handleError<SysStateModel>('getSysstate'))
      );
  }

  /**
   * Allows to set the sys state without previous authorization for special use cases.
   * E.g. DisasterSwap along with a config backup password
   * @param modeToSet only allows disaster swap
   */
  public setMode(modeToSet: SysStateTransport): Observable<SysStateResult> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<SysStateResult>(
        syncosyncEndpoints.public.sysstateUrl,
        modeToSet,
        options
      )
      .pipe(
        tap((_) => this.log('Update mode')),
        catchError(this.handleError<SysStateResult>('setMode'))
      );
  }
}
