import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {
  AuthService,
  SosServiceBaseUnauthenticatedService,
  UiLanguage,
} from '@syncosync_common';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class UiLanguageService extends SosServiceBaseUnauthenticatedService {
  protected serviceName = 'UiLanguageService';

  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService,
    protected router: Router
  ) {
    super(httpClient, authService);
  }

  public changeLanguage(languageToBeSet: UiLanguage): void {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    this.httpClient
      .post(syncosyncEndpoints.public.uiLanguageUrl, languageToBeSet, options)
      .pipe(
        tap(() => this.log('Changed language')),
        catchError(this.handleError<never>('changeLanguage'))
      )
      .subscribe(() => {
        document.location.href = this.router.url;
      });
  }

  public getLanguage(): Observable<UiLanguage> {
    return this.httpClient
      .get<UiLanguage>(syncosyncEndpoints.public.uiLanguageUrl)
      .pipe(catchError(this.handleError<UiLanguage>('getLanguage')));
  }
}
