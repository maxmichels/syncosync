import { Component } from '@angular/core';
import { Hostname, SetupAssistantDirective } from '@syncosync_common';

@Component({
  selector: 'app-hostname-setup-partial',
  templateUrl: './hostname-setup-partial.component.html',
  styleUrls: ['./hostname-setup-partial.component.css'],
})
export class HostnameSetupPartialComponent extends SetupAssistantDirective<Hostname> {
  constructor() {
    super();
    this.canBeSkipped = true;
    this.canBeReturnedTo = true;
  }
}
