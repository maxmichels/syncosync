import { Component, OnInit } from '@angular/core';
import { MailConfig, SetupAssistantDirective } from '@syncosync_common';

@Component({
  selector: 'app-mail-configuration-setup-partial',
  templateUrl: './mail-configuration-setup-partial.component.html',
  styleUrls: ['./mail-configuration-setup-partial.component.css'],
})
export class MailConfigurationSetupPartialComponent
  extends SetupAssistantDirective<MailConfig>
  implements OnInit
{
  constructor() {
    super();
    this.canBeSkipped = true;
    this.canBeReturnedTo = true;
  }

  ngOnInit(): void {
    // nop
  }
}
