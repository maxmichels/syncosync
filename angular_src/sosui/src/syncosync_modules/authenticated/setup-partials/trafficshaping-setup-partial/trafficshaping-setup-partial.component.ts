import { Component, OnInit } from '@angular/core';
import { SetupAssistantDirective, Trafficshape } from '@syncosync_common';

@Component({
  selector: 'app-trafficshaping-setup-partial',
  templateUrl: './trafficshaping-setup-partial.component.html',
  styleUrls: ['./trafficshaping-setup-partial.component.css'],
})
export class TrafficshapingSetupPartialComponent extends SetupAssistantDirective<Trafficshape> {
  constructor() {
    super();
    this.canBeSkipped = true;
    this.canBeReturnedTo = true;
  }
}
