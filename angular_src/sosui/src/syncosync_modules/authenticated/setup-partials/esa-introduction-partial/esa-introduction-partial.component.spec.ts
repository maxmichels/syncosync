import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsaIntroductionPartialComponent } from './esa-introduction-partial.component';

describe('EsaIntroductionPartialComponent', () => {
  let component: EsaIntroductionPartialComponent;
  let fixture: ComponentFixture<EsaIntroductionPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EsaIntroductionPartialComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsaIntroductionPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
