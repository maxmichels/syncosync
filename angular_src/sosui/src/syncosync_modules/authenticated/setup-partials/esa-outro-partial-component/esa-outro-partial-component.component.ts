import { Component } from '@angular/core';
import { SetupAssistantDirective } from '@syncosync_common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-esa-outro-partial-component',
  templateUrl: './esa-outro-partial-component.component.html',
  styleUrls: ['./esa-outro-partial-component.component.css'],
})
export class EsaOutroPartialComponentComponent extends SetupAssistantDirective<null> {
  canBeSkipped = false;
  canBeReturnedTo = false;

  constructor(private router: Router) {
    super();
    //
  }

  redirectToStatus(): void {
    this.router.navigate(['/syncosync'], {});
  }
}
