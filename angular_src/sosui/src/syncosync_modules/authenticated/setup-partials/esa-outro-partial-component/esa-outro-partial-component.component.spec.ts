import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EsaOutroPartialComponentComponent } from './esa-outro-partial-component.component';

describe('EsaOutroPartialComponentComponent', () => {
  let component: EsaOutroPartialComponentComponent;
  let fixture: ComponentFixture<EsaOutroPartialComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EsaOutroPartialComponentComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EsaOutroPartialComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
