import { Component, OnInit } from '@angular/core';
import { SetupAssistantDirective, SshConfig } from '@syncosync_common';

@Component({
  selector: 'app-ssh-port-setup-partial',
  templateUrl: './ssh-port-setup-partial.component.html',
  styleUrls: ['./ssh-port-setup-partial.component.css'],
})
export class SshPortSetupPartialComponent
  extends SetupAssistantDirective<SshConfig>
  implements OnInit
{
  constructor() {
    super();
    this.canBeSkipped = true;
    this.canBeReturnedTo = true;
  }

  ngOnInit(): void {
    // nop
  }
}
