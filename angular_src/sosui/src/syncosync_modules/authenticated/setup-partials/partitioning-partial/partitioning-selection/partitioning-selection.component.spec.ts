import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartitioningSelectionComponent } from './partitioning-selection.component';

describe('PartitioningSelectionComponent', () => {
  let component: PartitioningSelectionComponent;
  let fixture: ComponentFixture<PartitioningSelectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PartitioningSelectionComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartitioningSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
