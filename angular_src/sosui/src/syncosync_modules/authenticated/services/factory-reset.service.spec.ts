import { TestBed } from '@angular/core/testing';

import { FactoryResetService } from './factory-reset.service';

describe('FactoryResetService', () => {
  let service: FactoryResetService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FactoryResetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
