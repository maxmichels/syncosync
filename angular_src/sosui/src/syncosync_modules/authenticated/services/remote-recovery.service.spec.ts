import { TestBed } from '@angular/core/testing';

import { RemoteRecoveryService } from './remote-recovery.service';

describe('RemoteRecoveryService', () => {
  let service: RemoteRecoveryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RemoteRecoveryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
