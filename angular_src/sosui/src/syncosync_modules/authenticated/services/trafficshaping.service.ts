import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {
  AuthService,
  SosServiceBaseAuthenticatedService,
  Trafficshape,
} from '@syncosync_common';
import { Observable } from 'rxjs';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError, tap } from 'rxjs/operators';
import { TrafficShapeSwitchModel } from '../../common/model/trafficshape';

@Injectable({
  providedIn: 'root',
})
export class TrafficshapingService extends SosServiceBaseAuthenticatedService {
  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
  }

  public getTrafficshape(): Observable<Trafficshape> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .get<Trafficshape>(
        syncosyncEndpoints.authenticated.trafficshapingURL,
        options
      )
      .pipe(
        tap((_) => this.log('Got Trafficshape')),
        catchError(this.handleError<Trafficshape>('getTrafficshape'))
      );
  }

  public setTrafficshape(
    newTrafficshape: Trafficshape
  ): Observable<Trafficshape> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<Trafficshape>(
        syncosyncEndpoints.authenticated.trafficshapingURL,
        newTrafficshape,
        options
      )
      .pipe(
        tap((_) => this.log('upate trafficshape')),
        catchError(this.handleError<Trafficshape>('setTrafficshape'))
      );
  }

  public getTrafficshapeMode(): Observable<TrafficShapeSwitchModel> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .get<TrafficShapeSwitchModel>(
        syncosyncEndpoints.authenticated.trafficshapingModeURL,
        options
      )
      .pipe(
        tap((_) => this.log('Got TrafficShapeSwitchModel')),
        catchError(
          this.handleError<TrafficShapeSwitchModel>('getTrafficshapeMode')
        )
      );
  }

  public setTrafficshapeMode(
    newTrafficshapeMode: TrafficShapeSwitchModel
  ): Observable<TrafficShapeSwitchModel> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<TrafficShapeSwitchModel>(
        syncosyncEndpoints.authenticated.trafficshapingModeURL,
        newTrafficshapeMode,
        options
      )
      .pipe(
        tap((_) => this.log('Update TrafficShapeSwitchModel')),
        catchError(
          this.handleError<TrafficShapeSwitchModel>('setTrafficshapeMode')
        )
      );
  }

  protected serviceName: string;
}
