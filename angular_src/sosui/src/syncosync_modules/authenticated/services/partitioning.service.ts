import { Injectable } from '@angular/core';
import {
  PartitioningData,
  SystemSetupData,
} from '../../common/model/systemSetupData';
import { Observable } from 'rxjs';
import {
  AuthService,
  Drives,
  GenericUiResponse,
  SosServiceBaseAuthenticatedService,
} from '@syncosync_common';
import { HttpClient, HttpParams } from '@angular/common/http';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PartitioningService extends SosServiceBaseAuthenticatedService {
  protected serviceName = 'PartitioningService';

  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
  }

  partitionDrives(
    partitioningConfiguration: PartitioningData
  ): Observable<GenericUiResponse> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<GenericUiResponse>(
        syncosyncEndpoints.authenticated.partitioningUrl,
        partitioningConfiguration,
        options
      )
      .pipe(
        tap((_) => this.log('Sent partitioning configuration')),
        catchError(this.handleError<GenericUiResponse>('partitionDrives'))
      );
  }

  leadPartitioning(): Observable<SystemSetupData> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<SystemSetupData>(
        syncosyncEndpoints.authenticated.partitioningLeadUrl,
        options
      )
      .pipe(
        tap((_) => this.log('Sent partitioning as leader')),
        catchError(this.handleError<SystemSetupData>('leadPartitioning'))
      );
  }

  getPartitioning() {
    return this.httpClient
      .get<PartitioningData>(syncosyncEndpoints.authenticated.partitioningUrl)
      .pipe(catchError(this.handleError<PartitioningData>('getPartitioning')));
  }
}
