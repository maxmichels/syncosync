import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {syncosyncEndpoints} from "../../../environments/endpoints";
import {catchError, tap} from "rxjs/operators";
import {AdminPassword, AuthService, GenericUiResponse, SosServiceBaseAuthenticatedService} from "@syncosync_common";


@Injectable({
    providedIn: 'root'
})
export class AdminManagementService extends SosServiceBaseAuthenticatedService {
    protected serviceName: string = "AdminManagamentService";

    constructor(
        protected httpClient: HttpClient,
        protected authService: AuthService
    ) {
        super(httpClient, authService);
    }

    public setAdminPassword(adminPassword: AdminPassword): Observable<GenericUiResponse> {
        const params = new HttpParams();
        const options = {
            params,
            reportProgress: true
        }

        return this.httpClient.post<GenericUiResponse>(syncosyncEndpoints.authenticated.adminPasswordUrl, adminPassword, options)
            .pipe(
                tap(_ => this.log('Password Change')),
                catchError(this.handleError<GenericUiResponse>('setAdminPassword'))
            );
    }

}
