import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {syncosyncEndpoints} from '../../../environments/endpoints';
import {AuthService, LoginState} from "../../common/services/auth.service";
import {Hostname, NicConfig, ResolvConfig, SosServiceBaseAuthenticatedService, SshConfig} from "@syncosync_common";


@Injectable({
    providedIn: 'root'
})
export class NetworkConfigService extends SosServiceBaseAuthenticatedService {
    protected serviceName = 'NetworkConfigService';

    constructor(
        protected httpClient: HttpClient,
        protected authService: AuthService
    ) {
        super(httpClient, authService);
    }

    public notifyLoginState(loginState: LoginState) {
        // nothing since we do not subscribe actively
    }

    public getHostname(): Observable<Hostname> {
        const params = new HttpParams();

        const options = {
            params,
            reportProgress: true,
        };
        return this.httpClient.get<Hostname>(syncosyncEndpoints.authenticated.hostnameUrl, options)
            .pipe(
                tap(_ => this.log('Got hostname')),
                catchError(this.handleError<Hostname>('getHostname'))
            );
    }

    public setHostname(newHostname: Hostname): Observable<Hostname> {
        const params = new HttpParams();

        const options = {
            params,
            reportProgress: true,
        };
        return this.httpClient.post<Hostname>(syncosyncEndpoints.authenticated.hostnameUrl, newHostname, options)
            .pipe(
                tap(_ => this.log('Update hostname')),
                catchError(this.handleError<Hostname>('setHostname'))
            );
    }

    getResolvConf(): Observable<ResolvConfig> {
        const params = new HttpParams();

        const options = {
            params,
            reportProgress: true,
        };
        return this.httpClient.get<ResolvConfig>(syncosyncEndpoints.authenticated.resolvConfigUrl, options)
            .pipe(
                tap(_ => this.log('Got ResolvConfig')),
                catchError(this.handleError<ResolvConfig>('getResolvConf'))
            );
    }

    public setResolvConf(newResolvConf: ResolvConfig): Observable<ResolvConfig> {
        const params = new HttpParams();

        const options = {
            params,
            reportProgress: true,
        };
        return this.httpClient.post<ResolvConfig>(syncosyncEndpoints.authenticated.resolvConfigUrl, newResolvConf, options)
            .pipe(
                tap(_ => this.log('Update resolvConf')),
                catchError(this.handleError<ResolvConfig>('setResolvConf'))
            );
    }

    getNetworkInterfaces(): Observable<NicConfig[]> {
        const params = new HttpParams();

        const options = {
            params,
            reportProgress: true,
        };
        return this.httpClient.get<NicConfig[]>(syncosyncEndpoints.authenticated.networkInterfacesConfigUrl, options)
            .pipe(
                tap(_ => this.log('Got NIC configs')),
                catchError(this.handleError<NicConfig[]>('getNetworkInterfaceConfigs'))
            );
    }

    setNetworkInterfaces(nicConfigs: NicConfig[]) {
        const params = new HttpParams();

        const options = {
            params,
            reportProgress: true,
        };
        return this.httpClient.post<NicConfig[]>(syncosyncEndpoints.authenticated.networkInterfacesConfigUrl, nicConfigs, options)
            .pipe(
                tap(_ => this.log('Update nicConfigs')),
                catchError(this.handleError<NicConfig[]>('setNetworkInterfaces'))
            );
    }

    getSshConfig(): Observable<SshConfig> {
        const params = new HttpParams();

        const options = {
            params,
            reportProgress: true,
        };
        return this.httpClient.get<SshConfig>(syncosyncEndpoints.authenticated.sshConfigUrl, options)
            .pipe(
                tap(_ => this.log('Got SSH config')),
                catchError(this.handleError<SshConfig>('getSshConfig'))
            );
    }

    setSshConfig(sshConfig: SshConfig) {
        const params = new HttpParams();

        const options = {
            params,
            reportProgress: true,
        };
        return this.httpClient.post<SshConfig>(syncosyncEndpoints.authenticated.sshConfigUrl, sshConfig, options)
            .pipe(
                tap(_ => this.log('Update nicConfigs')),
                catchError(this.handleError<SshConfig>('setNetworkInterfaces'))
            );
    }

}
