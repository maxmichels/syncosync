import { TestBed } from '@angular/core/testing';

import { SyncSpeedSelectionService } from './sync-speed-selection.service';

describe('SyncSpeedSelectionService', () => {
  let service: SyncSpeedSelectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SyncSpeedSelectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
