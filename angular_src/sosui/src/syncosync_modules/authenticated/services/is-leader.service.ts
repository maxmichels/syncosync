import { Injectable } from '@angular/core';
import {
  AuthService,
  SosServiceBaseAuthenticatedService,
} from '@syncosync_common';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError } from 'rxjs/operators';
import { IsLeader } from '@syncosync_common';

@Injectable({
  providedIn: 'root',
})
export class IsLeaderService extends SosServiceBaseAuthenticatedService {
  protected serviceName = 'IsLeaderService';

  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
  }

  public isLeader(): Observable<IsLeader> {
    return this.httpClient
      .get<IsLeader>(syncosyncEndpoints.authenticated.isLeaderUrl)
      .pipe(catchError(this.handleError<IsLeader>('isLeader')));
  }
}
