import { Injectable } from '@angular/core';
import {
  AuthService,
  GenericUiResponse,
  SosServiceBaseAuthenticatedService,
} from '@syncosync_common';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { SosToCheck } from '@syncosync_common';
import { Observable } from 'rxjs';
import { SysStateResult } from 'src/syncosync_modules/common/model/sysState';

@Injectable({
  providedIn: 'root',
})
export class RemoteRecoveryService extends SosServiceBaseAuthenticatedService {
  protected serviceName = 'RemoteRecoveryService';

  constructor(private http: HttpClient, protected authService: AuthService) {
    super(http, authService);
  }

  getProgress(): Observable<SosToCheck> {
    return this.httpClient
      .get<SosToCheck>(
        syncosyncEndpoints.authenticated.getRemoteRecoveryProgress
      )
      .pipe(catchError(this.handleError<SosToCheck>('getProgress')));
  }

  getRRResult(): Observable<SysStateResult> {
    return this.httpClient
      .get<SysStateResult>(syncosyncEndpoints.authenticated.getRRResult)
      .pipe(catchError(this.handleError<SysStateResult>('getRRResult')));
  }

  cancelRemoteRecovery(): Observable<GenericUiResponse> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<GenericUiResponse>(
        syncosyncEndpoints.authenticated.cancelRemoteRecovery,
        options
      )
      .pipe(
        tap((_) => this.log('Sent cancel RemoteRecovery request')),
        catchError(this.handleError<GenericUiResponse>('cancelRemoteRecovery'))
      );
  }
}
