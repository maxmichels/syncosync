import { TestBed } from '@angular/core/testing';

import { ConfigurationBackupManagementService } from './configuration-backup-management.service';

describe('ConfigurationBackupManagementService', () => {
  let service: ConfigurationBackupManagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConfigurationBackupManagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
