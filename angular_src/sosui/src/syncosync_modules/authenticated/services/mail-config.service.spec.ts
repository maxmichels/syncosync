import {TestBed} from '@angular/core/testing';

import {MailConfigService} from './mail-config.service';

describe('MailConfigService', () => {
    let service: MailConfigService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(MailConfigService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
