import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { syncosyncEndpoints } from '../../../environments/endpoints';
import { catchError, tap } from 'rxjs/operators';
import {
  AuthService,
  GenericUiResponse,
  MailConfig,
  SosServiceBaseAuthenticatedService,
} from '@syncosync_common';

@Injectable({
  providedIn: 'root',
})
export class MailConfigService extends SosServiceBaseAuthenticatedService {
  protected serviceName = 'NetworkConfigService';

  constructor(
    protected httpClient: HttpClient,
    protected authService: AuthService
  ) {
    super(httpClient, authService);
  }

  public getMailConfig(): Observable<MailConfig> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .get<MailConfig>(syncosyncEndpoints.authenticated.mailConfigUrl, options)
      .pipe(
        tap((_) => this.log('Got Mailconfig')),
        catchError(this.handleError<MailConfig>('getMailconfig'))
      );
  }

  public setMailConfig(newMailConfig: MailConfig): Observable<MailConfig> {
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
    };
    return this.httpClient
      .post<MailConfig>(
        syncosyncEndpoints.authenticated.mailConfigUrl,
        newMailConfig,
        options
      )
      .pipe(
        tap((_) => this.log('upate mailConfig')),
        catchError(this.handleError<MailConfig>('setMailConfig'))
      );
  }

  public sendTestMail(): Observable<GenericUiResponse> {
    return this.httpClient
      .post<GenericUiResponse>(
        syncosyncEndpoints.authenticated.mailTestMail,
        null
      )
      .pipe(catchError(this.handleError<GenericUiResponse>('sendTestMail')));
  }
}
