import { TestBed } from '@angular/core/testing';

import { PartitioningService } from './partitioning.service';

describe('PartitioningService', () => {
  let service: PartitioningService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartitioningService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
