import { Component, Input, OnInit } from '@angular/core';
import { AccountInfo, Calculators, Status } from '@syncosync_common';

@Component({
  selector: 'app-accountstatus-overdue',
  templateUrl: './accountstatus-overdue.component.html',
  styleUrls: ['./accountstatus-overdue.component.css'],
})
export class AccountstatusOverdueComponent implements OnInit {
  @Input() account: AccountInfo;
  @Input() status?: Status;
  public updateOverdue = 0;

  constructor() {
    // nop
  }

  ngOnInit(): void {
    if (this.status !== undefined) {
      // console.log("this.status.date", this.status.date)
      // console.log("this.account.last_access", this.account.last_access)
      this.updateOverdue = Calculators.secToDays(
        this.status.date - this.account.last_access
      );
    }
  }

  getColor(): string {
    if (this.updateOverdue > this.account.remind_duration) {
      return 'error';
    } else {
      return 'ok';
    }
  }
}
