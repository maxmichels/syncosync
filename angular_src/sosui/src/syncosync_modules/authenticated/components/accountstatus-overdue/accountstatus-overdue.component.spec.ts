import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AccountstatusOverdueComponent} from './accountstatus-overdue.component';

describe('AccountstatusOverdueComponent', () => {
    let component: AccountstatusOverdueComponent;
    let fixture: ComponentFixture<AccountstatusOverdueComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AccountstatusOverdueComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AccountstatusOverdueComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
