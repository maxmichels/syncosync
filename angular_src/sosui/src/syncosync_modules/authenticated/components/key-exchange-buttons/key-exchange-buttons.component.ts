import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { SyncConfigurationService } from '../../services/sync-configuration.service';
import {
  ConfirmationModalResult,
  GenericUiResponse,
  GenericUiResponseStatus,
  HelpStateService,
  InfoLevel,
  LocalizedMessage,
  SetupAssistantDirective,
  SshFingerprint,
  SyncosyncModalService,
  SystemSetupData,
} from '@syncosync_common';

@Component({
  selector: 'app-key-exchange-buttons',
  templateUrl: './key-exchange-buttons.component.html',
  styleUrls: ['./key-exchange-buttons.component.css'],
})
export class KeyExchangeButtonsComponent
  extends SetupAssistantDirective<SystemSetupData>
  implements OnInit
{
  @Input() disableButtons = true;

  constructor(
    public syncConfigurationService: SyncConfigurationService,
    private modalService: SyncosyncModalService,
    public helpStateService: HelpStateService
  ) {
    super();
  }

  ngOnInit(): void {
    // nop
  }

  initiateLeadKeyExchange(): void {
    const resultEmitter: EventEmitter<SystemSetupData> =
      new EventEmitter<SystemSetupData>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined && result !== null) {
        this.acceptOppositeKeyDialog(result);
      }
    });
    this.modalService.showProcessingModal<SystemSetupData>(
      LocalizedMessage.SYNC_SETUP_LEADING_KEY_EXCHANGE_PROCESSING_MODAL_TITLE,
      LocalizedMessage.SYNC_SETUP_LEADING_KEY_EXCHANGE_PROCESSING_MODAL_CONTENT,
      this.syncConfigurationService.leadKeyExchange(),
      this.syncConfigurationService.cancelKeyExchange(),
      resultEmitter
    );
  }

  followKeyExchange(): void {
    const resultEmitter: EventEmitter<SystemSetupData> =
      new EventEmitter<SystemSetupData>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined && result !== null) {
        this.acceptOppositeKeyDialog(result);
      }
    });
    this.modalService.showProcessingModal<SystemSetupData>(
      LocalizedMessage.SYNC_SETUP_FOLLOW_KEY_EXCHANGE_PROCESSING_MODAL_TITLE,
      LocalizedMessage.SYNC_SETUP_FOLLOW_KEY_EXCHANGE_PROCESSING_MODAL_CONTENT,
      this.syncConfigurationService.followKeyExchange(),
      this.syncConfigurationService.cancelKeyExchange(),
      resultEmitter
    );
  }

  private acceptOppositeKeyDialog(result: SystemSetupData) {
    this.modalService
      .showConfirmationModal(
        LocalizedMessage.KEY_EXCHANGE_ACCEPT_REMOTE_KEY_TITLE,
        LocalizedMessage.KEY_EXCHANGE_ACCEPT_REMOTE_KEY_CONFIRMATION_EXPLANATION,
        [
          ' ',
          'Remote: ' + SshFingerprint.fingerprint(result.pub_key),
          'Local: ' + this.syncConfigurationService.getLocalKey().fingerprint,
        ]
      )
      .subscribe((userDecision) => {
        if (userDecision === ConfirmationModalResult.CONFIRM) {
          this.acceptOppositeKey(result);
        } else {
          this.modalService.showInfoModal(
            InfoLevel.WARNING,
            LocalizedMessage.KEY_EXCHANGE_FAILED_REMOTE_KEY_NOT_ACCEPTED_CONFIRMATION_CONTENT
          );
        }
      });
  }

  private acceptOppositeKey(acceptedKey: SystemSetupData) {
    const resultEmitter: EventEmitter<GenericUiResponse> =
      new EventEmitter<GenericUiResponse>();
    resultEmitter.subscribe((result) => {
      if (
        result !== undefined &&
        result.status === GenericUiResponseStatus.OK
      ) {
        if (
          this.configurationFinished.closed ||
          this.configurationFinished.hasError ||
          this.configurationFinished.isStopped
        ) {
          console.log('Crap');
        }
        this.configurationFinished.emit(acceptedKey);
        console.log('Done accepting key...');
      } else {
        this.showAcceptKeyFailed();
      }
    });
    this.modalService.showProcessingModal<GenericUiResponse>(
      LocalizedMessage.SYNC_SETUP_ACCEPT_REMOTE_KEY_PROCESSING_TITLE,
      LocalizedMessage.SYNC_SETUP_ACCEPT_REMOTE_KEY_PROCESSING_CONTENT,
      this.syncConfigurationService.acceptRemoteKey(),
      null,
      resultEmitter
    );
  }

  private showAcceptKeyFailed() {
    this.modalService.showInfoModal(
      InfoLevel.ERROR,
      LocalizedMessage.KEY_EXCHANGE_ACCEPT_REMOTE_KEY_FAILED_CONTENT
    );
  }
}
