import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SyncosyncAuthenticatedGraphsModule } from '../graphs/syncosync-authenticated-graphs.module';
import { SyncosyncCommonModule } from '../../common/syncosync-common.module';
import {
  AccountdetailDropComponent,
  AccountsDropListingComponent,
  AccountstatusDotComponent,
  AccountstatusOverdueComponent,
  KeyExchangeButtonsComponent,
  RemoteKeyComponent,
  SetupPartialAssistantComponent,
  ToggleSetupModeComponent,
} from '@syncosync_authenticated';
import { ConfigurationBackupManagementComponent } from './configuration-backup-management/configuration-backup-management.component';

@NgModule({
  declarations: [
    AccountstatusDotComponent,
    AccountstatusOverdueComponent,
    AccountdetailDropComponent,
    AccountsDropListingComponent,
    KeyExchangeButtonsComponent,
    RemoteKeyComponent,
    SetupPartialAssistantComponent,
    ToggleSetupModeComponent,
    ConfigurationBackupManagementComponent,
  ],
  imports: [
    CommonModule,
    SyncosyncCommonModule,
    NgbModule,
    SyncosyncAuthenticatedGraphsModule,
  ],
  exports: [
    AccountstatusDotComponent,
    AccountstatusOverdueComponent,
    AccountdetailDropComponent,
    AccountsDropListingComponent,
    RemoteKeyComponent,
    KeyExchangeButtonsComponent,
    SetupPartialAssistantComponent,
    ToggleSetupModeComponent,
    ConfigurationBackupManagementComponent,
  ],
})
export class SyncosyncComponentsModule {}
