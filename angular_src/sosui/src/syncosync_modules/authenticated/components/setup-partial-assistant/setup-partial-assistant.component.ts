import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  SetupAssistantDirective,
  SetupPartialDirective,
} from '@syncosync_common';

@Component({
  selector: 'app-setup-partial-assistant',
  templateUrl: './setup-partial-assistant.component.html',
  styleUrls: ['./setup-partial-assistant.component.css'],
})
export class SetupPartialAssistantComponent implements OnInit {
  /**
   * Class to handle multiple SetupPartials in the given order and display one after each other upon successful submit
   * Needs to ONLY be loaded if any components are to be shown (ngIf in the html you define it to be viewed)
   */
  @ViewChild(SetupPartialDirective, { static: true })
  setupPartialDirective: SetupPartialDirective;
  currentPartialIndex = -1;
  @Input() setupPartials = [];
  // TODO: Dynamically read it off the forms themselves?
  showSkipButton = false;
  showBackButton = false;
  previousComponentRef: ComponentRef<SetupAssistantDirective<any>> = null;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {}

  ngOnInit(): void {
    this.loadComponent();
  }

  loadComponent(forward = true): void {
    if (forward) {
      this.currentPartialIndex += 1;
    } else {
      this.currentPartialIndex -= 1;
    }
    if (this.currentPartialIndex < 0) {
      this.currentPartialIndex = 0;
    }
    const setupPartialItemClass = this.setupPartials[this.currentPartialIndex];
    if (this.currentPartialIndex > 0) {
      this.showBackButton =
        this.previousComponentRef.instance.isCanBeReturnedTo();
    } else {
      this.showBackButton = false;
    }

    const viewContainerRef = this.setupPartialDirective.viewContainerRef;
    viewContainerRef.clear();
    const componentFactory =
      this.componentFactoryResolver.resolveComponentFactory<
        SetupAssistantDirective<any>
      >(setupPartialItemClass);
    const componentRef =
      viewContainerRef.createComponent<SetupAssistantDirective<any>>(
        componentFactory
      );
    componentRef.instance.configurationFinished.subscribe((value) => {
      if (value === undefined) {
        this.loadComponent(false);
      } else {
        this.loadComponent();
      }
    });
    this.previousComponentRef = componentRef;
    componentRef.instance.canBeSkippedChanged.subscribe((canBeSkipped) => {
      this.showSkipButton = canBeSkipped;
    });
    componentRef.instance.shouldBeSkippedChanged.subscribe(
      (shouldBeSkipped) => {
        if (shouldBeSkipped) {
          // Depending on the above direction passed, we need to move forward or backward
          this.loadComponent(forward);
        }
      }
    );
    this.showSkipButton = componentRef.instance.isCanBeSkipped();
  }
}
