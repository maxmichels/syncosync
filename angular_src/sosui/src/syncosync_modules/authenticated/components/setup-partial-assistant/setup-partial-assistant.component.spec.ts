import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SetupPartialAssistantComponent} from './setup-partial-assistant.component';

describe('SetupPartialAssistantComponent', () => {
    let component: SetupPartialAssistantComponent;
    let fixture: ComponentFixture<SetupPartialAssistantComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SetupPartialAssistantComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SetupPartialAssistantComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
