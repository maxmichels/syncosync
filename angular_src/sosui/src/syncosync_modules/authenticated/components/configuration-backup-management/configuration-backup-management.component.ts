import { Component, OnInit } from '@angular/core';
import { ConfigurationBackupManagementService } from '../../services/configuration-backup-management.service';
import { saveAs } from 'file-saver';
@Component({
  selector: 'app-configuration-backup-management',
  templateUrl: './configuration-backup-management.component.html',
  styleUrls: ['./configuration-backup-management.component.css'],
})
export class ConfigurationBackupManagementComponent {
  constructor(
    private configBackupManagementService: ConfigurationBackupManagementService
  ) {}

  downloadLatestConfigBackupFile(): void {
    this.configBackupManagementService
      .downloadLatestConfigBackup()
      .subscribe((res) => {
        //const url = window.URL.createObjectURL(blob);
        saveAs(res.file, res.filename);
        //window.open(url);
      });
  }
}
