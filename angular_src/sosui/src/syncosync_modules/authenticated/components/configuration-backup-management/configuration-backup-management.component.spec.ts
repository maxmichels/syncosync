import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurationBackupManagementComponent } from './configuration-backup-management.component';

describe('ConfigurationBackupManagementComponent', () => {
  let component: ConfigurationBackupManagementComponent;
  let fixture: ComponentFixture<ConfigurationBackupManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConfigurationBackupManagementComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationBackupManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
