import { Component, EventEmitter } from '@angular/core';
import { SyncConfigurationService } from '../../services/sync-configuration.service';
import {
  AbstractEditableFormDirective,
  ConfirmationModalResult,
  InfoLevel,
  LocalizedMessage,
  HelpStateService,
  SosKey,
  SyncosyncModalService,
} from '@syncosync_common';
import { Observable } from 'rxjs';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';

@Component({
  selector: 'app-remote-key',
  templateUrl: './remote-key.component.html',
  styleUrls: ['./remote-key.component.css'],
})
export class RemoteKeyComponent extends AbstractEditableFormDirective<SosKey> {
  constructor(
    public syncConfigurationService: SyncConfigurationService,
    private modalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService,
    public helpStateService: HelpStateService
  ) {
    super(modalService, processingSpinnerStateService);
  }

  updateRemoteKey(): void {
    const resultEmitter: EventEmitter<SosKey> = new EventEmitter<SosKey>();
    resultEmitter.subscribe((remoteKey) => {
      if (remoteKey !== undefined) {
        this.setModel(remoteKey);
      }
    });
    this.modalService.showProcessingModal<SosKey>(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.REMOTE_KEY_GET_PROCESSING_MODAL_CONTENT,
      this.syncConfigurationService.getRemoteKey(),
      null,
      resultEmitter
    );
  }

  /** Calls for the removal of the remote public key stored on the box in order to not sync with the partner
   * anymore **/
  onSubmit(): void {
    this.modalService
      .showConfirmationModal(
        LocalizedMessage.SYNC_SETUP_TERMINATE_SYNC_DELETE_REMOTE_KEY_TITLE,
        LocalizedMessage.SYNC_SETUP_TERMINATE_SYNC_DELETE_REMOTE_KEY_CONTENT
      )
      .subscribe((userDecision) => {
        if (userDecision == ConfirmationModalResult.CONFIRM) {
          this.removeRemoteKey();
        } else {
          this.modalService.showInfoModal(
            InfoLevel.WARNING,
            LocalizedMessage.SYNC_SETUP_TERMINATE_SYNC_DELETE_REMOTE_KEY_FAILED
          );
        }
      });
  }

  protected getInitModel(): SosKey {
    return new SosKey();
  }

  protected modelRequest(): Observable<SosKey> {
    return this.syncConfigurationService.getRemoteKey();
  }

  private removeRemoteKey() {
    const resultEmitter: EventEmitter<SosKey> = new EventEmitter<SosKey>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined) {
        this.updateRemoteKey();
      } else {
        this.modalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.SYNC_SETUP_TERMINATE_SYNC_DELETE_REMOTE_KEY_FAILED
        );
      }
    });
    this.modalService.showProcessingModal<SosKey>(
      LocalizedMessage.SYNC_SETUP_TERMINATE_SYNC_DELETE_REMOTE_KEY_TITLE,
      LocalizedMessage.SYNC_SETUP_TERMINATE_SYNC_DELETE_REMOTE_KEY_PROCESSING_CONTENT,
      this.syncConfigurationService.removeRemoteKey(),
      null,
      resultEmitter
    );
  }
}
