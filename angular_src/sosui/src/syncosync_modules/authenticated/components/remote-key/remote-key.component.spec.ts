import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RemoteKeyComponent} from './remote-key.component';

describe('RemoteKeyComponent', () => {
    let component: RemoteKeyComponent;
    let fixture: ComponentFixture<RemoteKeyComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RemoteKeyComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RemoteKeyComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
