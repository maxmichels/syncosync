import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AccountstatusDotComponent} from './accountstatus-dot.component';

describe('AccountstatusDotComponent', () => {
    let component: AccountstatusDotComponent;
    let fixture: ComponentFixture<AccountstatusDotComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AccountstatusDotComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AccountstatusDotComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
