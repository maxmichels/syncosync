import { Component, Input, OnInit } from '@angular/core';
import { AccountInfo, Status } from '@syncosync_common';

@Component({
  selector: 'app-accountstatus-dot',
  templateUrl: './accountstatus-dot.component.html',
  styleUrls: ['./accountstatus-dot.component.css'],
})
export class AccountstatusDotComponent implements OnInit {
  @Input() account: AccountInfo;
  @Input() status?: Status;
  public spaceUsedPercentage = 0;

  constructor() {
    // nop
  }

  ngOnInit(): void {
    if (this.status !== undefined) {
      this.spaceUsedPercentage = Math.round(
        (10000 * this.account.space_used) /
          (this.status.total_size * this.account.max_space_allowed)
      );
    }
  }

  getColor(): string {
    if (this.spaceUsedPercentage >= 98) {
      return 'error';
    } else if (this.spaceUsedPercentage >= 90) {
      return 'warning';
    } else {
      return 'ok';
    }
  }
}
