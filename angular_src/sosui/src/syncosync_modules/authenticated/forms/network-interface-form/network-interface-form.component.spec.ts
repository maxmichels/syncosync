import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NetworkInterfaceFormComponent} from './network-interface-form.component';

describe('NetworkInterfaceFormComponent', () => {
    let component: NetworkInterfaceFormComponent;
    let fixture: ComponentFixture<NetworkInterfaceFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NetworkInterfaceFormComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NetworkInterfaceFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
