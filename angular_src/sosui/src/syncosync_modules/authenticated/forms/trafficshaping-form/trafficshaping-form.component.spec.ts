import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrafficshapingFormComponent } from './trafficshaping-form.component';

describe('TrafficshapingFormComponent', () => {
  let component: TrafficshapingFormComponent;
  let fixture: ComponentFixture<TrafficshapingFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TrafficshapingFormComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrafficshapingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
