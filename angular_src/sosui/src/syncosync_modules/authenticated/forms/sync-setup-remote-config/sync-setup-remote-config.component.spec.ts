import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SyncSetupRemoteConfigComponent} from './sync-setup-remote-config.component';

describe('SyncSetupRemoteConfigComponent', () => {
    let component: SyncSetupRemoteConfigComponent;
    let fixture: ComponentFixture<SyncSetupRemoteConfigComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SyncSetupRemoteConfigComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SyncSetupRemoteConfigComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
