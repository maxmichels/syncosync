import { Component, EventEmitter } from '@angular/core';
import { SyncConfigurationService } from '../../services/sync-configuration.service';
import {
  AbstractEditableFormDirective,
  HelpStateService,
  InfoLevel,
  LocalizedMessage,
  RemoteHost,
  SyncosyncModalService,
} from '@syncosync_common';
import { Observable } from 'rxjs';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';

@Component({
  selector: 'app-sync-setup-remote-config',
  templateUrl: './sync-setup-remote-config.component.html',
  styleUrls: ['./sync-setup-remote-config.component.css'],
})
export class SyncSetupRemoteConfigComponent extends AbstractEditableFormDirective<RemoteHost> {
  constructor(
    public syncConfigurationService: SyncConfigurationService,
    public helpStateService: HelpStateService,
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService
  ) {
    super(syncosyncModalService, processingSpinnerStateService);
  }

  /**
   * Simply saves the config
   */
  onSubmit() {
    const resultEmitter: EventEmitter<RemoteHost> =
      new EventEmitter<RemoteHost>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined) {
        console.log(result);
        this.setModel(result);
        this.createFormGroup();
        this.formFinished.emit(this.model);
      } else {
        this.savingRemoteConfigFailedModal();
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.REMOTE_CONFIG_SAVE_PROCESSING_MODAL_CONTENT,
      this.syncConfigurationService.saveRemoteHostConfig(this.model),
      null,
      resultEmitter
    );
  }

  protected createFormGroup(): void {
    // nop
  }

  protected getInitModel(): RemoteHost {
    return new RemoteHost();
  }

  protected modelRequest(): Observable<RemoteHost> {
    return this.syncConfigurationService.getCurrentRemoteHostConfig();
  }

  private savingRemoteConfigFailedModal() {
    this.syncosyncModalService.showInfoModal(
      InfoLevel.ERROR,
      LocalizedMessage.REMOTE_CONFIG_SAVE_FAILED_CONTENT
    );
  }
}
