import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShutdownFormComponent } from './shutdown-form.component';

describe('ShutdownFormComponent', () => {
  let component: ShutdownFormComponent;
  let fixture: ComponentFixture<ShutdownFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ShutdownFormComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShutdownFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
