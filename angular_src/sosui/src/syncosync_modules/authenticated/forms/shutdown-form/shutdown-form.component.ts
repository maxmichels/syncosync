import { Component, EventEmitter } from '@angular/core';
import { AbstractEmptyFormDirective } from '../../../common/components/abstract-empty-form-directive/abstract-empty-form.directive';
import { SystemService } from '@syncosync_authenticated';
import {
  ConfirmationModalResult,
  GenericUiResponse,
  GenericUiResponseStatus,
  InfoLevel,
  LocalizedMessage,
  SyncosyncModalService,
} from '@syncosync_common';

@Component({
  selector: 'app-shutdown-form',
  templateUrl: './shutdown-form.component.html',
  styleUrls: ['./shutdown-form.component.css'],
})
export class ShutdownFormComponent extends AbstractEmptyFormDirective<null> {
  constructor(
    private systemService: SystemService,
    private syncosyncModalService: SyncosyncModalService
  ) {
    super();
  }

  protected getInitModel(): null {
    return null;
  }

  onSubmit(): void {
    this.syncosyncModalService
      .showConfirmationModal(
        LocalizedMessage.SHUTDOWN_SYSTEM_CONFIRMATION_TITLE,
        LocalizedMessage.SHUTDOWN_SYSTEM_CONFIRMATION_CONTENT
      )
      .subscribe((result: ConfirmationModalResult) => {
        if (
          result === undefined ||
          result === null ||
          result === ConfirmationModalResult.CANCEL
        ) {
          // User aborted password input
          return;
        }
        this.shutdownSystem();
      });
  }

  private shutdownSystem(): void {
    const resultEmitter: EventEmitter<GenericUiResponse> =
      new EventEmitter<GenericUiResponse>();
    resultEmitter.subscribe((result) => {
      if (
        result !== undefined &&
        result !== null &&
        result.status === GenericUiResponseStatus.OK
      ) {
        this.formFinished.emit(this.model);
        this.syncosyncModalService.showInfoModal(
          InfoLevel.INFO,
          LocalizedMessage.SHUTDOWN_SYSTEM_SHUTTING_DOWN
        );
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.SHUTDOWN_SYSTEM_FAILED
        );
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.SHUTDOWN_SYSTEM_IN_PROGRESS_TITLE,
      LocalizedMessage.SHUTDOWN_SYSTEM_IN_PROGRESS_TITLE,
      this.systemService.shutdownSystem(),
      null,
      resultEmitter
    );
  }
}
