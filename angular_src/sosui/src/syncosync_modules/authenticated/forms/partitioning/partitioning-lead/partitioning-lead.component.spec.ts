import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartitioningLeadComponent } from './partitioning-lead.component';

describe('PartitioningLeadComponent', () => {
  let component: PartitioningLeadComponent;
  let fixture: ComponentFixture<PartitioningLeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PartitioningLeadComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartitioningLeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
