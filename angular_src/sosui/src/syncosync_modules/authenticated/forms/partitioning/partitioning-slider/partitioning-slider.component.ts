import { Component } from '@angular/core';
import {
  Calculators,
  ConfirmationModalResult,
  InfoLevel,
  LocalizedMessage,
  SyncosyncModalService,
} from '@syncosync_common';
import { PartitioningData } from '@syncosync_common';
import { ChangeContext, Options } from '@angular-slider/ngx-slider';
import { ProcessingSpinnerStateService } from '../../../../common/services/processing-spinner-state.service';
import { PartitioningService } from '../../../services/partitioning.service';
import { AbstractPartitioningDataHandlerDirective } from '../abstract-partitioning-data-handler.directive';
import { SyncConfigurationService } from '@syncosync_authenticated';
import { Router } from '@angular/router';
import { SysstateService } from '@syncosync_public';

@Component({
  selector: 'app-partitioning-slider',
  templateUrl: './partitioning-slider.component.html',
  styleUrls: ['./partitioning-slider.component.scss'],
})
export class PartitioningSliderComponent extends AbstractPartitioningDataHandlerDirective {
  value = 50;
  options: Options = {
    floor: 0,
    ceil: 100,
    step: 5,
    // minLimit to display local_min (allocated data)
    minLimit: 0,
    // maxLimit to display remote_min (allocated data)
    maxLimit: 100,
    showTicks: true,
    showSelectionBar: true,
    // showSelectionBar: true,
    translate: (value: number): string => {
      const totalSize = this.model.free + this.model.remote + this.model.local;
      const approxSize = (totalSize * value) / 100;
      const sizeHumanReadable = Calculators.bytesToHumanReadableSize(
        Calculators.extendsToBytes(approxSize)
      );
      return value + '% (' + sizeHumanReadable + ')';
    },
    getLegend: (value: number): string => {
      if (value % 5 === 0) {
        return value + '%';
      } else {
        return '';
      }
    },
  };

  constructor(
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService,
    protected partitioningService: PartitioningService,
    protected syncConfigurationService: SyncConfigurationService,
    protected router: Router,
    protected sysstateService: SysstateService
  ) {
    super(
      syncosyncModalService,
      processingSpinnerStateService,
      partitioningService,
      router,
      sysstateService
    );
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onUserChangeEnd(_changeContext: ChangeContext): void {
    const totalSize = this.model.free + this.model.remote + this.model.local;
    this.model.local = Math.floor((this.value / 100) * totalSize);
    this.model.remote = totalSize - this.model.local;
    this.model.free = 0;
  }

  public setModel(newModel: PartitioningData): void {
    super.setModel(newModel);

    const totalSize = this.model.free + this.model.remote + this.model.local;
    this.options.maxLimit = Math.floor(
      ((totalSize - this.model.remote_min) / totalSize) * 100
    );
    this.options.minLimit = Math.ceil((this.model.local_min / totalSize) * 100);

    this.options.ticksArray = [];
    for (let i = 0; i <= 100; i += 10) {
      this.options.ticksArray.push(i);
    }

    if (this.model.remote === 0 && this.model.local === 0) {
      // no distribution yet, suggest 50%
      this.value = 50;
    } else {
      // assuming drive have already been partitioned, roughly estimate the percentage of the distribution
      const totalPartitioned = this.model.remote + this.model.local;
      const percent = this.model.local / totalPartitioned;
      this.value = Math.floor(percent * 100);
    }
  }

  onSubmit(): void {
    const totalSize = this.model.free + this.model.remote + this.model.local;
    const modelCopy: PartitioningData = Object.assign({}, this.model);
    const valueDistributionLocalPercentage = this.value;
    modelCopy.local = Math.floor(
      (valueDistributionLocalPercentage / 100) * totalSize
    );
    modelCopy.remote = totalSize - modelCopy.local;
    modelCopy.free = 0;
    if (
      modelCopy.local < modelCopy.local_min ||
      modelCopy.remote < modelCopy.remote_min
    ) {
      console.log('Invalid local and remote values (smaller than min)');
      this.syncosyncModalService.showInfoModal(
        InfoLevel.WARNING,
        LocalizedMessage.PARTITION_DRIVES_FAILED_GENERIC_MESSAGE,
        'Invalid local/remote values (less than min).'
      );
      return;
    }
    this.confirmationDialogForPartitioning(
      valueDistributionLocalPercentage,
      modelCopy
    );
  }

  confirmationDialogForPartitioning(
    valueDistributionLocalPercentage: number,
    partitioningData: PartitioningData
  ): void {
    const remoteSizeHumanReadable = Calculators.bytesToHumanReadableSize(
      Calculators.extendsToBytes(partitioningData.remote)
    );
    const localSizeHumanReadable = Calculators.bytesToHumanReadableSize(
      Calculators.extendsToBytes(partitioningData.local)
    );
    const remotePercentage = 100 - valueDistributionLocalPercentage;
    this.syncosyncModalService
      .showConfirmationModal(
        LocalizedMessage.PARTITION_DRIVES_CONFIRMATION_TITLE,
        LocalizedMessage.PARTITION_DRIVES_CONFIRMATION_CONTENT,
        valueDistributionLocalPercentage +
          '% (' +
          localSizeHumanReadable +
          ') : ' +
          remotePercentage +
          '% (' +
          remoteSizeHumanReadable +
          ')'
      )
      .subscribe((userDecision) => {
        if (userDecision === ConfirmationModalResult.CONFIRM) {
          this.partitionDrives(partitioningData);
        }
      });
  }
}
