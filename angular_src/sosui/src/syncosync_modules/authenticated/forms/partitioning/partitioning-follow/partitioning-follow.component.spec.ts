import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartitioningFollowComponent } from './partitioning-follow.component';

describe('PartitioningFollowComponent', () => {
  let component: PartitioningFollowComponent;
  let fixture: ComponentFixture<PartitioningFollowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PartitioningFollowComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartitioningFollowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
