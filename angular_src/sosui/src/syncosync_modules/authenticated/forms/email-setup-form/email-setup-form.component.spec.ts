import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EmailSetupFormComponent} from './email-setup-form.component';

describe('EmailSetupFormComponent', () => {
    let component: EmailSetupFormComponent;
    let fixture: ComponentFixture<EmailSetupFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [EmailSetupFormComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EmailSetupFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
