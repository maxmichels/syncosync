import { Component, EventEmitter } from '@angular/core';
import { MailConfigService } from '@syncosync_authenticated';
import {
  AbstractEditableFormDirective,
  GenericUiResponse,
  HelpStateService,
  InfoLevel,
  LocalizedMessage,
  MailAuthenticationType,
  MailConfig,
  MailInfoLevels,
  MailSslType,
  SyncosyncModalService,
} from '@syncosync_common';
import { Observable } from 'rxjs';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';

@Component({
  selector: 'app-email-setup-form',
  templateUrl: './email-setup-form.component.html',
  styleUrls: ['./email-setup-form.component.css'],
})
export class EmailSetupFormComponent extends AbstractEditableFormDirective<MailConfig> {
  sslTypeNoSecurity: MailSslType = MailSslType.NO_SECURITY;
  sslTypeSslTls: MailSslType = MailSslType.SSL_TLS;
  sslTypesStarttls: MailSslType = MailSslType.STARTTLS;
  mailInfoLevelNone: MailInfoLevels = MailInfoLevels.NONE;
  mailInfoLevelError: MailInfoLevels = MailInfoLevels.ERROR;
  mailInfoLevelWarning: MailInfoLevels = MailInfoLevels.WARNING;
  mailInfoLevelInfo: MailInfoLevels = MailInfoLevels.INFO;

  authenticationNormal: MailAuthenticationType =
    MailAuthenticationType.PASSWORD_NORMAL;
  authenticationCrypted: MailAuthenticationType =
    MailAuthenticationType.PASSWORD_CRYPTED;

  constructor(
    private mailConfigService: MailConfigService,
    protected syncosyncModalService: SyncosyncModalService,
    public helpStateService: HelpStateService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService
  ) {
    super(syncosyncModalService, processingSpinnerStateService);
  }

  onSubmit(): void {
    const resultEmitter: EventEmitter<MailConfig> =
      new EventEmitter<MailConfig>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined && result !== null) {
        this.setModel(result);
        this.formFinished.emit(this.model);
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.MAILSETUP_SAVE_FAILED_MESSAGE
        );
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.PROCESSING_MAILSETUP_SAVE_MESSAGE,
      this.mailConfigService.setMailConfig(this.model),
      null,
      resultEmitter
    );
  }

  sendTestMail(): void {
    const resultEmitter: EventEmitter<GenericUiResponse> =
      new EventEmitter<GenericUiResponse>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined && result !== null) {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.INFO,
          result.localized_reason,
          result.additional_information
        );
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.TESTMAIL_FAILED_TO_SEND
        );
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.TESTMAIL_SENDING,
      this.mailConfigService.sendTestMail(),
      null,
      resultEmitter
    );
  }

  protected createFormGroup(): void {
    //
  }

  protected modelRequest(): Observable<MailConfig> {
    return this.mailConfigService.getMailConfig();
  }

  protected getInitModel(): MailConfig {
    return new MailConfig();
  }
}
