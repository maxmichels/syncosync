import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountAddFormComponent } from './account-add-form.component';

describe('AccountAddFormComponent', () => {
  let component: AccountAddFormComponent;
  let fixture: ComponentFixture<AccountAddFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AccountAddFormComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
