import { ChangeDetectorRef, Component } from '@angular/core';
import {
  AccountAdd,
  HelpStateService,
  MailInfoLevels,
  SshFingerprint,
} from '@syncosync_common';
import { AbstractEmptyFormDirective } from '../../../common/components/abstract-empty-form-directive/abstract-empty-form.directive';
import { AccountApiService } from '../../services/account-api.service';

@Component({
  selector: 'app-account-add-form',
  templateUrl: './account-add-form.component.html',
  styleUrls: ['./account-add-form.component.css'],
})
export class AccountAddFormComponent extends AbstractEmptyFormDirective<AccountAdd> {
  newFingerprint: string;
  mailInfoLevels = MailInfoLevels;

  constructor(
    public helpStateService: HelpStateService,
    private cd: ChangeDetectorRef,
    private accountApiService: AccountApiService
  ) {
    super();
  }

  protected createFormGroup(): void {
    // nop
  }

  protected getInitModel(): AccountAdd {
    return new AccountAdd();
  }

  onSubmit(): void {
    this.accountApiService.addAccount(this.model).subscribe((result) => {
      if (result) {
        this.formFinished.emit(this.model);
      } else {
        // TODO: show error
      }
    });
  }

  onSSHKeyChange($event): void {
    if ($event.target.files && $event.target.files.length) {
      const [file] = $event.target.files;
      console.log(file);
      // just checking if it is an pub key with file ending .pub or if it's more than 4 MB
      const fileEnding = file.name.substr(file.name.lastIndexOf('.') + 1);
      if (!fileEnding.startsWith('pub') || file.size > 2 ** 12) {
        this.cd.markForCheck();
      } else {
        file.text().then((content) => {
          this.newFingerprint = SshFingerprint.fingerprint(content);
          this.model.ssh_pub_key = content;
        });
        this.cd.markForCheck();
      }
    }
  }

  public isInvalidConfiguration(): boolean {
    const newValidSshKey: boolean =
      this.model.ssh_pub_key !== null && this.model.ssh_pub_key.length > 0;
    const newPassword: boolean =
      this.model.password !== null && this.model.password.length > 0;

    // basically starting from scratch
    return !newPassword && !newValidSshKey;
  }
}
