import { Component, EventEmitter } from '@angular/core';
import { NetworkConfigService } from '../../services/network-config.service';
import {
  AbstractEditableFormDirective,
  ResolvConfig,
  SyncosyncModalService,
  HelpStateService,
  NicConfig,
  InfoLevel,
  LocalizedMessage,
} from '@syncosync_common';
import { Observable } from 'rxjs';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';

@Component({
  selector: 'app-nameserver-form',
  templateUrl: './nameserver-form.component.html',
  styleUrls: ['./nameserver-form.component.css'],
})
export class NameserverFormComponent extends AbstractEditableFormDirective<ResolvConfig> {
  constructor(
    public networkConfigService: NetworkConfigService,
    public helpStateService: HelpStateService,
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService
  ) {
    super(syncosyncModalService, processingSpinnerStateService);
  }

  onSubmit(): void {
    const resultEmitter: EventEmitter<ResolvConfig> =
      new EventEmitter<ResolvConfig>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined && result !== null) {
        this.setModel(result);
        this.formFinished.emit(this.model);
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.NAMESERVER_SAVE_FAILED_MESSAGE
        );
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.PROCESSING_NAMESERVER_SAVE_MESSAGE,
      this.networkConfigService.setResolvConf(this.model),
      null,
      resultEmitter
    );
  }

  protected getInitModel(): ResolvConfig {
    return new ResolvConfig();
  }

  protected modelRequest(): Observable<ResolvConfig> {
    return this.networkConfigService.getResolvConf();
  }
}
