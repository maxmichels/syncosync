import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoteRecoveryConfigBackupFormComponent } from './remote-recovery-config-backup-form.component';

describe('RemoteRecoveryConfigBackupFormComponent', () => {
  let component: RemoteRecoveryConfigBackupFormComponent;
  let fixture: ComponentFixture<RemoteRecoveryConfigBackupFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RemoteRecoveryConfigBackupFormComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoteRecoveryConfigBackupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
