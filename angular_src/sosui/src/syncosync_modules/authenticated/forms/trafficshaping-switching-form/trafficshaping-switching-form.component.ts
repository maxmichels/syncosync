import { Component, EventEmitter } from '@angular/core';
import {
  AbstractEditableFormDirective,
  InfoLevel,
  HelpStateService,
  LocalizedMessage,
  SyncosyncModalService,
} from '@syncosync_common';
import {
  TrafficShapeSwitchModel,
  TrafficShapeSwitchValues,
} from '../../../common/model/trafficshape';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';
import { Observable } from 'rxjs';
import { TrafficshapingService } from '../../services/trafficshaping.service';

@Component({
  selector: 'app-trafficshaping-switching-form',
  templateUrl: './trafficshaping-switching-form.component.html',
  styleUrls: ['./trafficshaping-switching-form.component.css'],
})
export class TrafficshapingSwitchingFormComponent extends AbstractEditableFormDirective<TrafficShapeSwitchModel> {
  /**
   * 3 Radio buttons: Day, Night, No Sync
   * Day: TrafficShapeSwitchValues.DAY
   * Night: TrafficShapeSwitchValues.NIGHT
   * No Sync: TrafficShapeSwitchValues.OFF
   */
  disableSpinnerUsage = true;
  trafficShapeSwitchValues = TrafficShapeSwitchValues;

  constructor(
    public helpStateService: HelpStateService,
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService,
    private trafficshapingService: TrafficshapingService
  ) {
    super(syncosyncModalService, processingSpinnerStateService);
  }

  protected getInitModel(): TrafficShapeSwitchModel {
    return new TrafficShapeSwitchModel();
  }

  protected modelRequest(): Observable<TrafficShapeSwitchModel> {
    return this.trafficshapingService.getTrafficshapeMode();
  }

  onSubmit(): void {
    const resultEmitter: EventEmitter<TrafficShapeSwitchModel> =
      new EventEmitter<TrafficShapeSwitchModel>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined && result !== null) {
        this.setModel(result);
        this.formFinished.emit(this.model);
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.UNKNOWN_ERROR
        );
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.PROCSESSING_TRAFFICSHAPE_MODE_SWITCHING_MESSAGE,
      this.trafficshapingService.setTrafficshapeMode(this.model),
      null,
      resultEmitter
    );
  }
}
