import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimezoneSetupFormComponent } from './timezone-setup-form.component';

describe('TimezoneSetupFormComponent', () => {
  let component: TimezoneSetupFormComponent;
  let fixture: ComponentFixture<TimezoneSetupFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TimezoneSetupFormComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimezoneSetupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
