import { Component, EventEmitter } from '@angular/core';
import {
  AbstractEditableFormDirective,
  InfoLevel,
  LocalizedMessage,
  HelpStateService,
  SyncosyncModalService,
} from '@syncosync_common';
import { Timezone } from '../../../common/model/timezone';
import { Observable } from 'rxjs';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';
import { TimezoneService } from '../../services/timezone.service';
import { AvailableTimezones } from '../../../common/model/availableTimezones';

@Component({
  selector: 'app-timezone-setup-form',
  templateUrl: './timezone-setup-form.component.html',
  styleUrls: ['./timezone-setup-form.component.css'],
})
export class TimezoneSetupFormComponent extends AbstractEditableFormDirective<Timezone> {
  public availableTimezones: AvailableTimezones = new AvailableTimezones();
  public currentTimezone: string;

  constructor(
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService,
    public helpStateService: HelpStateService,
    private timezoneService: TimezoneService
  ) {
    super(syncosyncModalService, processingSpinnerStateService);
  }

  onSubmit(): void {
    const resultEmitter: EventEmitter<Timezone> = new EventEmitter<Timezone>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined && result !== null) {
        this.setModel(result);
        this.formFinished.emit(this.model);
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.UNKNOWN_ERROR
        );
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.PROCSESSING_TIMEZONE_UPDATE_MESSAGE,
      this.timezoneService.setTimezone(this.model),
      null,
      resultEmitter
    );
  }

  public setModel(newModel: Timezone): void {
    super.setModel(newModel);
    this.currentTimezone = this.model.timezone;
  }

  protected initialize(): void {
    super.initialize();
    this.timezoneService.getAvailableTimezones().subscribe((res) => {
      if (
        this.availableTimezones === null ||
        this.availableTimezones === undefined
      ) {
        this.availableTimezones = res;
      } else {
        Object.assign(this.availableTimezones, res);
      }
    });
  }

  protected createFormGroup(): void {
    //nop
  }

  protected getInitModel(): Timezone {
    return new Timezone();
  }

  protected modelRequest(): Observable<Timezone> {
    return this.timezoneService.getTimeZone();
  }

  setTimezone(target: any): void {
    this.model.timezone = target.value;
  }
}
