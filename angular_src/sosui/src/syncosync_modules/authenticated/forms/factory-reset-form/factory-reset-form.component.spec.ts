import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FactoryResetFormComponent } from './factory-reset-form.component';

describe('FactoryResetFormComponent', () => {
  let component: FactoryResetFormComponent;
  let fixture: ComponentFixture<FactoryResetFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FactoryResetFormComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FactoryResetFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
