import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RebootFormComponent } from './reboot-form.component';

describe('RebootFormComponent', () => {
  let component: RebootFormComponent;
  let fixture: ComponentFixture<RebootFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RebootFormComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RebootFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
