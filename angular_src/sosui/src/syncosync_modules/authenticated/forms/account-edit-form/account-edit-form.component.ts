import { ChangeDetectorRef, Component } from '@angular/core';
import {
  AbstractEditableFormDirective,
  HelpStateService,
  MailInfoLevels,
  SshFingerprint,
  SyncosyncModalService,
} from '@syncosync_common';
import { Observable } from 'rxjs';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';
import { AccountApiService } from '../../services/account-api.service';
import { AccountEdit } from '../../../common/model/sosaccount';

@Component({
  selector: 'app-account-edit-form',
  templateUrl: './account-edit-form.component.html',
  styleUrls: ['./account-edit-form.component.css'],
})
export class AccountEditFormComponent extends AbstractEditableFormDirective<AccountEdit> {
  newFingerprint = '';
  sshKeyContent: string = null;
  mailInfoLevels = MailInfoLevels;

  constructor(
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService,
    public helpStateService: HelpStateService,
    private accountApiService: AccountApiService,
    private cd: ChangeDetectorRef
  ) {
    super(syncosyncModalService, processingSpinnerStateService);
  }

  protected createFormGroup(): void {
    // nop
  }

  protected getInitModel(): AccountEdit {
    throw new Error('Initial model not to be used');
  }

  protected initialize(): void {
    // suppress requestdata model overwriting model
  }

  onSubmit(): void {
    const toBeSent = new AccountEdit();
    Object.keys(this.model).forEach((key) => (toBeSent[key] = this.model[key]));

    if (this.sshKeyContent) {
      toBeSent.ssh_pub_key = this.sshKeyContent;
    }
    this.accountApiService.updateAccount(toBeSent).subscribe((result) => {
      if (result) {
        this.formFinished.emit(result);
      } else {
        this.formFinished.emit(null);
      }
    });
  }

  onSSHKeyChange($event): void {
    if ($event.target.files && $event.target.files.length) {
      const [file] = $event.target.files;
      console.log(file);
      // just checking if it is an pub key with file ending .pub or if it's more than 4 MB
      const fileEnding = file.name.substr(file.name.lastIndexOf('.') + 1);
      if (!fileEnding.startsWith('pub') || file.size > 2 ** 12) {
        this.cd.markForCheck();
      } else {
        file.text().then((content) => {
          this.newFingerprint = SshFingerprint.fingerprint(content);
          this.sshKeyContent = content.trim();
        });
        this.cd.markForCheck();
      }
    }
  }

  protected modelRequest(): Observable<AccountEdit> {
    return null;
  }

  public isInvalidConfiguration(): boolean {
    const newValidSshKey: boolean =
      this.sshKeyContent !== null && this.sshKeyContent.trim().length > 0;
    const newPassword: boolean =
      this.model.password !== null && this.model.password.trim().length > 0;
    const keyFingerprintPresent: boolean =
      this.model.key_fingerprint !== null &&
      this.model.key_fingerprint.trim().length > 0;
    const deleteSshPubKey: boolean =
      this.model.ssh_pub_key !== null &&
      this.model.ssh_pub_key.trim().length === 0 &&
      !newValidSshKey;
    // TODO: Was ist wenn ssh_pub_key null -> Wird nicht gesetzt

    const alwaysInvalid = this.model.delete_password && deleteSshPubKey;
    // In case no keys are set and are not to be set or deleted but the password is to be deleted. It does not matter whether one was set before or not as that configuration is invalid
    const deletePasswordInvalidCases =
      this.model.delete_password &&
      !deleteSshPubKey &&
      !keyFingerprintPresent &&
      !newValidSshKey;
    // In case the deletion of the SSH key is indicated and no password has been set before, an invalid configuration will be reached (it does not matter whether there as a key present beforehand or not)
    const deleteSshPubKeyInvalidCases =
      !this.model.delete_password &&
      deleteSshPubKey &&
      !this.model.is_password_set &&
      !newPassword &&
      !newValidSshKey;
    // basically starting from scratch
    const allFalse =
      !this.model.delete_password &&
      !keyFingerprintPresent &&
      !deleteSshPubKey &&
      !this.model.is_password_set &&
      !newPassword &&
      !newValidSshKey;
    return (
      alwaysInvalid ||
      deletePasswordInvalidCases ||
      deleteSshPubKeyInvalidCases ||
      allFalse
    );
  }
}
