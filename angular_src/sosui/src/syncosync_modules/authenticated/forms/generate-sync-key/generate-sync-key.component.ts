import { Component, EventEmitter } from '@angular/core';
import { SyncConfigurationService } from '../../services/sync-configuration.service';
import {
  AbstractEditableFormDirective,
  ConfirmationModalResult,
  HelpStateService,
  InfoLevel,
  LocalizedMessage,
  SosKey,
  SyncosyncModalService,
} from '@syncosync_common';
import { Observable } from 'rxjs';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';

@Component({
  selector: 'app-generate-sync-key',
  templateUrl: './generate-sync-key.component.html',
  styleUrls: ['./generate-sync-key.component.css'],
})
export class GenerateSyncKeyComponent extends AbstractEditableFormDirective<SosKey> {
  constructor(
    public syncConfigurationService: SyncConfigurationService,
    public helpStateService: HelpStateService,
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService
  ) {
    super(syncosyncModalService, processingSpinnerStateService);
  }

  onSubmit(): void {
    if (!this.model.fingerprint || this.model.fingerprint.trim().length == 0) {
      this.generateKey();
    } else {
      this.syncosyncModalService
        .showConfirmationModal(
          LocalizedMessage.GENERATE_NEW_KEY_CONFIRMATION_MODAL_TITLE,
          LocalizedMessage.GENERATE_NEW_KEY_CONFIRMATION_MODAL_CONTENT
        )
        .subscribe((userDecision) => {
          if (userDecision == ConfirmationModalResult.CONFIRM) {
            this.generateKey();
          }
        });
    }
  }

  protected createFormGroup(): void {
    //
  }

  protected getInitModel(): SosKey {
    return new SosKey();
  }

  protected modelRequest(): Observable<SosKey> {
    return this.syncConfigurationService.getCurrentLocalKey();
  }

  private generateKey() {
    const resultOfKeyGenerationEmitter: EventEmitter<SosKey> =
      new EventEmitter<SosKey>();
    resultOfKeyGenerationEmitter.subscribe((result) => {
      if (result !== undefined) {
        this.setModel(result);
        this.formFinished.emit(this.model);
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.GENERATE_NEW_KEY_FAILED
        );
      }
    });
    this.syncosyncModalService.showProcessingModal<SosKey>(
      LocalizedMessage.GENERATING_NEW_KEY_PROCESSING_MODAL_TITLE,
      LocalizedMessage.GENERATING_NEW_KEY_PROCESSING_MODAL_CONTENT,
      this.syncConfigurationService.generateLocalKey(),
      null,
      resultOfKeyGenerationEmitter
    );
  }
}
