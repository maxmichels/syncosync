import { Component } from '@angular/core';
import { NetworkConfigService } from '../../services/network-config.service';
import {
  AbstractEditableFormDirective,
  SshConfig,
  HelpStateService,
  SyncosyncModalService,
} from '@syncosync_common';
import { Observable } from 'rxjs';
import { ProcessingSpinnerStateService } from '../../../common/services/processing-spinner-state.service';

@Component({
  selector: 'app-ssh-port-form',
  templateUrl: './ssh-port-form.component.html',
  styleUrls: ['./ssh-port-form.component.css'],
})
export class SshPortFormComponent extends AbstractEditableFormDirective<SshConfig> {
  constructor(
    public networkConfigService: NetworkConfigService,
    public helpStateService: HelpStateService,
    protected syncosyncModalService: SyncosyncModalService,
    protected processingSpinnerStateService: ProcessingSpinnerStateService
  ) {
    super(syncosyncModalService, processingSpinnerStateService);
    this.model = new SshConfig();
  }

  public onSubmit(): void {
    // TODO: work with result...
    console.log('Sending: ', this.model);
    this.networkConfigService.setSshConfig(this.model).subscribe((config) => {
      console.log('ssh port received: ', config);
      if (config !== undefined) {
        this.setModel(config);
        this.formFinished.emit(this.model);
      } else {
        alert('Failed setting ssh port');
      }
    });
  }

  protected createFormGroup(): void {
    // nop
  }

  protected getInitModel(): SshConfig {
    return new SshConfig();
  }

  protected modelRequest(): Observable<SshConfig> {
    return this.networkConfigService.getSshConfig();
  }
}
