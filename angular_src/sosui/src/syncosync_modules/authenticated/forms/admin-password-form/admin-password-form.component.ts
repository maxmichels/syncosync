import { Component, EventEmitter } from '@angular/core';
import { AdminManagementService } from '../../services/admin-management.service';
import {
  AdminPassword,
  GenericUiResponse,
  HelpStateService,
  InfoLevel,
  LocalizedMessage,
  SyncosyncModalService,
} from '@syncosync_common';
import { AbstractEmptyFormDirective } from '../../../common/components/abstract-empty-form-directive/abstract-empty-form.directive';

@Component({
  selector: 'app-admin-password-form',
  templateUrl: './admin-password-form.component.html',
  styleUrls: ['./admin-password-form.component.css'],
})
export class AdminPasswordFormComponent extends AbstractEmptyFormDirective<AdminPassword> {
  passwordTextType = false;

  constructor(
    private adminManagementService: AdminManagementService,
    public helpStateService: HelpStateService,
    protected syncosyncModalService: SyncosyncModalService
  ) {
    super();
  }

  togglePasswordTextType(): void {
    this.passwordTextType = !this.passwordTextType;
  }

  onSubmit(): void {
    const resultEmitter: EventEmitter<GenericUiResponse> =
      new EventEmitter<GenericUiResponse>();
    resultEmitter.subscribe((result) => {
      if (result !== undefined && result !== null) {
        this.formFinished.emit(this.model);
      } else {
        this.syncosyncModalService.showInfoModal(
          InfoLevel.ERROR,
          LocalizedMessage.ADMIN_PASSWORD_SAVE_FAILED_MESSAGE
        );
      }
    });
    this.syncosyncModalService.showProcessingModal(
      LocalizedMessage.PROCESSING_MODAL_GENERIC_TITLE,
      LocalizedMessage.PROCESSING_ADMIN_PASSWORD_SAVE_MESSAGE,
      this.adminManagementService.setAdminPassword(this.model),
      null,
      resultEmitter
    );
  }

  protected getInitModel(): AdminPassword {
    return new AdminPassword();
  }
}
