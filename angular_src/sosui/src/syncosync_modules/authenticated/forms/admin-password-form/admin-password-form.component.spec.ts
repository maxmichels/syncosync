import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AdminPasswordFormComponent} from './admin-password-form.component';

describe('AdminPasswordFormComponent', () => {
    let component: AdminPasswordFormComponent;
    let fixture: ComponentFixture<AdminPasswordFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AdminPasswordFormComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AdminPasswordFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
