import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-configuration-traficshaping',
  templateUrl: './configuration-traficshaping.component.html',
  styleUrls: ['./configuration-traficshaping.component.css'],
})
export class ConfigurationTraficshapingComponent {
  constructor(public activeModal: NgbActiveModal) {}
}
