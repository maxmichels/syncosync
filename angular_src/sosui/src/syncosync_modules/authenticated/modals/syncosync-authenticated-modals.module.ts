import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SyncosyncCommonModule } from '../../common/syncosync-common.module';
import { AccountDeleteComponent } from '@syncosync_authenticated';
import { ConfigurationTraficshapingComponent } from './configuration-traficshaping/configuration-traficshaping.component';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { SyncosyncAuthenticatedFormsModule } from '../forms/syncosync-authenticated-forms.module';

@NgModule({
  declarations: [AccountDeleteComponent, ConfigurationTraficshapingComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    SyncosyncCommonModule,
    NgxSliderModule,
    SyncosyncAuthenticatedFormsModule,
  ],
  exports: [AccountDeleteComponent],
})
export class SyncosyncAuthenticatedModalsModule {}
