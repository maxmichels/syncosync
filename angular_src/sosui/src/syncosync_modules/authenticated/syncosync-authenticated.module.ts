import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SyncosyncAuthenticatedFormsModule } from './forms/syncosync-authenticated-forms.module';
import { SetupPartialsModule } from './setup-partials/setup-partials.module';
import { SyncosyncAuthenticatedModalsModule } from './modals/syncosync-authenticated-modals.module';

@NgModule({
  declarations: [],
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    NgbModule,
    SyncosyncAuthenticatedFormsModule,
    SetupPartialsModule,
  ],
  exports: [SyncosyncAuthenticatedModalsModule, SetupPartialsModule],
})
export class SyncosyncAuthenticatedModule {}
