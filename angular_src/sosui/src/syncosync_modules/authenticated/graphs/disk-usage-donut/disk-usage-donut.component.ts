import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { AccountApiService } from '../../services/account-api.service';
import {
  AbstractSubscriberComponent,
  AccountList,
  Calculators,
} from '@syncosync_common';
import { StatusService } from '../../../public';

import {
  ApexAxisChartSeries,
  ApexTitleSubtitle,
  ApexDataLabels,
  ApexChart,
  ApexPlotOptions,
  ApexLegend,
  ChartComponent,
  ApexTooltip,
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  title: ApexTitleSubtitle;
  plotOptions: ApexPlotOptions;
  legend: ApexLegend;
  colors: string[];
  tooltip: ApexTooltip;
};

@Component({
  selector: 'app-disk-usage-donut',
  templateUrl: './disk-usage-donut.component.html',
  styleUrls: ['./disk-usage-donut.component.css'],
})
export class DiskUsageDonutComponent
  extends AbstractSubscriberComponent
  implements AfterViewInit
{
  @ViewChild(ChartComponent) chart: ChartComponent;
  public chartOptions: Partial<ChartOptions> = {
    series: [
      {
        data: [
          {
            x: 'Free',
            y: 1,
          },
        ],
      },
    ],
    legend: {
      show: false,
    },
    chart: {
      height: 120,
      type: 'treemap',
      animations: {
        enabled: false,
      },
      toolbar: {
        tools: {
          download: false,
        },
      },
    },
    colors: [
      '#44CC44',
      '#3B93A5',
      '#ADD8C7',
      '#EC3C65',
      '#CDD7B6',
      '#D43F97',
      '#1E5D8C',
      '#421243',
      '#7F94B0',
      '#EF6537',
      '#C0ADDB',
      '#F7B844',
    ],
    plotOptions: {
      treemap: {
        distributed: true,
        enableShades: false,
      },
    },
    tooltip: {
      enabled: true,
      x: {
        show: false,
      },
      y: {
        formatter: function (value) {
          return Calculators.bytesToHumanReadableSize(value);
        },
      },
    },
  };

  constructor(
    private statusService: StatusService,
    private accountApiService: AccountApiService
  ) {
    super();
  }

  ngAfterViewInit(): void {
    this.accountApiService.latestAccountlistObservable$
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((accounts) => {
        this.handleAccounts(accounts);
      });
  }

  private handleAccounts(accountList: AccountList) {
    if (
      accountList === undefined ||
      this.statusService.getStatus() === undefined
    ) {
      return;
    }
    //this.barChartData.length = 1;
    let freeSpace = this.statusService.getStatus().total_size;
    const data = [];
    data.push({
      x: 'Free',
      y: 100,
    });
    for (const account of accountList.getAccounts()) {
      const spaceUsed = account.space_used;
      freeSpace -= spaceUsed;
      data.push({
        x: account.name,
        y: spaceUsed,
      });
    }
    data[0].y = freeSpace;
    const series = [
      {
        data: data,
      },
    ];
    this.chartOptions.series = series;
  }
}
