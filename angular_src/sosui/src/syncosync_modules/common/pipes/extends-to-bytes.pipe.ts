import {Pipe, PipeTransform} from '@angular/core';
import {Calculators} from '../utils/calculators';

@Pipe({
    name: 'extendsToBytes'
})
export class ExtendsToBytesPipe implements PipeTransform {
    transform(bytes: number): number {
        return bytes === undefined ? 0 : Calculators.extendsToBytes(bytes);
    }
}
