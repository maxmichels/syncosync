import {Pipe, PipeTransform} from '@angular/core';
import {Calculators} from '../utils/calculators';

@Pipe({
    name: 'bytesToHumanReadableSize'
})

export class BytesToHumanReadableSizePipe implements PipeTransform {
    transform(bytes: number): string {
        return bytes === undefined ? "" : Calculators.bytesToHumanReadableSize(bytes);
    }
}
