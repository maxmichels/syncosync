import { Directive } from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import { validateIPv4 } from '@syncosync_common';

@Directive({
  selector: '[validateIPv4]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ValidateIPv4Directive,
      multi: true,
    },
  ],
})
export class ValidateIPv4Directive implements Validator {
  constructor() {
    // nop
  }

  validate(formControl: AbstractControl): ValidationErrors | null {
    return validateIPv4(formControl);
  }
}
