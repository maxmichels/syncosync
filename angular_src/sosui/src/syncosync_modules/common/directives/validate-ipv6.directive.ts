import { Directive } from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import { validateIPv6 } from '@syncosync_common';

@Directive({
  selector: '[validateIPv6]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ValidateIPv6Directive,
      multi: true,
    },
  ],
})
export class ValidateIPv6Directive implements Validator {
  constructor() {
    // nop
  }

  validate(formControl: AbstractControl): ValidationErrors | null {
    return validateIPv6(formControl);
  }
}
