import {Directive, OnInit} from '@angular/core';
import {FormControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";
import {validateAdminPassword} from "../validators/FormValidators";

@Directive({
    selector: '[validateSyncosyncPassword]',
    providers: [
        {provide: NG_VALIDATORS, useExisting: ValidateSyncosyncPasswordDirective, multi: true}
    ]
})
export class ValidateSyncosyncPasswordDirective implements Validator, OnInit {

    ngOnInit() {
    }

    validate(formControl: FormControl): ValidationErrors | null {
        return validateAdminPassword(formControl);
    }
}
