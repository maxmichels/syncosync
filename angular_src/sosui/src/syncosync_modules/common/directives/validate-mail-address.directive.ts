import {Directive} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";
import {validateMailAddress} from "../validators/FormValidators";

@Directive({
    selector: '[validateMailAddress]',
    providers: [
        {provide: NG_VALIDATORS, useExisting: ValidateMailAddressDirective, multi: true}
    ]
})
export class ValidateMailAddressDirective implements Validator {

    constructor() {
    }

    validate(formControl: AbstractControl): ValidationErrors | null {
        return validateMailAddress(formControl);
    }

}
