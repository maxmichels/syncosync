import { Directive } from '@angular/core';
import {
  FormControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import { validateAdminPassword } from '@syncosync_common';

@Directive({
  selector: '[validateAdminPassword]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ValidateAdminPasswordDirective,
      multi: true,
    },
  ],
})
export class ValidateAdminPasswordDirective implements Validator {
  validate(formControl: FormControl): ValidationErrors | null {
    return validateAdminPassword(formControl);
  }
}
