import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BootstrapIconsModule } from 'ng-bootstrap-icons';
import { allIcons } from 'ng-bootstrap-icons/icons';
import { FormsModule } from '@angular/forms';
import {
  BytesToGbPipe,
  BytesToHumanReadableSizePipe,
  ConfirmationModalComponent,
  ExtendsToBytesPipe,
  InfoModalComponent,
  LocalizationService,
  LocalizeMessagePipe,
  ProcessingModalComponent,
  QuotaUsedPercentagePipe,
  SetupPartialDirective,
  ToggleHelpButtonComponent,
  ValidateAccountNameDirective,
  ValidateAccountPasswordDirective,
  ValidateHostnameDirective,
  ValidateInfoperiodDirective,
  ValidateLongNameDirective,
  ValidateMailAddressDirective,
  ValidatePortDirective,
  ValidateSyncosyncPasswordDirective,
} from '@syncosync_common';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { ProcessingSpinnerComponent } from './components/processing-spinner/processing-spinner.component';
import { PartitioningDataDisplayComponent } from './components/partitioning-data-display/partitioning-data-display.component';
import { ToggleShapingButtonComponent } from './components/toggle-shaping-button/toggle-shaping-button.component';
import { PasswordCheckComponent } from './forms/password-check/password-check.component';
import { RequestDataModalComponent } from './modals/request-data-modal/request-data-modal.component';
import { ValidateAdminPasswordDirective } from './directives/validate-admin-password.directive';
import { FileSelectorComponent } from './components/file-upload/file-selector.component';
import { ValidateIpAddressDirective } from './directives/validate-ip-address.directive';
import { ValidateIPv4Directive } from './directives/validate-ipv4.directive';
import { ValidateIPv6Directive } from './directives/validate-ipv6.directive';

/** Upon loading the module, we want to request the localized messages in order to have them at hand
 * whenever we need it **/
export function requestTranslations(localizationService: LocalizationService) {
  return () => localizationService.initTranslations();
}

@NgModule({
  exports: [
    BootstrapIconsModule,
    BytesToGbPipe,
    BytesToHumanReadableSizePipe,
    QuotaUsedPercentagePipe,
    ValidateLongNameDirective,
    ValidateAccountNameDirective,
    ValidateAccountPasswordDirective,
    ValidateSyncosyncPasswordDirective,
    SetupPartialDirective,
    ValidateSyncosyncPasswordDirective,
    ValidateMailAddressDirective,
    ValidatePortDirective,
    ValidateHostnameDirective,
    ValidateInfoperiodDirective,
    ProcessingSpinnerComponent,
    ExtendsToBytesPipe,
    PartitioningDataDisplayComponent,
    ToggleHelpButtonComponent,
    ToggleShapingButtonComponent,
    ValidateAdminPasswordDirective,
    FileSelectorComponent,
    ValidateIpAddressDirective,
    ValidateIPv4Directive,
    ValidateIPv6Directive,
  ],
  declarations: [
    BytesToGbPipe,
    BytesToHumanReadableSizePipe,
    QuotaUsedPercentagePipe,
    ValidateLongNameDirective,
    ValidateAccountNameDirective,
    ValidateAccountPasswordDirective,
    ConfirmationModalComponent,
    ProcessingModalComponent,
    InfoModalComponent,
    LocalizeMessagePipe,
    ValidateSyncosyncPasswordDirective,
    SetupPartialDirective,
    ValidateMailAddressDirective,
    ValidatePortDirective,
    ValidateHostnameDirective,
    ValidateInfoperiodDirective,
    ToggleHelpButtonComponent,
    ProcessingSpinnerComponent,
    ExtendsToBytesPipe,
    PartitioningDataDisplayComponent,
    ToggleShapingButtonComponent,
    PasswordCheckComponent,
    RequestDataModalComponent,
    ValidateAdminPasswordDirective,
    FileSelectorComponent,
    ValidateIpAddressDirective,
    ValidateIpAddressDirective,
    ValidateIPv4Directive,
    ValidateIPv6Directive,
  ],
  imports: [
    BootstrapIconsModule.pick(allIcons),
    FormsModule,
    CommonModule,
    NgbTooltipModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: requestTranslations,
      multi: true,
      deps: [LocalizationService],
    },
  ],
})
export class SyncosyncCommonModule {}
