import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { SidebarVisibilityService } from '@syncosync_common';

@Injectable({
  providedIn: 'root',
})
export class SetupUiSwitchingService {
  /**
   * TODO: Add more complex checks as to which components are to be shown depending on mode of system
   */
  private showSetupComponents = false;

  constructor(
    public sidebarVisibilityService: SidebarVisibilityService,
    private router: Router
  ) {}

  public isShowSetupComponents(): boolean {
    return this.showSetupComponents;
  }

  public toggleShowSetupComponents(): void {
    this.showSetupComponents = !this.showSetupComponents;
    if (this.showSetupComponents) {
      this.sidebarVisibilityService.setShowSidebar(true);
    } else {
      this.router.navigate(['/syncosync/status']);
    }
  }
}
