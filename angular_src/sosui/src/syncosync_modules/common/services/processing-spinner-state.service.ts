import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ProcessingSpinnerStateService {
  /**
   * Keeping a count to track ongoing processes such that one process does not override a simple boolean
   * allowing an action to be taken
   * @private
   */
  private spinnerCount = 0;

  constructor() {
    //
  }

  public isShowSpinner(): boolean {
    return this.spinnerCount != 0;
  }

  public addSpinner(): void {
    this.spinnerCount++;
  }

  public removeSpinner(): void {
    this.spinnerCount--;
  }
}
