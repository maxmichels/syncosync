import { TestBed } from '@angular/core/testing';

import { SetupUiSwitchingService } from './setup-ui-switching.service';

describe('SetupUiSwitchingService', () => {
  let service: SetupUiSwitchingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SetupUiSwitchingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
