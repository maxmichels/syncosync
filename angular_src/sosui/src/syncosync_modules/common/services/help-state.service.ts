import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class HelpStateService {
  private showHelp = false;

  public isShowHelp(): boolean {
    return this.showHelp;
  }

  public toggleShowHelp(): void {
    this.showHelp = !this.showHelp;
  }
}
