import {TestBed} from '@angular/core/testing';

import {ProcessingSpinnerStateService} from './processing-spinner-state.service';

describe('ProcessingSpinnerStateService', () => {
    let service: ProcessingSpinnerStateService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(ProcessingSpinnerStateService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
