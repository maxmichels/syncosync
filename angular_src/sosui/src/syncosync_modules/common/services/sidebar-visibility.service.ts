import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SidebarVisibilityService {
  /**
   * Service providing a simple boolean flag indicating whether or not a sidebar - if applicable - is to be shown
   */
  private showSidebar = false;

  constructor() {
    // nop
  }

  public isShowSidebar(): boolean {
    return this.showSidebar;
  }

  public toggleShowSidebar(): void {
    this.showSidebar = !this.showSidebar;
  }

  public setShowSidebar(value: boolean): void {
    this.showSidebar = value;
  }
}
