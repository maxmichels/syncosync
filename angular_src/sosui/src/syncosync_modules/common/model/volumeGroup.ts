export class VolumeGroup {
    vg_uuid: string = null;
    vg_extent_size: string = null;
    vg_extent_count: string = null;
    vg_free: string = null;
    vg_status: string = null;
    vg_name: string = null;
}