export class PartitionInfo {
  number: number;
  name: string;
  length: number;
  fstype: string = null;
}
