export enum InfoLevel {
  INFO,
  WARNING,
  ERROR,
  FATAL,
}
