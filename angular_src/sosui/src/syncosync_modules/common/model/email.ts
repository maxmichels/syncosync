import {MailAuthenticationType, MailInfoLevels, MailSslType} from "./sosEnums";

export class MailConfig {
    sender_name: string;
    smtp_user: string;
    smtp_passwd: string;
    mail_from: string;
    smtp_port: number;
    smtp_host: string;
    ssl_type: MailSslType;
    authentication_type: MailAuthenticationType;
    admin_mail_address: string;
    admin_info_period: number = 7;
    mail_info_level: MailInfoLevels;
}