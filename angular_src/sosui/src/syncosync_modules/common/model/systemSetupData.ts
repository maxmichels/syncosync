import { Type } from 'class-transformer';

export class SystemSetupData {
  configname = 'systemsetupdata';
  // tslint:disable-next-line:variable-name
  pub_key = '';
  hostname = 'syncosync';
  action: SsdAction = SsdAction.INTERACTIVE_LEADER;
  @Type(() => PartitioningData)
  // tslint:disable-next-line:variable-name
  partition_data: PartitioningData = new PartitioningData();
  // tslint:disable-next-line:variable-name
  vg_sos_uuid = '';
}

export class PartitioningData {
  free = 0;
  local = 0;
  remote = 0;
  // tslint:disable-next-line:variable-name
  local_min = 0;
  // tslint:disable-next-line:variable-name
  remote_min = 0;
  // Slider: remote_min | remote + Free + local | local_min

  public flipLocalRemote(): void {
    /**
     * Flips the remote, local and remote_min, local_min values as the local
     * box receives the SSD/PartitioningData of the opposing box.
     * Thus local and remote need to be flipped for a valid representation
     */
    const localMin = this.remote_min;
    const local = this.remote;
    const remote = this.local;
    const remoteMin = this.local_min;
    this.local_min = localMin;
    this.local = local;
    this.remote = remote;
    this.remote_min = remoteMin;
  }

  public isAssumedToBeResized(): boolean {
    /**
     * This method essentially returns whether the model is assumed to
     * represent an already allocated partitioning. If true, the partitions
     * have data allocated locally or remotely and thus should not be (aka
     * factory state)
     */
    return this.local_min > 4 || this.remote_min > 4;
  }
}

export enum SsdAction {
  INTERACTIVE_LEADER,
  INTERACTIVE_FOLLOWER,
}
