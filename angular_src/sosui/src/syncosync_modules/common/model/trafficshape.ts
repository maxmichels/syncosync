export class Trafficshape {
  out_day = 10000;
  in_day = 10000;
  out_night = 10000;
  in_night = 10000;
  day = 360;
  night = 0;
  maxout = 10000;
  maxin = 10000;
}

export class TrafficShapeSwitchModel {
  mode = TrafficShapeSwitchValues.OFF;
}

export enum TrafficShapeSwitchValues {
  OFF = 0,
  DAY = 1,
  NIGHT = 2,
}
