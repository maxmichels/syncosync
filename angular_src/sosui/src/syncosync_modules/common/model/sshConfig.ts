import {SshdAccess} from "./sosEnums";

export class SshConfig {
    // tslint:disable-next-line:variable-name
    public port: number;
    public access: SshdAccess = SshdAccess.OFF;
    public pid: number = -1;

}
