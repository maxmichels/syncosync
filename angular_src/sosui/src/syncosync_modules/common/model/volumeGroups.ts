import {VolumeGroup} from "./volumeGroup";

export class VolumeGroups {
    vg: Map<string, VolumeGroup> = new Map<string, VolumeGroup>();
    bad: boolean = false;
    multiple_sos: boolean = false;
    sos_found: boolean = false;
    cfgmd5sum: string = null;
    cfgdate: number = 0;
    cfghostname: string = null;
}