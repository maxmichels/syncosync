import { UiLanguage } from './uiLanguage';
import { SysType } from './sosEnums';

export class SosConfig {
  configname = '';
  systype: SysType = SysType.GENERIC;
  upnp = false;
  ui_language: UiLanguage = new UiLanguage();
  local: number = null;
  remote: number = null;
  vg_sos_uuid = '';
}
