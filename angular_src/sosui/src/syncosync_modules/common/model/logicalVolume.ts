export class LogicalVolume {
    lv_name: string = null;
    lv_uuid: string = null;
    lv_full_name: string = null;
    lv_path: string = null;
    lv_active: string = null;
    lv_size: number = 0;
    vg_uuid: string = null;
    vg_name: string = null;
    mountpoint: string = null;
}