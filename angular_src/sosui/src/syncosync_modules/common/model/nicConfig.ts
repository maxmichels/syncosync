/* tslint:disable:variable-name */
import { NicMode } from './sosEnums';

export class NicConfig {
  public interface: string;
  // v4 vs v6
  public ip4_mode: NicMode;
  public ip6_mode: NicMode;

  public ip4_address: string;
  public ip4_netmask: string;
  public ip4_gateway: string;

  public ip6_address: string;
  public ip6_netmask: string;
  public ip6_gateway: string;

  // show the potential speed of the connection (100M; 1G; 10G etc...)
  public linkspeed: string;

  // TODO: validation-method?
}
