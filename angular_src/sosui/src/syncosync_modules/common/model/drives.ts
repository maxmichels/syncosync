import { Drive } from './drive';
import { PhysicalVolumeGroups } from './physicalVolumeGroups';
import { VolumeGroups } from './volumeGroups';
import { LogicalVolumes } from './logicalVolumes';
import { VolMount } from './sosEnums';

export class Drives {
  non_sos_drive = false;
  drives: Drive[] = [];
  pvs: PhysicalVolumeGroups = null;
  vgs: VolumeGroups = null;
  lvs: LogicalVolumes = null;
  sos_vol_mount: VolMount = VolMount.NONE;
}
