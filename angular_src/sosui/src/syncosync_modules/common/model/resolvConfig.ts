export class ResolvConfig {
    // as per `man resolv.conf`:
    // "Up to  MAXNS  (currently  3,  see  <resolv.h>)  name servers may be listed, one per keyword."
    // tslint:disable-next-line:variable-name
    public first_nameserver = '';
    // tslint:disable-next-line:variable-name
    public second_nameserver = '';
    // tslint:disable-next-line:variable-name
    public third_nameserver = '';
}
