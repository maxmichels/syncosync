import {PartitionInfo} from "./partitionInfo";

export class Drive {
    id: string = null;
    product: string = null;
    revision: string = null;
    device: string = null;
    sata_size: number = 0;
    vg_uuid: string = null;
    vg_name: string = null;
    vg_state: string = null;
    partitions: PartitionInfo[] = [];
}