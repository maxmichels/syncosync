export class Calculators {
  public static blocksToGb(blocks: number): number {
    if (blocks === undefined) {
      return 0;
    } else {
      return Math.round(blocks / 1048576);
    }
  }

  public static bytesToGb(bytes: number): number {
    if (bytes === undefined) {
      return 0;
    } else {
      return Math.round(bytes / 1e9);
    }
  }

  public static extendsToBytes(amountOfExtends: number): number {
    // one extend is 4MB (4*10^6 bytes)
    return amountOfExtends * 4 * 1e6;
  }

  public static bytesToMb(bytes: number): number {
    if (bytes === undefined) {
      return 0;
    } else {
      return Math.round(bytes / 1e6);
    }
  }

  public static bytesToHumanReadableSize(bytes: number): string {
    /***
     * https://stackoverflow.com/questions/10420352/converting-file-size-in-bytes-to-human-readable-string/10420404
     */
    const si = false;
    const dp = 1;
    const thresh = si ? 1000 : 1024;

    if (Math.abs(bytes) < thresh) {
      return bytes + ' B';
    }

    const units = si
      ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
      : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    let u = -1;
    const r = 10 ** dp;

    do {
      bytes /= thresh;
      ++u;
    } while (
      Math.round(Math.abs(bytes) * r) / r >= thresh &&
      u < units.length - 1
    );

    return bytes.toFixed(dp) + ' ' + units[u];
  }

  public static quotaUsed(
    bytesUsed: number,
    totalSize: number,
    percentageAllowed: number
  ): number {
    /***
     * param percentageAllowed is 0-100 formatted percentage
     */
    // Typescript is weird. Omitting /100 * 100 makes it wrong by 1e5
    return (bytesUsed / (totalSize * (percentageAllowed / 100))) * 100;
  }

  public static secToDays(sec: number): number {
    return Math.floor(sec / (3600 * 24));
  }
}
