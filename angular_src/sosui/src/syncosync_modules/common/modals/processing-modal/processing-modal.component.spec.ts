import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProcessingModalComponent} from '@syncosync_common';

describe('ProcessingModalComponent', () => {
    let component: ProcessingModalComponent<any>;
    let fixture: ComponentFixture<ProcessingModalComponent<any>>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ProcessingModalComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ProcessingModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
