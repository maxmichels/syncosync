import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestDataModalComponent } from './request-data-modal.component';

describe('RequestDataModalComponent', () => {
  let component: RequestDataModalComponent;
  let fixture: ComponentFixture<RequestDataModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RequestDataModalComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestDataModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
