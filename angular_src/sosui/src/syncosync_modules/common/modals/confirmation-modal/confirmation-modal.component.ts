import { Component, EventEmitter, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SyncosyncModalParent } from '../syncosync-modal-parent/syncosync-modal-parent.injectable';
import { LocalizedMessage } from '../../model/localizedMessage';
import { ConfirmationModalResult } from '../../model/confirmationModalResult';

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.css'],
})
export class ConfirmationModalComponent extends SyncosyncModalParent<
  LocalizedMessage,
  LocalizedMessage,
  LocalizedMessage
> {
  @Output() onResult: EventEmitter<ConfirmationModalResult> =
    new EventEmitter();

  constructor(public activeModal: NgbActiveModal) {
    super(activeModal);
  }

  confirmModal(): void {
    this.onResult.emit(ConfirmationModalResult.CONFIRM);
    this.closeModal();
  }

  cancelModal(): void {
    this.onResult.emit(ConfirmationModalResult.CANCEL);
    this.closeModal();
  }
}
