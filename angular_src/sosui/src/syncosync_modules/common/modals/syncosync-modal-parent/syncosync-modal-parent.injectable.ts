import { Injectable, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalizedMessage } from '../../model/localizedMessage';

@Injectable()
export class SyncosyncModalParent<
  T,
  S,
  W extends LocalizedMessage | string | string[]
> {
  @Input() title: S;
  @Input() content: T;
  @Input() additionalContent: W = null;

  protected constructor(public activeModal: NgbActiveModal) {}

  closeModal(): void {
    this.activeModal.close('Closed');
  }

  isAdditionalContentArray(): boolean {
    if (
      this.additionalContent === null ||
      !Array.isArray(this.additionalContent)
    ) {
      return false;
    }
    let somethingIsNotString = false;
    this.additionalContent.forEach(function (item) {
      if (typeof item !== 'string') {
        somethingIsNotString = true;
      }
    });
    return !somethingIsNotString && this.additionalContent.length > 0;
  }

  isAdditionalContentString(): boolean {
    if (
      this.additionalContent === null ||
      Array.isArray(this.additionalContent)
    ) {
      return false;
    }
    return typeof this.additionalContent === 'string';
  }

  isAdditionalContentLocalizedMessage(): boolean {
    if (
      this.additionalContent === null ||
      Array.isArray(this.additionalContent) ||
      typeof this.additionalContent === 'string'
    ) {
      return false;
    }
    return this.additionalContent in LocalizedMessage;
  }
}
