import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleShapingButtonComponent } from './toggle-shaping-button.component';

describe('ToggleShapingButtonComponent', () => {
  let component: ToggleShapingButtonComponent;
  let fixture: ComponentFixture<ToggleShapingButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ToggleShapingButtonComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleShapingButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
