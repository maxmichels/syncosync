import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-selector.component.html',
  styleUrls: ['./file-selector.component.css'],
})
export class FileSelectorComponent {
  @Input()
  requiredFileType: string;

  @Output() newFile = new EventEmitter<File>();

  fileName = '';

  constructor() {
    //
  }

  onFileSelected(event): void {
    const file: File = event.target.files[0];
    if (file === null || file === undefined) {
      return;
    }
    this.fileName = file.name;
    this.newFile.emit(file);
  }
}
