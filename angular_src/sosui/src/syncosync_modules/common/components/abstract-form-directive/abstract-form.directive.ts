import { Directive, EventEmitter, OnInit, Output } from '@angular/core';
import { AbstractSubscriberComponent } from '../abstract-subscriber-component/abstract-subscriber.component';

@Directive()
export abstract class AbstractFormDirective<T = any>
  extends AbstractSubscriberComponent
  implements OnInit
{
  @Output() modelChanged: EventEmitter<T> = new EventEmitter();
  /**
   * Indicates whether the form was submitted successfully (value != null) or not (== null)
   */
  @Output() formFinished: EventEmitter<T> = new EventEmitter();
  @Output() model: T = null;

  protected constructor() {
    super();
  }

  public abstract onSubmit(): void;

  ngOnInit(): void {
    this.initialize();
  }

  protected abstract initialize(): void;

  protected abstract getInitModel(): T;

  public setModel(newModel: T): void {
    if (this.model === null || this.model === undefined) {
      this.model = Object.assign(this.getInitModel(), newModel);
    } else {
      Object.assign(this.model, newModel);
    }

    this.modelChanged.emit(this.model);
  }

  public setModelDirect(newModel: T): void {
    /**
     * Replaces the model as is expecting a properly instantiated object which
     * may not be the case if returned values are just passed through
     * apparently.
     */
    this.model = newModel;
    this.modelChanged.emit(this.model);
  }
}
