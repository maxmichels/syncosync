import { Component, Input } from '@angular/core';
import { PartitioningData } from '@syncosync_common';

@Component({
  selector: 'app-partitioning-data-display',
  templateUrl: './partitioning-data-display.component.html',
  styleUrls: ['./partitioning-data-display.component.css'],
})
export class PartitioningDataDisplayComponent {
  @Input() model: PartitioningData = null;

  constructor() {
    // nop
  }
}
