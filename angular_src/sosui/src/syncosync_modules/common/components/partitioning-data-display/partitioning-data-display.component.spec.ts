import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartitioningDataDisplayComponent } from './partitioning-data-display.component';

describe('PartitioningDataDisplayComponent', () => {
  let component: PartitioningDataDisplayComponent;
  let fixture: ComponentFixture<PartitioningDataDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PartitioningDataDisplayComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartitioningDataDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
