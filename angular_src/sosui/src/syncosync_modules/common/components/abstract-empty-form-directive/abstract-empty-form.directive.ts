import { Directive } from '@angular/core';
import { AbstractFormDirective } from '../abstract-form-directive/abstract-form.directive';

@Directive()
export abstract class AbstractEmptyFormDirective<
  T = any
> extends AbstractFormDirective<T> {
  protected initialize(): void {
    if (this.model === undefined || this.model === null) {
      this.model = this.getInitModel();
    }
  }
}
