import { Component } from '@angular/core';
import { ProcessingSpinnerStateService } from '../../services/processing-spinner-state.service';

@Component({
  selector: 'app-processing-spinner',
  templateUrl: './processing-spinner.component.html',
  styleUrls: ['./processing-spinner.component.css'],
})
export class ProcessingSpinnerComponent {
  constructor(
    public processingSpinnerStateService: ProcessingSpinnerStateService
  ) {}
}
