import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleHelpButtonComponent } from './toggle-help-button.component';

describe('ToggleHelpButtonComponent', () => {
  let component: ToggleHelpButtonComponent;
  let fixture: ComponentFixture<ToggleHelpButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToggleHelpButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleHelpButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
