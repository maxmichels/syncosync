import { Component } from '@angular/core';
import { HelpStateService } from '../../services/help-state.service';

@Component({
  selector: 'app-toggle-help-button',
  templateUrl: './toggle-help-button.component.html',
  styleUrls: ['./toggle-help-button.component.css'],
})
export class ToggleHelpButtonComponent {
  constructor(public helpStateService: HelpStateService) {}
}
