import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbstractEditableFormDirective } from './abstract-editable-form.directive';

describe('AbstractEditableFormComponentComponent', () => {
  let component: AbstractEditableFormDirective;
  let fixture: ComponentFixture<AbstractEditableFormDirective>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AbstractEditableFormDirective],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbstractEditableFormDirective);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
