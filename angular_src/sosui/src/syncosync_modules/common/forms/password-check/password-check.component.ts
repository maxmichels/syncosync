import { Component } from '@angular/core';
import { AbstractFormDirective, AdminPassword } from '@syncosync_common';
import { AbstractEmptyFormDirective } from '../../components/abstract-empty-form-directive/abstract-empty-form.directive';

@Component({
  selector: 'app-password-check',
  templateUrl: './password-check.component.html',
  styleUrls: ['./password-check.component.css'],
})
export class PasswordCheckComponent extends AbstractEmptyFormDirective<AdminPassword> {
  constructor() {
    super();
  }

  protected createFormGroup(): void {
    // nop
  }

  onSubmit(): void {
    this.formFinished.emit(this.model);
  }

  protected getInitModel(): AdminPassword {
    return new AdminPassword();
  }
}
