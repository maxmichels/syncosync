"""
Managing remote hosts
"""
import glob

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import os
import re
import shutil
import socket
import time
from typing import Optional, Tuple

import paramiko
from soscore import configbase, soshelpers_file, soshelpers_shape
from sosmodel import sosconstants
from sosmodel.remotehost import RemoteHostModel
from sosmodel.sos_enums import Partition, RemoteUpStatus
from sosmodel.sosconstants import MOUNT, MOUNTDEV, SOS_CONF_REMOTEHOST
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


class IgnorePolicy(paramiko.MissingHostKeyPolicy):
    """
    Policy for accepting any host key.
    TODO: is this secure enough?
    """

    def missing_host_key(self, client, hostname, key):
        return


def check_host(hostname, port, keyfile) -> Tuple[RemoteUpStatus, str]:
    """
    this checks any other host if it is reachable via ssh, could be used for evaluation
    :param keyfile: the path to a keyfile, this is not the key itself!
    :param hostname: hostname of remote host
    :param port: port
    :return: RemoteUpStatus...
            Attention: if BAD_KEY is issued, this means, that the host is reachable and there is an issue
            with the keyfile - either it is wrong or even not there
    """
    uuid = ""
    if hostname is None or hostname == "":
        return (RemoteUpStatus.NOT_AVAILABLE, uuid)
    if (keyfile is not None) and (not os.path.isfile(keyfile)):
        logger.debug(f"Checking host {hostname} port {str(port)} with no keyfile")
        keyfile = None
    else:
        logger.debug(f"keyfile: {keyfile}")
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.WarningPolicy())
    try:
        client.connect(
            socket.gethostbyname(hostname),
            int(port),
            username="syncosync",
            timeout=30,
            allow_agent=False,
            key_filename=keyfile,
        )
    except paramiko.SSHException as e:
        if (
            str(e) == "No existing session"
            or str(e)
            == "Error reading SSH protocol banner[Errno 104] Connection reset by peer"
        ):
            client.close()
            return (RemoteUpStatus.NO_SSH, uuid)
        if str(e) == "Private key file is encrypted":
            client.close()
            return (RemoteUpStatus.BAD_KEY, uuid)
        else:
            logger.error(f"ssh exception: {str(e)}")
            client.close()
            return (RemoteUpStatus.FAULTY, uuid)
    except socket.error as e:
        logger.error(f"socket error: {str(e)}")
        client.close()
        return (RemoteUpStatus.NO_REPLY, uuid)
    except Exception as e:
        logger.warning(f"socket error: {str(e)}")
        client.close()
        return (RemoteUpStatus.HOST_CLOSED, uuid)
    try:
        (stdin, stdout, stderr) = client.exec_command("source ./uuid")
        uuid = stdout.readlines()[0]
    except Exception as e:
        logger.error(f"No uuid readable on remote host:{str(e)}")
    logger.debug(f"Got uuid from remotehost: {uuid}")
    client.close()
    return (RemoteUpStatus.UP, uuid)


def read_from_file(filepath=sosconstants.LSYNCD_CONF_FILE) -> Optional[RemoteHostModel]:
    if not os.path.isfile(filepath):
        return None
    my_remotehost = RemoteHostModel()
    input_file = open(filepath, "r")
    file_content = input_file.read()
    m = re.search(r".*target.*=.*\"(.*):.*", file_content)
    if m:
        my_remotehost.hostname = m.group(1)
    else:
        my_remotehost.hostname = ""

    m = re.search(r".*_extra.*-p ([0-9]*).*", file_content)
    if m:
        my_remotehost.port = m.group(1)
    else:
        my_remotehost.port = ""
    input_file.close()
    return my_remotehost


def set_in_file(filepath, config: RemoteHostModel):
    input_file = open(sosconstants.STATIC_LSYNCD_CONF, "r")
    file_content = input_file.read()
    input_file.close()
    m = re.compile('(.*=.*").*(:remote/syncosync")')
    res = m.sub(r"\g<1>" + str(config.hostname) + r"\g<2>", file_content)
    file_content = res
    m = re.compile(r"(.*_extra.*-p )[0-9]*(.*)")
    res = m.sub(r"\g<1>" + str(config.port) + r"\g<2>", file_content)
    file_content = res
    soshelpers_file.write_to_file(filepath, file_content)


def check_and_set_config() -> bool:
    """
    checks, if the lsyncd.conf.lua exists
    """
    if not os.path.isfile(sosconstants.LSYNCD_CONF_FILE):
        return False
    else:
        return True


def check_and_start_lsyncd(restart=False):
    """
    starts or stops lsyncd depending on mode
    :param restart: if True, lsyncd will also be restarted, independent if it is already in this mode
    :return: True if started
    """
    if not check_and_set_config() or restart:
        stop_lsyncd()
    if os.system("systemctl is-active --quiet lsyncd") != 0:
        # now another security check, we do not start lsyncd, when no local drive is mounted
        destination, mountopts = soshelpers_file.get_mountpoint(
            MOUNTDEV[Partition.LOCAL]
        )
        if destination == MOUNT[Partition.LOCAL]:
            # and even not start, when there is no account is on this drive
            accounts = glob.glob(
                os.path.join(
                    sosconstants.MOUNT[sosconstants.Partition.LOCAL],
                    sosconstants.HDDSUBDIR,
                    "*",
                )
            )
            if len(accounts) <= 0:
                logger.info(
                    "lsyncd will not be started as no directories on /mnt/local/syncosync (even no syncosync dir)"
                )
            else:
                logger.info("lsyncd will be started (check and start)")
                os.system("service lsyncd start")
        else:
            logger.info(
                "lsyncd will not be started as local partition is not mounted on local"
            )


def stop_lsyncd():
    """
    stops lsyncd, no matter what state
    :return: True if stopped, False else
    """
    # return if lsyncd is not running
    if os.system("systemctl is-active --quiet lsyncd") != 0:
        return True
    stop_trials = 1  # wait try for 30 seconds to stop
    while os.system("systemctl is-active --quiet lsyncd") == 0 and stop_trials <= 6:
        os.system("service lsyncd stop")
        if os.system("systemctl is-active --quiet lsyncd") != 0:
            logger.info(f"lsyncd is stopped on trial {stop_trials}")
            return True
        else:
            stop_trials += 1
            time.sleep(5)
    logger.error(f"lsyncd could not be stopped after trial {stop_trials}")
    return False


class RemoteHost(configbase.ConfigBase):
    """
    remote host getter, setter and controls
    """

    def __init__(self):
        self.lsyncd_last_read = 0
        self.specific = RemoteHostModel()
        super().__init__(RemoteHostModel(), SOS_CONF_REMOTEHOST)

    @staticmethod
    def provide_model():
        return RemoteHostModel()

    def get_specific(self) -> Optional[RemoteHostModel]:
        """
        requests service specific configuration and returns this to the get call in the parent.
        :return: RemoteHostModel or None if error
        """
        if not os.path.isfile(sosconstants.LSYNCD_CONF_FILE):
            self.set_specific()
            return None
        if int(os.stat(sosconstants.LSYNCD_CONF_FILE).st_mtime) > self.lsyncd_last_read:
            self.specific = read_from_file()
            self.lsyncd_last_read = int(os.stat(sosconstants.LSYNCD_CONF_FILE).st_mtime)
        return self.specific

    def set_specific(self):
        """
        internal method for writing configuration to lsyncd.config
        """
        self.config: RemoteHostModel
        logger.debug(
            f"Set lsyncd configuration to {self.config.hostname}:{self.config.port}"
        )
        set_in_file(sosconstants.LSYNCD_CONF_FILE, self.config)
        self.last_read = int(os.stat(sosconstants.LSYNCD_CONF_FILE).st_mtime)
        soshelpers_shape.shape_mod_outgoing_port(self.config.port)
        return True

    def check_host(self) -> Tuple[RemoteUpStatus, str]:
        """
        checks if a remote host is listening
        :return: RemoteUpStatus
        NOT_AVAILABLE -> show yellow
        UP: Host is up and responding -> show green
        Everything else: host is not up or not responding or no key or whatever -> show red
        """
        # self.config: RemoteHostModel
        self.get()
        return check_host(
            self.config.hostname, self.config.port, str(sosconstants.SOS_KEYFILE)
        )

    def factory_reset(self):
        stop_lsyncd()
        if os.path.isfile(sosconstants.STATIC_LSYNCD_CONF):
            shutil.copy(sosconstants.STATIC_LSYNCD_CONF, sosconstants.LSYNCD_CONF_FILE)
