"""
Some helper functions. Most of them should move to their corresponding classes
"""

#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import base64
import hashlib
import json
import os
import re

from soscore import soshelpers_file
from sosmodel import sos_enums, sosconstants
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def str2base64(to_encode: str) -> str:
    """
    base64 encodes an input str.
    :param to_encode: input to encode
    :return: encoded input
    """
    input_bytes = to_encode.encode("ascii")
    base64_bytes = base64.b64encode(input_bytes)
    return base64_bytes.decode("ascii")


def base642str(to_decode: str) -> str:
    """
    base64 decodes an input str.
    :param to_decode: input to encode
    :return: decoded input
    """
    resbytes = base64.b64decode(to_decode)
    return resbytes.decode("ascii")


def pretty_print(json_string: str, pretty):
    """
    outputs a json string more pretty if desired
    :param json_string:  input string
    :param pretty: True if pretty print desired
    :return: string in pretty printed (or just the input
    """
    if not pretty:
        return json_string
    else:
        return json.dumps(json.loads(json_string), indent=4)


def easy_grep(textarray, searchstring):
    """
    search for searchstring in textarray
    :param textarray:
    :param searchstring:
    :return:
    """
    for line in textarray:
        if searchstring in line:
            return True
    return False


def get_system_list():
    """
    reads list of available systems
    :return: list of systems e.g. generic, sosbp, ...
    """
    system_list = []
    try:
        for dirname in os.listdir(sosconstants.SOS_SYSTEM_DESCRIPTIONS):
            system_list.append(dirname)
    except Exception as e:
        logger.error(
            "No system descriptions found in "
            + sosconstants.SOS_SYSTEM_DESCRIPTIONS
            + " This should not happen:",
            str(e),
        )
    system_list.sort()
    return system_list


def get_dir_space(dir):
    """
    returns the space in bytes of a dir
    :param dir: full path to dir
    :return used space in bytes, 0 also if dir does not exist
    """
    if os.path.exists(dir):
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(dir):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                # skip if it is symbolic link
                if not os.path.islink(fp):
                    total_size += os.path.getsize(fp)
        return total_size
    else:
        return 0


def get_account_used_space(name):
    """
    uses linux quota shell command to retrieve usage of users dir
    :param name:
    :return: returns either bytes used or 0
    """
    try:
        quota_read = os.popen("quota -v -w -u " + name + " | tail -1").read()
        m = re.search(r"^[^\s]*\s*\**([0-9]*).*$", quota_read)
        if m:
            return int(m.group(1)) * 1024
        else:
            logger.warning(f"No quota entry for {name}, returning standard disk usage")
            return get_dir_space(
                os.path.join(
                    sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                    sosconstants.HDDSUBDIR,
                    name,
                    "data",
                )
            )
    except Exception as e:
        dir_space = get_dir_space(
            os.path.join(
                sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                sosconstants.HDDSUBDIR,
                name,
                "data",
            )
        )
        logger.warning(
            f"Something went wrong, calling quota for {name}, returning standard disk usage: {dir_space}"
        )
        logger.debug(f"Issue was: {str(e)}")


def return_youngest_mod(directory):
    """
    return youngest modification time for a file or dir in a directory

    keyword args:
    directory -- the starting root directory of the search

    """
    unix_modified_time = 0
    for root, sub_folders, files in os.walk(directory):
        for file in files:
            try:
                unix_modified_time_new = int(os.stat(os.path.join(root, file)).st_mtime)
                if unix_modified_time_new > unix_modified_time:
                    unix_modified_time = unix_modified_time_new
            except Exception:
                pass
    return unix_modified_time


def check_drive(drive):
    """
    checks if a drive is running
    :param drive: just the device without any path, e.g. sda
    :return: True if drive is up
    """
    quota_read = os.popen("hdparm -C /dev/" + drive).read()
    m = re.search(".*standby.*", quota_read)
    if m:
        return True
    else:
        return False


def sshfingerprint(key) -> str:
    """
    derives a md5 fingerprint from a key in a string
    :param key:
    :return: fingerprint
    """
    try:
        key = base64.b64decode(key.strip().split()[1].encode("ascii"))
        fp_plain = hashlib.md5(key).hexdigest()
        fp = ":".join(a + b for a, b in zip(fp_plain[::2], fp_plain[1::2]))
    except IndexError:
        fp = ""
    return fp


def sshfilefingerprint(filename):
    """
    derives a md5 fingerprint from a ssh-key in a file
    :param filename: filepath
    :return: fingerprint as str
    """
    if os.path.isfile(filename):
        try:
            with open(filename, "r") as f:
                key = f.read()
                return sshfingerprint(key)
        except Exception:
            return ""
    return ""


def set_ssh_pub_key(filepath: str, ssh_pub_key) -> str:
    """
    sets or deletes a ssh_pub_key: if ssh_pub_key is "", then the key is deleted, else it is checked, if it has to
    change or not. If ssh_pub_key is None, nothing will happen.
    :param filepath: full path for the public key
    :param ssh_pub_key: ssh_pub_key in one string
    :return: fingerprint as str
    """
    if ssh_pub_key is None:
        return sshfilefingerprint(filepath)
    if ssh_pub_key == "":
        # ok, this means, we have just to delete the key if there is one
        if os.path.isfile(os.path.join(filepath)):
            os.remove(os.path.join(filepath))
            logger.debug(f"Deleted ssh pub key: {filepath}")
        return ""
    else:
        # now check if there is already a key and load it to compare
        if os.path.isfile(filepath):
            try:
                with open(filepath) as ssh_pub_key_file:
                    existing_key = ssh_pub_key_file.read()
            except Exception as e:
                logger.error(f"Could not read key from {filepath}:", str(e))
        else:
            existing_key = ""  # this makes comparison sure if there is no key
        if existing_key != ssh_pub_key:
            # ok, finally write the key.
            soshelpers_file.write_to_file(filepath, ssh_pub_key)

        return sshfilefingerprint(filepath)


def md5Checksum(filePath):
    """
    calculate md5 sum of a file
    :param filePath:
    :return: md5sum as str
    """
    with open(filePath, "rb") as fh:
        m = hashlib.md5()
        while True:
            data = fh.read(8192)
            if not data:
                break
            m.update(data)
        return m.hexdigest()


def parse_shadowpwd_data(pwd):
    m = re.search(r"!?(?P<salt>\$[0-9]\$[^$]*)\$(?P<pwhash>.*)", pwd)
    if m:
        return m.groupdict()
    else:
        logger.error(f"Shadow entry {pwd} is not parseable?")
        return None
