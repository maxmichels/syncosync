"""
About managing the remote host systemsetupdata

"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from soscore import configbase
from sosmodel.sosconstants import SOS_CONF_REMOTEHOST_SSD
from sosmodel.systemsetupdata import SystemSetupDataModel
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


class RemoteHostSystemSetupData(configbase.ConfigBase):
    """Holds some systemsetupdata from the remote host, which is received during key exchange"""

    def __init__(self):
        """
        normally nothing more than calling supers init with the right config file name and the right model
        """
        super().__init__(SystemSetupDataModel(), SOS_CONF_REMOTEHOST_SSD)

    @staticmethod
    def provide_model():
        return SystemSetupDataModel()
