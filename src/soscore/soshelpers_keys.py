"""
helper functions for keys
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from soscore import soshelpers
from sosmodel import sosconstants
from sosmodel.soskey import SosKeyModel
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def get_remote_key():
    """
    get remote pub key and fingerprint
    :return: remote key
    """
    remote_key = SosKeyModel()
    if os.path.isfile(sosconstants.REMOTE_KEYFILE_PUB):
        with open(sosconstants.REMOTE_KEYFILE_PUB, "r") as f:
            remote_key.pub_key = f.read().rstrip()
            remote_key.fingerprint = soshelpers.sshfingerprint(remote_key.pub_key)
    else:
        logger.debug("Requested fingerprint for remote key but no key file is existing")
    return remote_key
