"""
Helpers for traffic shaping
"""

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import os
import re
from typing import Tuple

from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def is_shape_on():
    """
    Try to find out if shaping has already activated
    """
    tc_output = os.popen("tc qdisc show dev eth0").readlines()
    for line in tc_output:
        if re.search("qdisc netem", line):
            return True
    return False


def shape_on(port: int = 55055, outgoing: int = 10000, incoming: int = 10000):
    """
    Right now, we don't think too much but do it like a bash script
    """
    # outgoing
    os.system("tc qdisc add dev eth0 root handle 1a1a: htb default 1")
    os.system(
        f"tc class add dev eth0 parent 1a1a: classid 1a1a:142 htb rate {outgoing}Mbit"
    )
    os.system("tc qdisc add dev eth0 parent 1a1a:142 handle 2f1b: netem")
    os.system(
        f"tc filter add dev eth0 protocol ip parent 1a1a: prio 5 u32 match ip dst 0.0.0.0/0 match ip src 0.0.0.0/0 match ip dport {port} 0xffff flowid 1a1a:142"
    )

    os.system("modprobe ifb")
    os.system("ip link add ifb6682 type ifb")
    os.system("ip link set dev ifb6682 up")
    os.system("tc qdisc add dev eth0 ingress")
    os.system(
        "tc filter add dev eth0 parent ffff: protocol ip u32 match u32 0 0 flowid 1a1a: action mirred egress redirect dev ifb6682"
    )
    os.system("tc qdisc add dev ifb6682 root handle 1a1a: htb default 1")
    os.system(
        f"tc class add dev ifb6682 parent 1a1a: classid 1a1a:209 htb rate {incoming}Mbit"
    )
    os.system("tc qdisc add dev ifb6682 parent 1a1a:209 handle 2ab2: netem")
    os.system(
        "tc filter add dev ifb6682 protocol ip parent 1a1a: prio 5 u32 match ip dst 0.0.0.0/0 match ip src 0.0.0.0/0 match ip dport 55055 0xffff flowid 1a1a:209"
    )


def shape_off():
    """
    Turns shaping off
    :return:
    """
    os.system("tc qdisc del dev eth0 root")
    os.system("tc qdisc del dev eth0 ingress")
    os.system("tc qdisc del dev ifb6682 root")
    os.system("ip link set dev ifb6682 down")
    os.system("ip link delete ifb6682")
    os.system("rmmod ifb")


def shape_mod_bw(outgoing: int, incoming: int):
    """
    Shapes in mbit set
    """
    os.system(
        f"tc class change dev eth0 parent 1a1a: classid 1a1a:142 htb rate {outgoing}Mbit"
    )
    os.system(
        f"tc class change dev ifb6682 parent 1a1a: classid 1a1a:209 htb rate {incoming}Mbit"
    )
    logger.debug(
        f"Traffic shaping was set to {outgoing}Mbit out and {incoming}Mbit in", outgoing
    )


def shape_mod_outgoing_port(port: int):
    """changes outgoing port"""
    logger.debug(f"Changing outgoing port to {int(port)}")
    os.system(
        f"tc filter replace dev eth0 protocol ip parent 1a1a: prio 5 u32 match ip dst 0.0.0.0/0 match ip src 0.0.0.0/0 match ip dport {port} 0xffff flowid 1a1a:142"
    )


def get_traffic() -> Tuple[int, int]:
    try:
        stream = os.popen(
            "tc -s class show dev ifb6682 | grep Sent | awk  'BEGIN { ORS=\" \" }; { print $2 }'"
        )
        incoming = int(stream.read())
    except Exception:
        incoming = 0
    try:
        stream = os.popen("tc -s class show dev eth0 | grep Sent | awk  '{ print $2 }'")
        outgoing = int(stream.read())
    except Exception:
        outgoing = 0
    return outgoing, incoming
