"""
Setting sosadmin password
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from soscore import soshelpers_file
from sosmodel import sosconstants
from sosmodel.sosadmin_passwd import SosAdminPassWordModel
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def set(newpassword: SosAdminPassWordModel):
    """
    sets a new password for sosadmin, which has to be hashed already
    :param newpassword: hashed paswword
    :return:
    """
    command = "usermod -p '" + newpassword.password + "' " + sosconstants.SOSADMIN_USER
    if os.system(command):
        logger.error(f"Could not change password for {sosconstants.SOSADMIN_USER}")
        return False
    logger.info(f"Password changed for {sosconstants.SOSADMIN_USER}")
    soshelpers_file.set_configbackup_flag()
    return True
