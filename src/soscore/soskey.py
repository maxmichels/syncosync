"""
About generating and handling the syncosync key
"""
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Class to handle the syncosync key. Note: key exchange is a different file and class
import os
import subprocess

from soscore import configbackup_backup, soshelpers, soshelpers_file, sshdmanager
from sosmodel import sosconstants
from sosmodel.sos_enums import Sshd, SshdAccess
from sosmodel.soskey import SosKeyModel
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


class SosKey:
    def __init__(self):
        self.soskey = SosKeyModel()
        self.last_read = 0  # last time the config was refreshed from it's file. Should be the timestamp of the file
        self.__update()

    def __update(self):
        """
        sets fingerprint from keyfile
        :return:
        """
        if os.path.isfile(sosconstants.SOS_KEYFILE_PUB):
            if self.last_read < int(os.stat(sosconstants.SOS_KEYFILE_PUB).st_mtime):
                with open(sosconstants.SOS_KEYFILE_PUB) as f:
                    self.soskey.pub_key = f.read().rstrip()
                self.soskey.fingerprint = soshelpers.sshfingerprint(self.soskey.pub_key)
                self.last_read = int(os.stat(sosconstants.SOS_KEYFILE_PUB).st_mtime)
        else:
            self.soskey.fingerprint = None

    def generate(self):
        """
        Generates the syncosync key without
        :return:
        True if success
        False else
        """
        result = True
        if not os.path.isdir(sosconstants.SOS_KEYFILE_DIR):
            os.mkdir(sosconstants.SOS_KEYFILE_DIR)
        try:
            subprocess.run(
                f"echo 'y' | ssh-keygen -q -N '' -f {sosconstants.SOS_KEYFILE} -C 'syncosync'",
                shell=True,
            )
            self.__update()
            logger.info(f"Generated syncosync key with fp {self.soskey.fingerprint}")
            self.last_read = int(os.stat(sosconstants.SOS_KEYFILE_PUB).st_mtime)
            configbackup_backup.backup()
        except Exception as e:
            logger.error(
                f"Was not able to generate sos key in {sosconstants.SOS_KEYFILE_DIR}: {str(e)}"
            )
            self.soskey.fingerprint = None
            result = False
        return result

    def get(self):
        """
        standard getter
        :return:  SosKeyModel
        """
        self.__update()
        return self.soskey

    @staticmethod
    def factory_reset():
        sshdmanager.change_access(Sshd.EXTRANET, SshdAccess.OFF)
        soshelpers_file.del_file_if_exits(sosconstants.SOS_KEYFILE)
        soshelpers_file.del_file_if_exits(sosconstants.SOS_KEYFILE_PUB)
