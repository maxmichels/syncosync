"""
managing sos dedicated configuration which is not in other configuration areas
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from soscore import configbase, soshelpers_hardware
from sosmodel import sos_enums, sosconstants
from sosmodel.sosconfig import SosConfigModel
from sosmodel.systemdescription import SystemDescriptionModel
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def get_vguuid() -> str:
    """Just return vg uuid"""
    myconfig = SosConfig().get()
    return myconfig.vg_sos_uuid


class SosConfig(configbase.ConfigBase):
    def __init__(self):
        """
        normally nothing more than calling supers init with the right config file name and the right model
        """
        super().__init__(SosConfigModel(), sosconstants.SOS_CONF_GENERAL)

    @staticmethod
    def provide_model():
        return SosConfigModel()

    def get(self) -> SosConfigModel:
        """returns the actual configuration
        Returns:
            SosConfigModel: the configuration
        """
        myconfig: SosConfigModel = super().get()
        if myconfig.systype == sos_enums.SysType.UNKNOWN:
            myconfig.systype = soshelpers_hardware.get_systype()

            super().set()
        return myconfig

    def get_system_description(self) -> SystemDescriptionModel:
        """
        load the system description part of the config
        :return:
        """
        self.config: SosConfigModel
        self.get()
        type_config_file = os.path.join(
            sosconstants.SOS_SYSTEM_DESCRIPTIONS,
            sos_enums.SysType.typename(self.config.systype),
            "type_config.json",
        )
        if os.path.isfile(type_config_file):
            with open(type_config_file) as json_file:
                mysystemdescription = SystemDescriptionModel.from_json(json_file.read())
                if mysystemdescription.version != sosconstants.SOS_VERSION:
                    logger.warning(
                        f"type config file {type_config_file} has different version "
                    )
        else:
            logger.error(
                f"type config file for system {sos_enums.SysType.typename(self.config.systype)}"
                f"({self.config.systype}):{type_config_file} is not existing.",
            )
            mysystemdescription = SystemDescriptionModel()
        return mysystemdescription
