"""
About managing the timezone

"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import os
import subprocess
from typing import List

from soscore import configbase
from sosmodel.sosconstants import SOS_CONF_TIMEZONE
from sosmodel.timezone import TimeZoneModel
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


class TimeZone(configbase.ConfigBase):
    def __init__(self):
        """
        normally nothing more than calling supers init with the right config file name and the right model
        """
        super().__init__(TimeZoneModel(), SOS_CONF_TIMEZONE)

    @staticmethod
    def provide_model():
        return TimeZoneModel()

    @staticmethod
    def get_specific() -> TimeZoneModel:
        """
        requests service specific configuration and returns this to the get call in the parent.
        :return:
        """
        result = TimeZoneModel()
        td_output = os.popen("timedatectl show").readlines()
        for line in td_output:
            item, content = line.split("=")
            if item == "Timezone":
                result.timezone = content.rstrip()
        return result

    @staticmethod
    def get_zone_list() -> List[str]:
        """provides an array with possible timezones"""
        td_output = os.popen("timedatectl list-timezones").read()
        return td_output.splitlines()

    def set_specific(self) -> bool:
        """
        is called by the set method in the parent, if the configuration has changed. Should return True, when
        successful
        :return:
        """
        self.config: TimeZoneModel
        try:
            subprocess.run(
                f"timedatectl set-timezone {self.config.timezone}", shell=True
            )
            logger.info(f"Changed timezone to {self.config.timezone}")
        except Exception:
            logger.error(f"Was not able to set timezone to {self.config.timezone}")
            return False
        return True
