"""
About managing the hostname

This class - as it is the most easy using configbase - is also the example implementation of a configuration

"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import socket
import subprocess

from soscore import configbase
from soscore.soshelpers_file import write_to_file
from sosmodel.hostname import HostNameModel
from sosmodel.sosconstants import SOS_CONF_HOSTNAME
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


class HostName(configbase.ConfigBase):
    def __init__(self):
        """
        normally nothing more than calling supers init with the right config file name and the right model
        """
        super().__init__(HostNameModel(), SOS_CONF_HOSTNAME)

    @staticmethod
    def provide_model():
        return HostNameModel()

    @staticmethod
    def get_specific() -> HostNameModel:
        """
        requests service specific configuration and returns this to the get call in the parent.
        :return:
        """
        result = HostNameModel()
        result.hostname = socket.gethostname()
        return result

    def set_specific(self) -> bool:
        """
        is called by the set method in the parent, if the configuration has changed. Should return True, when
        successful
        :return:
        """
        self.config: HostNameModel
        try:
            subprocess.run(
                f"hostnamectl set-hostname {self.config.hostname}", shell=True
            )
            logger.info(f"Changed hostname to {self.config.hostname}")
        except Exception:
            logger.error(f"Was not able to set hostname to {self.config.hostname}")
            return False
        write_to_file("/etc/mailname", self.config.hostname)
        logger.info("Network config has changed, restarting networking")
        try:
            subprocess.run("service networking restart", shell=True)
        except Exception:
            logger.error("Network restart not possible")
        return True

    def factory_reset(self):
        self.config = HostNameModel()
        self.set_specific()
