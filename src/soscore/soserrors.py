#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from typing import Optional

from sosmodel.localizedMessages import LocalizedMessage


class SosError(Exception):
    """Base class for exceptions in this module."""

    def __init__(
        self,
        message: Optional[str] = None,
        localized_message: Optional[LocalizedMessage] = None,
    ):
        self._message: Optional[str] = message
        self._localized_message: Optional[LocalizedMessage] = localized_message

    @property
    def message(self) -> Optional[str]:
        return self._message

    @message.setter
    def message(self, message: Optional[str] = None) -> None:
        self._message = message

    @property
    def localized_message(self) -> Optional[LocalizedMessage]:
        return self._localized_message

    @localized_message.setter
    def localized_message(
        self, localized_message: Optional[LocalizedMessage] = None
    ) -> None:
        self._localized_message = localized_message


class AccountAlreadyExistingError(SosError):
    """Account already existing (user or system account)"""

    pass


class AccountCreationError(SosError):
    """Something went wrong while creating the account"""

    pass


class VolumeNotMounted(SosError):
    """no local or remote volume mounted"""

    pass


class ConfigBackupFilenameError(SosError):
    """the filename does not match conventions"""

    pass


class ConfigBackupFileDecryptionError(SosError):
    """the config backup file could not be decryptes"""

    pass


class VolumeSizeNotPossible(SosError):
    """the volume size is not possible"""

    pass


class VolumeResizeError(SosError):
    """volume resize failed"""

    pass


class ModeSwitchFailed(SosError):
    """mode switch failed"""

    pass


class ModeDoesNotAllowKeyExchange(SosError):
    """In this systemmode, a key exchange is not possible"""

    pass


class DriveNotFormatted(SosError):
    """The drive is not formatted"""

    pass
