"""
helper functions for I18n
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import json
import os
from string import Formatter

from sosmodel import sosconstants
from sosmodel.sos_enums import SupportedLanguage
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def translate_msg(key, data=None, lang=SupportedLanguage.UNSET):
    # this has to be local, if not, it will get circular
    from soscore.sosconfig import SosConfig

    """Loads a string from soscore.*.json in the given language and returns it

        :param key: key of content
        :type key: str
        :param data: dict with keywords to replace
        :type data: dict
        :param lang: language to use
        :type lang: SupportedLanguage
        :return: key content in Language lang
        :rtype: str
            """
    # If lang was not set use config lang
    if data is None:
        data = {}
    if lang == SupportedLanguage.UNSET:
        lang = SupportedLanguage.to_iso_core(SosConfig().get().ui_language.language)
    else:
        lang = SupportedLanguage.to_iso_core(lang)
    success = False
    content = ""
    # Check if language translation exists
    source = os.path.join(sosconstants.trans_place, f"soscore.{lang}.json")

    if not os.path.isfile(source):
        # If translation doesnt exist, use english as default
        logger.warn(
            f"For the given language: {repr(lang)}, exists no translation! "
            f"English was used as default"
        )
        lang = "en"

    while not success:
        source = os.path.join(sosconstants.trans_place, f"soscore.{lang}.json")

        with open(source, "r") as f:
            obj = json.loads(f.read())

        # There is a file but no key
        try:
            content = obj[key]
        except KeyError:
            logger.warning(
                f"Key Error for {repr(lang)}: {repr(key)} does not exist in this language"
            )
            if lang == "en":
                return key
            logger.warning("Loading key in english instead")
            lang = "en"
            continue

        # There is a translation file with key but the data for the key is empty
        if not content:
            logger.warning(
                f"The {repr(lang)} translation has no data for the key {repr(key)}"
            )
            if lang == "en":
                return key
            else:
                logger.warning("Loading key in english instead")
                lang = "en"
        else:
            success = True

    # At this point the content has been loaded in some language and is not empty
    fieldnames = [fname for _, fname, _, _ in Formatter().parse(content) if fname]
    # check if dict has more fields then content
    if len(fieldnames) < len(data):
        logger.warning(
            f"In the {repr(lang)} translation file for key {repr(key)} the provided data {data}has "
            f"more fields than necessary"
        )
    for field in fieldnames:
        if field not in data:
            data.update({f"{field}": f"{{{field}}}"})
            logger.warning(
                f"In the {repr(lang)} translation file, for key {repr(key)} and "
                f"field {repr(field)} no data was provided"
            )

    return content.format(**data)
