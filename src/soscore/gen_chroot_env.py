"""
Provides function to generate the chroot environment
"""

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import subprocess
from shutil import copy2

from sosmodel import sosconstants
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.soscore)


def gen_chroot_env(binlist=None):
    """
    Function to generate chroot environment
    """
    if binlist is None:
        binlist = ["bash", "rsync"]
    # Chroot root directory
    target = sosconstants.CHROOT_GEN_ENV
    # Create chroot directory with bin inside
    try:
        os.makedirs(os.path.join(target, "bin"))
        logger.info("Created chroot root directory with bin")
    except FileExistsError:
        logger.warning("Chroot dir with bin already exists")
    except PermissionError:
        logger.critical("Could not create chroot dir due to insufficient permissions")
        raise
    except OSError:
        logger.critical("Something went wrong while creating the chroot root directory")
        raise

    # Copy binaries to chroot bin
    copy_binaries(binlist, target)

    # Get dependencies, filter relevant parts
    for binary in binlist:
        p1 = subprocess.Popen(
            ["ldd", os.path.join(target, os.path.join("bin/", binary))],
            stdout=subprocess.PIPE,
        )
        try:
            deps = (
                subprocess.check_output(
                    ["egrep", "-o", "/lib.*\\.[0-9]"], stdin=p1.stdout
                )
                .decode("utf-8")
                .split("\n")
            )
            deps = deps[: len(deps) - 1]
        except subprocess.CalledProcessError as e:
            if e.returncode > 1:
                logger.critical("Error while searching for the right dependencies")
            elif e.returncode == 1:
                logger.critical("No matches were found")
            raise
        else:
            # Copy dependencies to the new Environment
            copy_dependencies(deps, target)


def copy_dependencies(deps, target):
    for dep in deps:
        # Make needed directories
        try:
            # Get path up to file and create any missing directories
            src = dep[: dep.rfind("/")]
            # Create needed directories
            # Ignoring already existing dirs
            os.makedirs(os.path.join(target + src), exist_ok=True)
            # Copyfile into the directories
            if os.path.isfile(dep):
                source = dep
            elif os.path.isfile(os.path.join("/usr", dep[1:])):
                source = os.path.join("/usr", dep[1:])
            else:
                logger.critical("Could not find the following dependency: " + dep)
                raise
            copy2(source, os.path.join(target + dep))
            logger.debug("Successfully copied " + source + " to " + target + dep)
        except PermissionError:
            logger.critical("Could not copy " + dep + ", Permission denied")
            raise
        except IsADirectoryError:
            logger.critical("Error, destination is a directory")
            raise
        except OSError:
            logger.critical("An Error occurred while copying the dependencies")
            raise


def copy_binaries(binlist, target):
    for binary in binlist:
        try:
            if os.path.isfile(os.path.join("/bin", binary)):
                source = "/bin"
            elif os.path.isfile(os.path.join("/usr/bin", binary)):
                source = "/usr/bin"
            else:
                logger.critical("Could not find the following binary: " + binary)
                raise
            copy2(os.path.join(source, binary), os.path.join(target, "bin/", binary))
            logger.debug(
                "Successfully copied "
                + os.path.join(source, binary)
                + " to "
                + os.path.join(target, "bin/", binary),
            )
        except PermissionError:
            logger.critical("Could not copy binary '" + binary + "', Permission denied")
            raise
        except IsADirectoryError:
            logger.critical("Error, destination is a directory")
            raise
        except OSError:
            logger.critical("An error occurred while copying the Binaries")
            raise
