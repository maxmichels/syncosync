import argparse

from sosmodel import sosconstants
from sosmodel.sosconstants import LOGFILE_BASENAME, LOGFILE_DESTINATION


def parser_log_args(parser: argparse.ArgumentParser) -> None:
    parser.add_argument(
        "--disable_file_logging",
        action="store_true",
        default=False,
        help=argparse.SUPPRESS,
        # help="Disable file logging (Default: file logging is enabled by default)",
    )
    parser.add_argument(
        "--log_path",
        default=f"{LOGFILE_DESTINATION}{LOGFILE_BASENAME}",
        help=argparse.SUPPRESS,
        # help="File path log should be appended to.",
    )
    parser.add_argument(
        "--log_file_rotation",
        default="50 MB",
        help=argparse.SUPPRESS,
        # help=(
        #     "This parameter expects a human-readable value like"
        #     " '18:00', 'sunday', 'weekly', 'monday at 12:00' or"
        #     " a maximum file size like '100 MB' or '0.5 GB'."
        #     " Set to '0' to disable completely. (Default: 50 MB)"
        # ),
    )
    parser.add_argument(
        "-v",
        "--console_log_level",
        default="WARNING",
        help=(
            "Sets the loglevel for stdout output (DEBUG, INFO, SUCCESS, WARNING, ERROR, CRITICAL)"
        ),
    )
    parser.add_argument(
        "-vf",
        "--log_file_level",
        default="DEBUG",
        help=argparse.SUPPRESS,
        # help="File logging level. See description for --console_log_level.",
    )
    parser.add_argument(
        "--log_file_retention",
        default="10",
        help=argparse.SUPPRESS,
        # help=(
        #     "Amount of days to keep file logs. Set to 0 to"
        #     " keep them forever (Default: 10)"
        # ),
    )
    parser.add_argument(
        "--no_log_colors",
        action="store_true",
        default=False,
        help=argparse.SUPPRESS,
        # help="Disable colored logs.",
    )


def parse_args():
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="the syncosync main program (daemon)",
        epilog=sosconstants.EPILOG,
    )
    parser.add_argument(
        "-d",
        "--debug",
        help="do not detach and send logging also to stdout",
        default=False,
        action="store_true",
    )
    parser.add_argument(
        "--root-path",
        help="Set location of Flask root",
        type=str,
        default=sosconstants.STATIC_BASE_DIR,
    )

    parser_log_args(parser)
    parser.set_defaults(DEBUG=False)

    args = parser.parse_args()
    return args
