#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import logging
import os
from multiprocessing import Process
from typing import Optional

from flask import Flask, send_from_directory
from flask_cors import CORS
from soscore.soscoreManager import SoscoreManager
from sosmodel.sosconstants import STATIC_MANUAL_DIR
from sosui.routes.api.account_management import register_account_management_endpoints
from sosui.routes.api.auth import register_auth_endpoints
from sosui.routes.api.public import register_public_endpoints
from sosui.routes.api.remote_recovery import register_remote_recovery_endpoints
from sosui.routes.api.setup import register_setup_endpoints
from sosui.routes.api.status import register_status_endpoints
from sosui.routes.sosui_root_endpoint import SosuiRootEndpoint
from sosutils.logging import InterceptHandler, LoggerCategory, get_logger
from werkzeug.routing import BaseConverter

logger = get_logger(LoggerCategory.sosui)
logging.getLogger("werkzeug").setLevel(logging.DEBUG)
logging.getLogger("werkzeug").addHandler(
    InterceptHandler(log_section=LoggerCategory.werkzeug)
)


class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]


class Sosui(Process):
    """
    Debugging non-debug running of WSGI in pycharm requires enabling of gevent support:
    Settings | Python Debugger | Gevent compatible debugging.
    """

    def __init__(
        self,
        soscore_manager: SoscoreManager,
        root_path: Optional[str] = None,
        debug_mode: bool = False,
    ):
        Process.__init__(self, name="SosUI")
        self.__root_path: Optional[str] = root_path
        self.__debug_mode: bool = debug_mode
        self._soscore_manager = soscore_manager
        self.__uid: Optional[int] = None
        self.__gid: Optional[int] = None
        self.__setup_flask()

    def __setup_flask(self):
        self.__flask_app = Flask("SOSUI", static_url_path="", static_folder="static")
        # TODO: randomize the secret key
        self.__flask_app.config["SECRET_KEY"] = os.urandom(24)
        self.__flask_app.config["CORS_HEADERS"] = "Content-Type"
        self.__flask_app.config["SEND_FILE_MAX_AGE_DEFAULT"] = 1
        self.__flask_app.url_map.converters["regex"] = RegexConverter
        if self.__root_path:
            self.__flask_app.root_path = self.__root_path
        logger.debug(f"Root path: {str(self.__flask_app.root_path)}")
        if self.__debug_mode:
            CORS(self.__flask_app)

        self.__load_routes()

    def run(self):
        logger.info(f"Running sosui with PID {str(os.getpid())}")
        if self.__uid and self.__gid:
            Sosui.report_ids("starting demotion of sosui")
            os.setgid(self.__gid)
            os.setuid(self.__uid)
            Sosui.report_ids("finished demotion of sosui")
        self.__flask_app.run(threaded=True, host="127.0.0.1", port=5000)
        self.stop_sosui()
        logger.info("Sosui stopped")

    def stop_sosui(self):
        logger.info("Stopping sosui")
        if self.__debug_mode:
            logger.debug("Thanks for debugging, flask will just die along the way")

    def start(self, user_uid=None, user_gid=None):
        # TODO: at this point, we could patch in/modularize the UI according to the box state
        # e.g. if the ESA has not been run through yet, only load up relevant parts?
        # self.run(threaded=True, host="0.0.0.0")
        self.__uid = user_uid
        self.__gid = user_gid
        Process.start(self)

    def __load_routes(self):
        # TODO: Have one endpoint handle /api/ by regex and delegating?
        register_public_endpoints(self.__flask_app, self._soscore_manager)
        register_status_endpoints(self.__flask_app, self._soscore_manager)
        register_account_management_endpoints(self.__flask_app, self._soscore_manager)
        register_setup_endpoints(self.__flask_app, self._soscore_manager)
        register_auth_endpoints(self.__flask_app, self._soscore_manager)
        register_remote_recovery_endpoints(self.__flask_app, self._soscore_manager)

        root_endpoint: SosuiRootEndpoint = SosuiRootEndpoint(
            self.__flask_app, self._soscore_manager
        )

        self.__flask_app.add_url_rule(
            rule="/", endpoint="catch_all", view_func=root_endpoint, methods=["GET"]
        )

        self.__flask_app.add_url_rule(
            rule='/<regex(?!api)(".*"):path>',
            endpoint="catch_all",
            view_func=root_endpoint,
            methods=["GET"],
        )

        def serve_manual(path: str = "", lang: str = ""):
            logger.debug(f"Serve Manual: {lang}, {path}")
            if not os.path.isdir(os.path.join(STATIC_MANUAL_DIR, lang)):
                lang = "en-US"
            return send_from_directory(os.path.join(STATIC_MANUAL_DIR, lang), path)

        self.__flask_app.add_url_rule(
            rule='/<regex("(?!api)([a-zA-Z]{2,3}(-[a-zA-Z]{2,3})?)"):lang>/manual/<regex(".*"):path>',
            view_func=serve_manual,
            methods=["GET"],
        )

    @staticmethod
    def report_ids(msg):
        logger.debug(f"uid, gid = {os.getuid()}, {os.getgid()}: {msg}")
