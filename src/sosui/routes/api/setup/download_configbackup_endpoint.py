# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2022  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import os.path
from typing import Optional

from flask import Flask, Response, request, send_file
from soscore.configbackup import get_latest_filepath
from soscore.soscoreManager import SoscoreManager
from sosui.sosui_endpoint import SosuiEndpoint, allowed_methods, requires_authentication
from sosui.status_codes import StatusCodes
from sosutils.logging import LoggerCategory, get_logger

logger = get_logger(LoggerCategory.sosui)


class DownloadConfigbackupEndpoint(SosuiEndpoint):
    def __init__(self, flask_app: Flask, soscore_manager: SoscoreManager):
        super().__init__(flask_app, soscore_manager)

    @requires_authentication()
    @allowed_methods(["GET"])
    def _action(self, path: Optional[str] = None, lang: Optional[str] = None):
        if request.method == "GET":
            return self.get(path, lang)

    def get(self, path: Optional[str] = None, lang: Optional[str] = None) -> Response:
        latest_filepath: str = get_latest_filepath()
        if not latest_filepath:
            return SosuiEndpoint.get_response(response_status=StatusCodes.NOT_FOUND)
        if not os.path.exists(latest_filepath) or not os.path.isfile(latest_filepath):
            # Unknown filename upcoming...
            logger.warning("Failed providing latest configbackup")
            return SosuiEndpoint.get_response(
                response_status=StatusCodes.INTERNAL_SERVER_ERROR
            )
        return send_file(latest_filepath, as_attachment=True)
