# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from typing import Optional

from flask import Flask, Response, request
from soscore.soscoreManager import SoscoreManager
from sosmodel.mailconfig import MailConfigModel
from sosui.sosui_endpoint import SosuiEndpoint, allowed_methods, requires_authentication
from sosui.status_codes import StatusCodes


class MailConfigEndpoint(SosuiEndpoint):
    def __init__(self, flask_app: Flask, soscore_manager: SoscoreManager):
        super().__init__(flask_app, soscore_manager)

    @requires_authentication()
    @allowed_methods(["GET", "POST"])
    def _action(
        self, path: Optional[str] = None, lang: Optional[str] = None
    ) -> Response:
        if request.method == "GET":
            return self._get()
        elif request.method == "POST":
            return self._post()
        else:
            return SosuiEndpoint.get_response(
                response_status=StatusCodes.METHOD_NOT_ALLOWED
            )

    def _get(self):
        mail_config: MailConfigModel = self._soscore_manager.get_mail_config()
        return SosuiEndpoint.get_response(
            response_status=StatusCodes.OK, payload=mail_config.to_json()
        )

    def _post(self):
        new_mail_config: MailConfigModel = MailConfigModel.from_json(request.data)
        self._soscore_manager.set_mail_config(new_mail_config)
        return SosuiEndpoint.get_response(
            response_status=StatusCodes.OK, payload=new_mail_config.to_json()
        )
