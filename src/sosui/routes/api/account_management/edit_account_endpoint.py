# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import crypt
from typing import Optional

from flask import Flask, Response, request
from soscore.soscoreManager import SoscoreManager
from soscore.soserrors import SosError
from sosmodel.sosaccount import SosAccountEditModel
from sosui.sosui_endpoint import SosuiEndpoint, allowed_methods, requires_authentication
from sosui.status_codes import StatusCodes


class EditAccountEndpoint(SosuiEndpoint):
    def __init__(self, flask_app: Flask, soscore_manager: SoscoreManager):
        super().__init__(flask_app, soscore_manager)

    @requires_authentication()
    @allowed_methods(["PUT"])
    def _action(
        self, path: Optional[str] = None, lang: Optional[str] = None
    ) -> Response:
        edit_user: SosAccountEditModel = SosAccountEditModel.from_json(request.data)

        if edit_user.name is None or len(edit_user.name) == 0:
            # return error!
            raise SosError("No account name was specified.")

        # hash the PW right now...
        if SosAccountEditModel.is_invalid_configuration(edit_user):
            raise SosError(
                "At least a password or a public key needs to be set for a user after editing."
            )
        elif edit_user.delete_password:
            # delete password
            edit_user.password = ""
        elif edit_user.password:
            edit_user.password = crypt.crypt(edit_user.password)

        self._soscore_manager.account_mod(edit_user)
        return SosuiEndpoint.get_response(response_status=StatusCodes.OK)
