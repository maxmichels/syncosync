#!/usr/bin/env python3
"""
Remotely recovers data
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse

# following imports are all necessary for cli
import re
import signal
import subprocess
import sys
import time

import soscore.sysstate
from soscore import remotehost, soshelpers_file, sysstate
from sosmodel import sosconstants
from sosmodel.remotehost import RemoteHostModel
from sosmodel.sos_enums import RemoteUpStatus, SystemMode
from sosmodel.sosconstants import RR_RESULT
from sosmodel.sostocheck import SosToCheck
from sosmodel.sysstate import SysStateResult
from sosutils.logging import LoggerCategory, get_logger, init_logging
from sosutils.runtime_args import parser_log_args


def thousandsint(thousandsstr: str) -> int:
    return int(thousandsstr.replace(",", ""))


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="reads - and caches - account size and access date",
        epilog=sosconstants.EPILOG,
    )
    # parser.add_argument("-s", "--simulate", help="use rsync simulation", action="store_true")

    parser_log_args(parser)
    args = parser.parse_args()
    init_logging(args)
    logger = get_logger(LoggerCategory.soscli)

    # stop_event: Event = Event()
    # main_pid: int = os.getpid()
    mystate = sysstate.SysState()
    finished = False
    rhost = remotehost.RemoteHost()

    # if mode is not REMOTE_RECOVERY, exit positive
    savedstate = sysstate.read_mode_from_file()
    if savedstate.mode != SystemMode.REMOTE_RECOVERY:
        logger.debug("As stored mode is not recovery, no action required")
        exit(0)

    while not finished:

        def signal_handler(sig, frame):
            logger.info(f"Received signal: {sig}")
            logger.warning(f"{host_status.message()}")
            soscore.sysstate.result_file_append(
                RR_RESULT, SystemMode.REMOTE_RECOVERY, "Stopped with SIGINT"
            )
            exit(0)

        # catches signal in all subprocesses for a proper shutdown
        signal.signal(signal.SIGINT, signal_handler)
        (host_status, uuid) = rhost.check_host()
        if host_status == RemoteUpStatus.UP and uuid != "":
            rhostcfg: RemoteHostModel = rhost.get()
            tocheck = SosToCheck()

            cmd = (
                f'rsync -aW --no-compress --stats --dry-run -e "/usr/bin/ssh -p {rhostcfg.port} -i /etc/syncosync/sshkeys/syncosync'
                f' -o User=syncosync -o StrictHostKeyChecking=no  -o UserKnownHostsFile=/dev/null" {rhostcfg.hostname}:remote/syncosync/*'
                f"  /mnt/local/syncosync/"
            )
            proc = subprocess.Popen(
                cmd,
                shell=True,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
            )

            remainder = proc.communicate()[0].decode("ascii")
            mn = re.findall(r"Number of files: ([\d,]+)", remainder)
            total_files = thousandsint(mn[0])
            tocheck.start_elements = total_files
            logger.debug(f"total files: {total_files}")

            cmd = (
                f'rsync -avW  --inplace --no-compress --progress --no-inc-recursive -e "/usr/bin/ssh -p {rhostcfg.port} -i /etc/syncosync/sshkeys/syncosync'
                f' -o User=syncosync -o StrictHostKeyChecking=no  -o UserKnownHostsFile=/dev/null" {rhostcfg.hostname}:remote/syncosync/*'
                f"  /mnt/local/syncosync/"
            )
            proc = subprocess.Popen(
                cmd,
                shell=True,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
            )
            while True:
                output = proc.stdout.readline()
                if b"to-chk" in output:
                    m = re.findall(r"to-chk=(\d+)/(\d+)", output.decode("ascii"))
                    logger.debug(f"rsync status: {int(m[0][0])}/{int(m[0][1])}")
                    tocheck.to_check = int(m[0][0])
                    tocheck.percent = int(
                        100 * (int(m[0][1]) - int(m[0][0])) / int(m[0][1])
                    )
                    soshelpers_file.write_to_file(
                        sosconstants.RRTOCHECK_CACHE, tocheck.to_json()
                    )
                    if int(m[0][0]) == 0:
                        break
                if output == b"":
                    break
            logger.info(f"rsync is ready with {tocheck.to_check} files left")
            proc.wait()
            returncode = proc.returncode
            if returncode == 0 or returncode == 23 or returncode == 24:
                finished = True
            else:
                logger.warning(f"rsync exited with returncode {returncode}")
                soscore.sysstate.result_file_append(
                    RR_RESULT,
                    SystemMode.REMOTE_RECOVERY,
                    f"rsync exited with returncode {returncode}",
                )

        else:
            logger.warning(
                f"Remote Host Stats: '{host_status.message(host_status)}' uuid: {uuid}"
            )
            soscore.sysstate.result_file_append(
                RR_RESULT,
                SystemMode.REMOTE_RECOVERY,
                f"{host_status.message(host_status)}",
            )
        time.sleep(10)

    logger.info("Success. Switching back to default mode")
    result: SysStateResult = mystate.changemode(SystemMode.RR_FINAL)
    if result.mode != SystemMode.RR_FINAL:
        logger.error("Could not switch to default mode after successful restore")
        sys.exit(0)


if __name__ == "__main__":
    main()
