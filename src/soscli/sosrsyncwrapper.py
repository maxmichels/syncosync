#!/usr/bin/env python3
"""
wrapping the rsync process for better parsing the progress
"""
import argparse
import copy

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import os
import re
import subprocess
import sys

import soscore.soshelpers_file
from sosmodel import sos_enums, sosconstants
from sosmodel.sostocheck import SosToCheck
from sosutils.logging import LoggerCategory, get_logger, init_logging
from sosutils.runtime_args import parser_log_args


def get_inodes():
    statvfs = os.statvfs(sosconstants.MOUNT[sos_enums.Partition.LOCAL])
    return statvfs.f_files - statvfs.f_ffree


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="sosrsyncwrapper",
        epilog=sosconstants.EPILOG,
    )
    parser_log_args(parser)
    args, rsync_args = parser.parse_known_args()
    init_logging(args)

    logger = get_logger(LoggerCategory.soscli)
    sostocheck = SosToCheck()
    new_sostocheck = SosToCheck()

    start_inodes = get_inodes()
    start_chk = -1
    cmd = "/usr/bin/rsync --info=progress --no-i-r "
    #    cmd = 'rsync --progress --no-i-r '
    for i in range(0, len(rsync_args)):
        cmd = cmd + " " + (rsync_args[i])
    cmd = cmd + " | gawk '1;{fflush()}' RS='\\r|\\n'"

    logger.debug(f"rsync command will be: {cmd}")

    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)

    while proc.poll() is None:
        new_sostocheck = copy.copy(sostocheck)
        line = proc.stdout.readline()
        line = line.decode("ascii")
        line = str(line.rstrip())
        # if os.path.isfile(os.path.join(sosconstants.MOUNT[sos_enums.Partition.LOCAL], line)):
        #     new_sostocheck.akt_file = ""  # was: line
        m = re.findall(r"to-chk=(\d+)/\d+", line)
        if m:
            to_chk = int(m[0])
            if start_chk == -1:
                start_chk = to_chk
            akt_inodes = get_inodes()
            new_sostocheck.to_check = to_chk + akt_inodes - start_inodes
            new_sostocheck.percent = 100
        else:
            m = re.findall(r"\s+.*\s+(\d+)%\s+.*", line)
            if m:
                new_sostocheck.percent = int(m[0])

        if new_sostocheck != sostocheck:
            soscore.soshelpers_file.write_to_file(
                sosconstants.SOSTOCHECK_CACHE, new_sostocheck.to_json()
            )
            sostocheck = copy.copy(new_sostocheck)
    rc = proc.returncode
    logger.debug(f"rsync finished rc={rc}")
    new_sostocheck.to_check = 0
    new_sostocheck.percent = 100
    soscore.soshelpers_file.write_to_file(
        sosconstants.SOSTOCHECK_CACHE, new_sostocheck.to_json()
    )
    return rc


if __name__ == "__main__":
    rc = main()
    sys.exit(rc)
