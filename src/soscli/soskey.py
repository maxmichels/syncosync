#!/usr/bin/env python3
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
This is script for sos key handling - not the account keys!
"""

import argparse

from soscore import soshelpers, soskey
from sosmodel import sosconstants
from sosutils.logging import init_logging
from sosutils.runtime_args import parser_log_args


def main():
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="Management of syncosync key",
        epilog=sosconstants.EPILOG,
    )
    parser.add_argument(
        "-n",
        "--new",
        help="generate a new key. Attention: will overwrite existing",
        action="store_true",
    )
    parser.add_argument(
        "-y", "--yes", help="confirmation for generating", action="store_true"
    )
    parser.add_argument(
        "-g", "--get", help="show actual configuration as json", action="store_true"
    )
    parser.add_argument(
        "-p", "--pretty", help="formats json output", action="store_true", default=False
    )
    parser_log_args(parser)
    args = parser.parse_args()
    init_logging(args)

    mysoskey = soskey.SosKey()
    if args.get:
        mysoskey_key = mysoskey.get()
        print(soshelpers.pretty_print(mysoskey_key.to_json(), args.pretty))
        exit()
    if args.new:
        if args.yes:
            print("Generating new sos key")
            result = mysoskey.generate()
            if result:
                print("Success")
        else:
            print("add -y to generate new sos key")
        exit()


if __name__ == "__main__":
    main()
