#!/usr/bin/env python3

"""
syncosync - secure peer to peer backup synchronization
Copyright (C) 2020  syncosync.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import argparse

# the following imports are all necessary for cli
import textwrap

from soscore import soshelpers, soshelpers_shape, trafficshape
from sosmodel import sosconstants
from sosmodel.trafficshape import TrafficShapeSwitchModel
from sosutils.logging import init_logging
from sosutils.runtime_args import parser_log_args


def main():
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description=textwrap.dedent(
            """\
                                    Traffic shaping scheduler
                                    =========================
                                    Allows to set maximum values for outgoing and incoming traffic
                                    to and from the remote host depending on adjustable day and night
                                    time marks.
                                    This keeps your internet connection still usable."""
        ),
        epilog=sosconstants.EPILOG,
    )
    parser.add_argument("-g", "--get", help="get json of settings", action="store_true")
    parser.add_argument(
        "-s",
        "--set",
        help='set shaping from json, e.g.: \'{"configname": '
        '"trafficshape", "day": 360, "in_day": 40, "in_night":'
        ' 100, "maxin": 100, "maxout": 50, "night": 0, "out_day": '
        '20, "out_night": 50, "version": "1"}\', note that the port'
        " for outgoing traffic is set via remotehost.py",
    )
    parser.add_argument(
        "--start",
        help="start shaping. Attention: this is also started from"
        "syncosync service, so you should never need this!",
        action="store_true",
    )
    parser.add_argument(
        "--stop",
        help="stop shaping. Attention: this is also stopped from"
        "syncosync service, so you should never need this!",
        action="store_true",
    )
    parser.add_argument(
        "-t",
        "--toggle",
        help="Toggle from day to night and vice versa, this is valid until the next start mark is reached",
        action="store_true",
    )
    parser.add_argument(
        "-f",
        "--force",
        help="force day or night switch via json: '{\"mode\": 1}' for day, 2 for night, 0 for no sync at all",
    )
    parser.add_argument(
        "-c",
        "--check",
        help="Check, if shaping is active and output status and values",
        action="store_true",
    )
    parser.add_argument(
        "-p", "--pretty", help="formats json output", action="store_true", default=False
    )
    parser_log_args(parser)
    args = parser.parse_args()
    init_logging(args)

    ts_manager = trafficshape.TrafficShape()

    if args.get:
        print(soshelpers.pretty_print(ts_manager.get().to_json(), args.pretty))

    if args.set:
        ts_manager.set_from_json(args.set)

    if args.start:
        ts_manager.start()

    if args.stop:
        ts_manager.stop()

    if args.toggle:
        ts_manager.toggle()

    if args.force:
        mymode = TrafficShapeSwitchModel.from_json(args.force)
        ts_manager.force_mode(mymode)

    if args.check:
        if soshelpers_shape.is_shape_on():
            print("Shaping is on: ")
            print(trafficshape.get_cached_json())
        else:
            print("Shaping is off")


if __name__ == "__main__":
    main()
