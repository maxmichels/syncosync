#!/usr/bin/env python3
"""
This script shows the usage of the syncstat classes. You could also start it in daemon mode e.g.
from a service script, but normally this functionality is bound into the master daemon of the
syncosync system
"""
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import time

from soscore import soshelpers, syncstat
from sosmodel import sosconstants
from sosutils.logging import init_logging
from sosutils.runtime_args import parser_log_args


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="syncstat - deamon and reader",
        epilog=sosconstants.EPILOG,
    )
    parser.add_argument(
        "-d",
        "--daemon",
        help="starts as daemon (writes cache file)",
        action="store_true",
    )
    parser.add_argument(
        "-c",
        "--client",
        help="starts as client (reads cache file)",
        action="store_true",
    )
    parser.add_argument(
        "-o",
        "--output",
        help="verbose output (for daemon and client)",
        action="store_true",
    )
    parser.add_argument("-l", "--loop", help="loop time in sec (0=once)", default=0)
    parser.add_argument(
        "-p", "--pretty", help="formats json output", action="store_true", default=False
    )
    parser_log_args(parser)
    args = parser.parse_args()
    init_logging(args)

    if args.daemon:
        mysyncstat = syncstat.SyncStatUpdater()
        while True:
            mysyncstat.update_data()
            if args.output:
                mysyncstatoutput = mysyncstat.get()
                print(soshelpers.pretty_print(mysyncstatoutput.to_json(), args.pretty))
            if args.loop == 0:
                exit(0)
            time.sleep(float(args.loop))

    if args.client:
        mysyncstat = syncstat.SyncStatReader()
        while True:
            mysyncstat.update_data()
            if args.output:
                mysyncstatoutput = mysyncstat.get()
                print(soshelpers.pretty_print(mysyncstatoutput.to_json(), args.pretty))
            if args.loop == 0:
                break
            time.sleep(float(args.loop))


if __name__ == "__main__":
    main()
