#!/usr/bin/env python3
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
This is the script for sos key exchange
"""

import argparse
import signal

from soscore import remotehost_systemsetupdata, soserrors, soshelpers
from soscore.soskeyx import SosKeyExchange
from sosmodel import sosconstants
from sosmodel.sos_enums import SsdAction
from sosmodel.systemsetupdata import SystemSetupDataModel
from sosutils.logging import LoggerCategory, get_logger, init_logging
from sosutils.runtime_args import parser_log_args


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="exchange of sync setup data (ssd)",
        epilog=sosconstants.EPILOG,
    )
    parser.add_argument(
        "-a", "--accept", help="accept a remote key", action="store_true"
    )
    parser.add_argument(
        "-d", "--delete", help="delete the remote key", action="store_true"
    )
    parser.add_argument("-g", "--get", help="get remote key info", action="store_true")
    parser.add_argument("-r", "--receive", help="receive ssd", action="store_true")
    parser.add_argument(
        "-s", "--send_key", help="send ssd with send key action", action="store_true"
    )
    parser.add_argument(
        "--init_partitions",
        help="send ssd with init partitions action",
        action="store_true",
    )
    parser.add_argument(
        "--resize_partitions",
        help="send ssd with resize partitions action",
        action="store_true",
    )
    parser.add_argument(
        "--request_data", help="send ssd with request data action", action="store_true"
    )
    parser.add_argument(
        "-l", "--latest_ssd", help="show last received remote ssd", action="store_true"
    )
    parser.add_argument(
        "-p", "--pretty", help="formats json output", action="store_true", default=False
    )
    parser_log_args(parser)
    args = parser.parse_args()
    init_logging(args)
    logger = get_logger(LoggerCategory.soscli)

    def sighandler(self, signum):
        # SosKeyExchange.receive_cancel_signal.set()
        SosKeyExchange.receive_cancel_signal = True
        logger.debug(f"received signal {signum}")
        raise soserrors.SosError("key exchange cancelled")

    signal.signal(signal.SIGUSR1, sighandler)

    if args.get:
        remote_key = SosKeyExchange.get_remote_key()
        print(soshelpers.pretty_print(remote_key.to_json(), args.pretty))

    if args.delete:
        SosKeyExchange.remove_remote_key()

    if args.receive:
        remote_ssd = SosKeyExchange.receive_ssd()
        print(soshelpers.pretty_print(remote_ssd.to_json(), args.pretty))
        print("Action: ", SsdAction.to_readable(remote_ssd.action))
        if remote_ssd.action == SsdAction.LEADER_SEND_KEY:
            print("The received key can be accepted by issuing: soskeyx.py -a")
        if remote_ssd.action == SsdAction.LEADER_REQUEST_DATA:
            print(
                "Leader requested your actual system setup data, no further action required"
            )
        if remote_ssd.action == SsdAction.LEADER_INIT_PARTITIONS:
            print(
                'The received partitioning can be installed by issuing: drivemanager.py -p \'{"local":%d, '
                '"remote":%d }\' -y'
                % (remote_ssd.partition_data.remote, remote_ssd.partition_data.local)
            )
        if remote_ssd.action == SsdAction.LEADER_RESIZE_PARTITIONS:
            print(
                'The received partitioning can be resized by issuing: drivemanager.py -r \'{"local":%d, '
                '"remote":%d }\' -y'
                % (remote_ssd.partition_data.remote, remote_ssd.partition_data.local)
            )

    if args.send_key:
        remote_ssd = SosKeyExchange.send_ssd(action=SsdAction.LEADER_SEND_KEY)
        print(soshelpers.pretty_print(remote_ssd.to_json(), args.pretty))
        print("Action: ", SsdAction.to_readable(remote_ssd.action))

    if args.init_partitions:
        remote_ssd = SosKeyExchange.send_ssd(action=SsdAction.LEADER_INIT_PARTITIONS)
        print(soshelpers.pretty_print(remote_ssd.to_json(), args.pretty))
        print("Action: ", SsdAction.to_readable(remote_ssd.action))

    if args.request_data:
        remote_ssd = SosKeyExchange.send_ssd(action=SsdAction.LEADER_REQUEST_DATA)
        print(soshelpers.pretty_print(remote_ssd.to_json(), args.pretty))
        print("Action: ", SsdAction.to_readable(remote_ssd.action))

    if args.resize_partitions:
        remote_ssd = SosKeyExchange.send_ssd(action=SsdAction.LEADER_RESIZE_PARTITIONS)
        print(soshelpers.pretty_print(remote_ssd.to_json(), args.pretty))
        print("Action: ", SsdAction.to_readable(remote_ssd.action))

    if args.accept:
        SosKeyExchange.accept_key()

    if args.latest_ssd:
        remote_ssd: SystemSetupDataModel = (
            remotehost_systemsetupdata.RemoteHostSystemSetupData().get()
        )
        print(soshelpers.pretty_print(remote_ssd.to_json(), args.pretty))
        print("Action: ", SsdAction.to_readable(remote_ssd.action))


if __name__ == "__main__":
    main()
