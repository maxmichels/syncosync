#!/usr/bin/env python3
"""
This is started as a daemon which updates sosusers last access and used_spaces values
based on inotify events
The structure is stored as a json file in /tmp/syncosync/sosuserlist-access.json
"""

# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# This is started as a daemon which updates sosusers last access and used_spaces values
# based on inotify events
# The structure is stored as a json file in /tmp/syncosync/sosuserlist-access.json


import argparse
import asyncio
import glob
import json

# following imports are all necessary for cli
import os
import re
import time
from typing import Dict

import soscore.soshelpers_file
from asyncinotify import Inotify, Mask
from soscore import soshelpers
from sosmodel import sos_enums, sosconstants
from sosutils.logging import LoggerCategory, get_logger, init_logging
from sosutils.runtime_args import parser_log_args

last_access = {}
space_used: Dict[str, int] = {}

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description="reads - and caches - account size and access date",
    epilog=sosconstants.EPILOG,
)
parser_log_args(parser)
args = parser.parse_args()
init_logging(args)
logger = get_logger(LoggerCategory.soscli)


def add_watch(path):
    """Adds a watch to a path

    Args:
        path ([type]): path to watch
    """
    if os.path.isdir(path):
        logger.debug(f"Adding watch for '{str(path)}'")
        inotify.add_watch(
            path,
            Mask.DELETE | Mask.ONLYDIR | Mask.CLOSE_WRITE | Mask.CREATE | Mask.MOVED_TO,
        )
    else:
        logger.warning(f"Dir '{path}' is not existing, so won't be added")


async def space_callback(account):
    await asyncio.sleep(10)
    logger.debug(f"Delayed space update for '{account}'")
    new_size = soshelpers.get_account_used_space(account)
    if account not in space_used or space_used[account] != new_size:
        space_used[account] = new_size
        logger.debug(f"Space used for '{account}' has changed to {new_size}")
        soscore.soshelpers_file.write_to_file(
            sosconstants.SOSACCOUNTSPACE_CACHE, json.dumps(space_used)
        )


async def account_watcher_callback(account):
    await asyncio.sleep(5)
    logger.debug(f"Delayed watch adder for '{account}'")
    account_dir = os.path.join(
        sosconstants.MOUNT[sos_enums.Partition.LOCAL],
        sosconstants.HDDSUBDIR,
        account,
        "data",
    )
    add_watch(account_dir)


# if started first time, generate a json cache for all users, this takes a few seconds, but not too long
# first check, if the directory exists
if not os.path.isdir(sosconstants.RUNDIR):
    os.mkdir(sosconstants.RUNDIR)

inotify = Inotify()
add_watch(
    os.path.join(sosconstants.MOUNT[sos_enums.Partition.LOCAL], sosconstants.HDDSUBDIR)
)


def add_tree(dirpath):
    if os.path.isdir(dirpath):
        # TODO: check if we are in the base of an account, so we do not need to add other folders than data
        for x in os.walk(dirpath):
            # print("Checking x:",x)
            m = re.search(
                sosconstants.MOUNT[sos_enums.Partition.LOCAL]
                + "/"
                + sosconstants.HDDSUBDIR
                + "/([^/]*)/data.*",
                str(x),
            )
            if m:
                add_watch(x[0])


for itempath in glob.glob(
    os.path.join(
        sosconstants.MOUNT[sosconstants.Partition.LOCAL], sosconstants.HDDSUBDIR, "*"
    )
):
    name = os.path.basename(itempath)
    if os.path.isdir(
        os.path.join(
            sosconstants.MOUNT[sosconstants.Partition.LOCAL],
            sosconstants.HDDSUBDIR,
            name,
        )
    ):
        last_access[name] = soshelpers.return_youngest_mod(
            os.path.join(
                sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                sosconstants.HDDSUBDIR,
                name,
                "data",
            )
        )
        add_tree(
            os.path.join(
                sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                sosconstants.HDDSUBDIR,
                name,
                "data",
            )
        )
        space_used[name] = soshelpers.get_account_used_space(name)

soscore.soshelpers_file.write_to_file(
    sosconstants.SOSACCOUNTACCESS_CACHE, json.dumps(last_access)
)
soscore.soshelpers_file.write_to_file(
    sosconstants.SOSACCOUNTSPACE_CACHE, json.dumps(space_used)
)


async def main():
    # Context manager to close the inotify handle after use
    # Adding the watch can also be done outside of the context manager.
    # __enter__ doesn't actually do anything except return self.
    # This returns an asyncinotify.inotify.Watch instance
    # Iterate events forever, yielding them one at a time
    async for event in inotify:
        # Events have a helpful __repr__.  They also have a reference to
        # their Watch instance.
        # print(event)
        if event.mask & Mask.ISDIR:
            # ok, this is a dir based event
            # this_basename = os.path.basename(event.path)
            if event.mask & Mask.CREATE:
                # print("path: ", event.path, " name: ", event.name, event.watch.path)
                if str(event.watch.path) == str(
                    os.path.join(
                        sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                        sosconstants.HDDSUBDIR,
                    )
                ):
                    asyncio.create_task(account_watcher_callback(event.name))
                else:
                    add_tree(str(event.path))
            if event.mask & Mask.DELETE:
                if (
                    str(event.watch.path)
                    == str(
                        os.path.join(
                            sosconstants.MOUNT[sos_enums.Partition.LOCAL],
                            sosconstants.HDDSUBDIR,
                        )
                    )
                    and event.name != ""
                ):
                    logger.debug(f"Account '{event.name}' deleted from watching")
        else:
            # now we have to check for file handling
            # this could happen only in the data area and below
            m = re.search(
                sosconstants.MOUNT[sos_enums.Partition.LOCAL]
                + "/"
                + sosconstants.HDDSUBDIR
                + "/([^/]*)/data.*",
                str(event.path),
            )
            if m:
                last_access[m.group(1)] = int(time.time())
                soscore.soshelpers_file.write_to_file(
                    sosconstants.SOSACCOUNTACCESS_CACHE, json.dumps(last_access)
                )
                soscore.soshelpers_file.write_to_file(
                    sosconstants.SOSACCOUNTACCESS_PERSISTENT, json.dumps(last_access)
                )
                if (
                    event.mask & Mask.CLOSE_WRITE
                    or event.mask & Mask.MOVED_TO
                    or event.mask & Mask.DELETE
                ):
                    asyncio.create_task(space_callback(m.group(1)))


loop = asyncio.get_event_loop()
try:
    loop.run_until_complete(main())
except KeyboardInterrupt:
    print("shutting down")
finally:
    loop.run_until_complete(loop.shutdown_asyncgens())
    loop.close()
