#!/usr/bin/env python3

"""
syncosync - secure peer to peer backup synchronization
Copyright (C) 2021  syncosync.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import argparse

# following imports are all necessary for cli
from soscore import sysstate
from soscore.factory_reset import factory_reset, factory_reset_disks
from sosmodel import sosconstants
from sosmodel.sos_enums import SystemMode
from sosutils.logging import init_logging
from sosutils.runtime_args import parser_log_args


def main():
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="factory reset of syncosync system",
        epilog=sosconstants.EPILOG,
    )
    parser.add_argument("-r", "--reset", help="factory reset", action="store_true")
    parser.add_argument(
        "-n", "--network", help="resets also network settings", action="store_true"
    )
    parser.add_argument("-e", "--erase", help="erase disks", action="store_true")
    parser.add_argument(
        "-y", "--yes", help="confirm factory reset", action="store_true"
    )
    parser_log_args(parser)
    args = parser.parse_args()
    init_logging(args)

    if args.reset:
        if not args.yes:
            print("Confirm with -y for factory reset")
        else:
            mysysstate = sysstate.SysState()
            mysysstate.changemode(SystemMode.SHUTDOWN)
            factory_reset(args.network)
            print("Now reboot for all reset actions to take effect")

    if args.network and not args.reset:
        print("Network setting reset is only possible with factory reset (-r)")

    if args.erase:
        if not args.yes:
            print("Confirm with -y for erasing disks")
        else:
            mysysstate = sysstate.SysState()
            mysysstate.changemode(SystemMode.SHUTDOWN)
            factory_reset_disks()
            print("Now reboot for all reset actions to take effect")


if __name__ == "__main__":
    main()
