#!/usr/bin/env python3
"""
This is the managing program for managing syncosync configs.
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import argparse

from soscore import sosconfig, soshelpers, soshelpers_hardware, sosversion
from sosmodel import sos_enums, sosconstants
from sosmodel.sosconfig import SosConfigModel
from sosmodel.sosversion import SyncosyncVersionModel
from sosmodel.systemdescription import SystemDescriptionModel
from sosutils.logging import init_logging
from sosutils.runtime_args import parser_log_args


def main():
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="Manages syncosync general configuration",
        epilog=sosconstants.EPILOG,
    )
    parser.add_argument(
        "-g", "--get", help="get configuration, returns json", action="store_true"
    )
    parser.add_argument(
        "-d",
        "--description",
        help="get system description, returns json",
        action="store_true",
    )
    parser.add_argument(
        "--description_string",
        help="get description type (e.g. rpi4 or bpi or qemu" ", returns json",
        action="store_true",
    )
    parser.add_argument(
        "-t",
        "--type_init",
        help="initialise systype by guessing system",
        action="store_true",
    )
    parser.add_argument(
        "-s",
        "--set",
        help="set new settings from json "
        'e.g. \'{"systype": "generic", "upnp": false, "ui_language":'
        ' {"language": 0}, "admin_mail_address": "", "admin_info_period":'
        " 7}",
    )
    parser.add_argument(
        "-p", "--pretty", help="formats json output", action="store_true", default=False
    )
    parser.add_argument(
        "-V",
        "--version",
        help="returns syncosync version",
        action="store_true",
        default=False,
    )
    parser_log_args(parser)
    args = parser.parse_args()
    init_logging(args)

    if args.version:
        mysosversionhandler = sosversion.SyncosyncVersion()
        mysosversion: SyncosyncVersionModel = mysosversionhandler.get()
        print(soshelpers.pretty_print(mysosversion.to_json(), args.pretty))

    myconfig = sosconfig.SosConfig()

    if args.type_init:
        new_systype = soshelpers_hardware.get_systype()
        print(f'Detected system is "{sos_enums.SysType.typename(new_systype)}"')
        mysosconfig: SosConfigModel = myconfig.get()
        mysosconfig.systype = new_systype
        myconfig.set()

    if args.set:
        # we have to get it first to change it. If it does not exist, no problem, it will be default
        mysosconfig: SosConfigModel = myconfig.get()
        mysosconfig.update_from_json(args.set)
        myconfig.set()

    if args.get:
        myconfigoutput: SosConfigModel = myconfig.get()
        print(soshelpers.pretty_print(myconfigoutput.to_json(), args.pretty))

    if args.description_string:
        myconfigoutput: SosConfigModel = myconfig.get()
        print(sos_enums.SysType.typename(myconfigoutput.systype))

    if args.description:
        myconfigoutput: SystemDescriptionModel = myconfig.get_system_description()
        print(soshelpers.pretty_print(myconfigoutput.to_json(), args.pretty))


if __name__ == "__main__":
    main()
