import unittest

from sosmodel import trafficshape


class MyTestCase(unittest.TestCase):
    def test_correct_value1(self):
        mymodel = trafficshape.TrafficShapeModel.from_json(
            '{"out_day": 50, "in_day": 10, "maxout": 100, "maxin": 200}'
        )
        self.assertEqual(mymodel.out_day, 50, "test good")
        self.assertEqual(mymodel.in_day, 10, "test good")
        self.assertEqual(mymodel.maxout, 100, "test good")
        self.assertEqual(mymodel.maxin, 200, "test good")

    def test_set(self):
        model = trafficshape.TrafficShapeModel()
        model.set(
            out_day=1,
            in_day=1,
            out_night=1,
            in_night=1,
            day=1,
            night=1,
            maxout=1,
            maxin=1,
        )
        # Try all possible methods being called...
        with self.assertRaises(ValueError):
            model.set(
                out_day=0,
                in_day=1,
                out_night=1,
                in_night=1,
                day=1,
                night=1,
                maxout=1,
                maxin=1,
            )
        with self.assertRaises(ValueError):
            model.set(
                out_day=1,
                in_day=0,
                out_night=1,
                in_night=1,
                day=1,
                night=1,
                maxout=1,
                maxin=1,
            )
        with self.assertRaises(ValueError):
            model.set(
                out_day=1,
                in_day=1,
                out_night=0,
                in_night=1,
                day=1,
                night=1,
                maxout=1,
                maxin=1,
            )
        with self.assertRaises(ValueError):
            model.set(
                out_day=1,
                in_day=1,
                out_night=1,
                in_night=0,
                day=1,
                night=1,
                maxout=1,
                maxin=1,
            )
        with self.assertRaises(ValueError):
            model.set(
                out_day=1,
                in_day=1,
                out_night=1,
                in_night=1,
                day=-1,
                night=1,
                maxout=1,
                maxin=1,
            )
        with self.assertRaises(ValueError):
            model.set(
                out_day=1,
                in_day=1,
                out_night=1,
                in_night=1,
                day=1,
                night=-1,
                maxout=1,
                maxin=1,
            )
        with self.assertRaises(ValueError):
            model.set(
                out_day=1,
                in_day=1,
                out_night=1,
                in_night=1,
                day=1,
                night=1,
                maxout=0,
                maxin=1,
            )
        with self.assertRaises(ValueError):
            model.set(
                out_day=1,
                in_day=1,
                out_night=1,
                in_night=1,
                day=1,
                night=1,
                maxout=1,
                maxin=0,
            )


class ExpectedFailureTestCase(unittest.TestCase):
    @unittest.expectedFailure
    def test_wrong_value1(self):
        mymodel = trafficshape.TrafficShapeModel.from_json('{"out_day": -10}')
        self.assertEqual(mymodel.out_day, -10, "broken")

    @unittest.expectedFailure
    def test_wrong_value2(self):
        mymodel = trafficshape.TrafficShapeModel()
        mymodel.update_from_json('{"out_day": 10001}')
        self.assertEqual(mymodel.out_day, 10001, "broken")


if __name__ == "__main__":
    unittest.main()
