from typing import List

from sosmodel.serializerbase import SerializerBase


class TimezoneListModel(SerializerBase):
    """
    Represents t
    """

    available_timezones: List[str]

    def __init__(self):
        self.available_timezones = []
