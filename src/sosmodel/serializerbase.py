import collections
import json
import logging
from json import JSONEncoder
from typing import Collection, Dict, List, Type, TypeVar, Union, get_type_hints

logger = logging.getLogger(__name__)


class SerializerBase(object):
    """
    Represents the system's hostname
    """

    def _update_from_dict(self, values: Dict):
        """
        Initializes values of instance with values given (dict).
        If your SerializerBase derived class uses instances of other derived classes you will need to overwrite this
        method and initialize the instances!
        """
        if values is None:
            return
        for key in self.__dir__():
            if key.startswith("__") and key.endswith("__"):
                continue
            if key in values:
                attr_or_method = getattr(self, key, None)
                new_values = values.get(key)
                # TODO: If method is a setter of a nested serializerbase...
                if attr_or_method is not None and callable(attr_or_method):
                    # we have a method, if there is a variable with _ prefix, call it
                    if "_" + key in self.__dict__:
                        attr_or_method(new_values)
                    else:
                        continue
                elif (
                    attr_or_method is not None
                    and isinstance(new_values, dict)
                    and isinstance(attr_or_method, SerializerBase)
                ):
                    attr_or_method._update_from_dict(new_values)
                elif (
                    attr_or_method is not None
                    and isinstance(new_values, list)
                    and isinstance(attr_or_method, list)
                    and all(isinstance(item, dict) for item in new_values)
                    and all(isinstance(item, SerializerBase) for item in attr_or_method)
                ):
                    # TODO: This may still be wonky
                    # all items in class variable list are Serializer base and the items in the new values list are
                    # dicts. We can assume intended updated
                    type_of_list = None
                    for val_of_list in attr_or_method:
                        if (
                            type_of_list is not None
                            and type_of_list != type(val_of_list)
                            or not isinstance(val_of_list, SerializerBase)
                        ):
                            continue
                        else:
                            type_of_list = type(val_of_list)
                    if type_of_list is None:
                        # First try to read a type hint?
                        type_hints = get_type_hints(self.__class__)
                        if (
                            type_hints
                            and key in type_hints
                            and type_hints[key].__args__ is not None
                            and len(type_hints[key].__args__) == 1
                        ):
                            type_of_list = type_hints[key].__args__[0]
                        else:
                            logger.warning(
                                "Failed to determine type for {}", new_values
                            )
                            continue
                    attr_or_method.clear()
                    for new_val in new_values:
                        if isinstance(new_val, str):
                            attr_or_method.append(type_of_list.from_json(new_val))
                        elif isinstance(new_val, dict):
                            # We have already deserialized...
                            attr_or_method.append(type_of_list.from_dict(new_val))
                        else:
                            logger.warning("Unable to deserialize data: {}", new_val)
                else:
                    setattr(self, key, values.get(key))

    @classmethod
    def from_json(cls, json_string):
        obj = cls.__new__(cls)
        cls.__init__(obj)
        try:
            loaded = json.loads(json_string)
            obj._update_from_dict(loaded)
        except Exception as e:
            logger.error(f'Could not decode json ({str(e)}): "{json_string}"')
            logger.exception(e)
        return obj

    @classmethod
    def from_dict(cls, dict_obj: dict):
        obj = cls.__new__(cls)
        cls.__init__(obj)
        try:
            obj._update_from_dict(dict_obj)
        except Exception as e:
            logger.error(f'Could not process dict to instance ({str(e)}): "{dict_obj}"')
            logger.exception(e)
        return obj

    def update_from_json(self, json_string):
        loaded = json.loads(json_string)
        self._update_from_dict(loaded)

    @classmethod
    def from_json_file(cls, filepath):
        obj = cls.__new__(cls)
        json_string = ""
        cls.__init__(obj)
        try:
            with open(filepath) as json_file:
                json_string = json_file.read()
        except Exception as e:
            logger.error(f'Could load from json file ({str(e)}): "{filepath}"')
        try:
            loaded = json.loads(json_string)
            obj._update_from_dict(loaded)
        except Exception as e:
            logger.error(f'Could not decode json ({str(e)}): "{json_string}"')
        return obj

    def to_json(self) -> str:
        return json.dumps(self, cls=SerializerBaseJsonEncoder, sort_keys=True)


class SerializerBaseJsonEncoder(JSONEncoder):
    T = TypeVar("T", bound=SerializerBase)

    def __encode_serializer_base_list(self, o: Collection[T]) -> str:
        buf = "["
        separator = self.item_separator
        first = True
        for value in o:
            if first:
                first = False
            else:
                buf += separator
            if not value:
                buf += "null"
            else:
                buf += value.to_json()
        buf += "]"
        return buf

    def encode(self, o):
        """Return a JSON string representation of a Python data structure.

        >>> from json.encoder import JSONEncoder
        >>> JSONEncoder().encode({"foo": ["bar", "baz"]})
        '{"foo": ["bar", "baz"]}'

        """
        # TODO: Enums break if a wrong value has been set before
        if isinstance(o, SerializerBase):
            dump_dict: Dict = {}
            for key in dir(o):
                if key.startswith("__"):
                    continue
                attr_or_method = getattr(o, key)
                if callable(attr_or_method) or key.startswith("__"):
                    # method or private var
                    continue
                # we have a variable/attribute
                # we need to first check if it starts with _ or __, then check if there is a callable without
                key_to_use: str = key
                if key.startswith("_"):
                    potential_setter = getattr(o, key.replace("_", "", 1))
                    if callable(potential_setter):
                        key_to_use = key.replace("_", "", 1)
                    else:
                        # unable to locate setter, we don't want to place protected/private into json tho
                        continue

                if isinstance(attr_or_method, SerializerBase):
                    attr_as_json = attr_or_method.to_json()
                    dump_dict[key_to_use] = json.loads(attr_as_json)
                elif isinstance(attr_or_method, list) and all(
                    item is None or isinstance(item, SerializerBase)
                    for item in attr_or_method
                ):
                    # if it is a list of serializerbase, we need to handle it accordingly...
                    dump_dict[key_to_use] = json.loads(
                        self.__encode_serializer_base_list(attr_or_method)
                    )
                elif isinstance(attr_or_method, dict):
                    # Value is a dict, we need to parse the values recursively accordingly...
                    sub_dict: Dict = {}
                    for key in attr_or_method.keys():
                        # TODO: First use vars()?
                        stringified = json.dumps(
                            attr_or_method[key],
                            cls=SerializerBaseJsonEncoder,
                            sort_keys=True,
                        )
                        sub_dict[key] = json.loads(stringified)
                    dump_dict[key_to_use] = sub_dict
                else:
                    # TODO: If a dict contains serializerbase, we would need to handle it...
                    # stringified = json.dumps(attr_or_method, cls=SerializerBaseJsonEncoder, sort_keys=True)
                    # dump_dict[key_to_use] = json.loads(stringified)
                    dump_dict[key_to_use] = attr_or_method
            return JSONEncoder.encode(self, dump_dict)
        elif isinstance(o, collections.Collection) and all(
            isinstance(item, SerializerBase) for item in o
        ):
            # list of SerializerBase items TODO
            return self.__encode_serializer_base_list(o)
        else:
            return JSONEncoder.encode(self, o)

    @staticmethod
    def to_json(o: Union[T, List[T]]) -> str:
        return json.dumps(o, cls=SerializerBaseJsonEncoder, sort_keys=True)


class SerializerBaseJsonDecoder:
    T = TypeVar("T", bound=SerializerBase)

    @staticmethod
    def decode(cls: Type[T], serialized_string: Union[str, bytes]) -> Union[T, List[T]]:
        """
        Decodes a given json formatted string (serialized string) using the class passed (cls)
        """
        json_decoded = json.loads(serialized_string)
        if isinstance(json_decoded, list):
            # the data we received was a list, we have to decode multiple items
            result = []
            for item in json_decoded:
                # item is a dict we need to transform to an instance...
                # since settattr does not play nicely with constructors, we transform back to str
                # performance hell
                result.append(cls.from_json(json.dumps(item)))
            return result
        elif isinstance(json_decoded, dict):
            # assume the data is a plain instance
            return cls.from_json(serialized_string)
        else:
            # we don't know how to deal with the data, throw a TypeError
            raise TypeError(
                f"Unable to decode string from json to class instances: {str(serialized_string)}"
            )
