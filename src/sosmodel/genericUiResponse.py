# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from typing import Optional

from sosmodel.localizedMessages import LocalizedMessage
from sosmodel.serializerbase import SerializerBase
from sosmodel.sos_enums import GenericUiResponseStatus


class GenericUiResponse(SerializerBase):
    """
    Represents a generic response sent to the UI for simple ACK/OK or error codes/responses
    """

    _status: GenericUiResponseStatus
    _localized_reason: Optional[LocalizedMessage]
    _additional_information: Optional[str]

    def __init__(self):
        self._status: GenericUiResponseStatus = GenericUiResponseStatus.OK
        self._localized_reason: Optional[LocalizedMessage] = None
        self._additional_information: Optional[str] = None

    @property
    def status(self) -> GenericUiResponseStatus:
        return self._status

    @status.setter
    def status(self, value: GenericUiResponseStatus) -> None:
        self._status = value

    @property
    def localized_reason(self) -> Optional[LocalizedMessage]:
        return self._localized_reason

    @localized_reason.setter
    def localized_reason(self, value: Optional[LocalizedMessage]) -> None:
        self._localized_reason = value

    @property
    def additional_information(self) -> Optional[str]:
        return self._additional_information

    @additional_information.setter
    def additional_information(self, value: Optional[str]) -> None:
        self._additional_information = value
