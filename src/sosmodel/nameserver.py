"""
Model for nameserver configuration
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from sosmodel.configbasemodel import ConfigBaseModel


class NameServerModel(ConfigBaseModel):
    """
    Only three nameservers since Debian only handles three by default.
    See man resolv.conf
    """

    def __init__(self):
        super().__init__()
        self.configname = "nameserver"
        self.first_nameserver = ""
        self.second_nameserver = ""
        self.third_nameserver = ""

    def set(self, first_nameserver="", second_nameserver="", third_nameserver=""):
        self.first_nameserver = first_nameserver
        self.second_nameserver = second_nameserver
        self.third_nameserver = third_nameserver

    # TODO: value checks
