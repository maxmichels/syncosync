"""
Model for sostocheck info
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from sosmodel.serializerbase import SerializerBase


class SosToCheck(SerializerBase):
    """
    info from rsync, how many files are to check and progress in %
    """

    start_elements: int
    to_check: int
    percent: int

    def __init__(self, to_check: int = 0, percent: int = 100, start_elements: int = 0):
        self.start_elements = start_elements
        self.to_check = to_check
        self.percent = percent

    def __eq__(self, other):
        return (
            self.to_check == other.to_check
            and self.percent == other.percent
            and self.start_elements == other.start_elements
        )
