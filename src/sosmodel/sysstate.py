"""
Models for sysstate
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from typing import List, Optional

from sosmodel.serializerbase import SerializerBase
from sosmodel.sos_enums import SwitchAction, SystemMode, SystemStatus


class SysStateResult(SerializerBase):
    """for representing the result of a state change"""

    mode: SystemMode
    report: List[str]
    # the timestamp (as of int(time.time())) of this status
    date: int

    def __init__(
        self,
        mode: SystemMode = SystemMode.UNKNOWN,
        report: Optional[List[str]] = None,
        date: int = 0,
    ):
        if report is None:
            report = []
        self.mode = mode
        self.report = report
        self.date = date

    def append(self, line: str) -> None:
        self.report.append(line)

    def extend(self, extend_list: List[str]) -> None:
        self.report.extend(extend_list)


class SysStateModel(SerializerBase):
    """
    outputs sysstate
    """

    sysstate: SystemStatus
    actmode: SystemMode
    storedmode: SystemMode

    def __init__(
        self,
        sysstate: SystemStatus = SystemStatus.NOTHING,
        actmode: SystemMode = SystemMode.UNKNOWN,
        storedmode: SystemMode = SystemMode.UNKNOWN,
    ):
        """
        :type sysstate: SystemStatus
        :type actmode: SystemMode the mode the system is actually running in
        :type storedmode: SystemMode the mode the system was running in before the last shutdown
        """
        self.sysstate = sysstate
        self.actmode = actmode
        self.storedmode = storedmode


class SysModeModel(SerializerBase):
    """only the system mode - for writing"""

    mode: SystemMode

    def __init__(self, mode: SystemMode = SystemMode.STARTUP):
        """
        :type mode: SystemMode
        """
        self.mode = mode


class SysStateTransportModel(SerializerBase):
    """
    Representing the current state as well as usable for setting it via the UI (meant for serialization)
    """

    _status: SysStateModel
    _state_desired: Optional[SystemMode]
    _config_backup_encoded: Optional[str]
    _config_backup_name: Optional[str]
    _config_backup_password: Optional[str]
    _switch_action: Optional[SwitchAction]

    def __init__(self):
        self._status = SysStateModel()
        self._state_desired = None
        self._config_backup_encoded = None
        self._config_backup_name = None
        self._config_backup_password = None
        self._switch_action = SwitchAction.DEFAULT

    @property
    def status(self) -> SysStateModel:
        return self._status

    @status.setter
    def status(self, value: SysStateModel) -> None:
        self._status = value

    @property
    def state_desired(self) -> Optional[SystemMode]:
        return self._state_desired

    @state_desired.setter
    def state_desired(self, value: Optional[SystemMode]) -> None:
        self._state_desired = value

    @property
    def config_backup_encoded(self) -> Optional[str]:
        return self._config_backup_encoded

    @config_backup_encoded.setter
    def config_backup_encoded(self, value: Optional[str]) -> None:
        self._config_backup_encoded = value

    @property
    def config_backup_name(self) -> Optional[str]:
        return self._config_backup_name

    @config_backup_name.setter
    def config_backup_name(self, value: Optional[str]) -> None:
        self._config_backup_name = value

    @property
    def config_backup_password(self) -> Optional[str]:
        return self._config_backup_password

    @config_backup_password.setter
    def config_backup_password(self, value: Optional[str]) -> None:
        self._config_backup_password = value

    @property
    def switch_action(self) -> Optional[SwitchAction]:
        return self._switch_action

    @switch_action.setter
    def switch_action(self, value: Optional[SwitchAction]) -> None:
        self._switch_action = value
