"""
This is the model for the remote host, the one to which the local data will be synced
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from sosmodel.configbasemodel import ConfigBaseModel
from sosmodel.serializerbase import SerializerBase
from sosmodel.sos_enums import SsdAction


class PartitioningData(SerializerBase):
    """Holding data around partitioning of the sos vg"""

    free: int
    local: int
    remote: int
    local_min: int
    remote_min: int

    def __init__(self):
        self.free = 0
        self.local = 0
        self.remote = 0
        self.local_min = 0
        self.remote_min = 0


class SystemSetupDataModel(ConfigBaseModel):
    """
    The objects necessary to exchange setup information between alice and bob
    There are no setters, as all the parameters come from internal providers
    """

    # action
    action: SsdAction
    pub_key: str
    hostname: str
    partition_data: PartitioningData
    # uuid of sos vg
    vg_sos_uuid: str

    def __init__(self):
        super().__init__()
        self.configname = "systemsetupdata"
        self.action = SsdAction.LEADER_SEND_KEY
        self.pub_key = ""
        self.hostname = "syncosync"
        self.partition_data: PartitioningData = PartitioningData()
        self.vg_sos_uuid = ""
