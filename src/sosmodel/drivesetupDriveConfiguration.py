from typing import Optional

from sosmodel.serializerbase import SerializerBase


class DrivesetupDriveConfiguration(SerializerBase):
    # The selected_vg_uuid can be None/null to erase all drives and set up a fresh volume group (VG).
    # Otherwise, the selected vg-uuid (if and only if the VG is present entirely) will be expanded by all drives
    # attached that are not part of the selected VG
    _selected_vg_uuid: Optional[str]

    # In case a "foreign" vg-uuid is selected, the password of the configuration backup on that vg is needed
    # in order to restore the configuration/system
    _password_confirmation: Optional[str]

    def __init__(self):
        super().__init__()
        self._selected_vg_uuid = None
        self._password_confirmation = None

    @property
    def selected_vg_uuid(self) -> Optional[str]:
        return self._selected_vg_uuid

    @selected_vg_uuid.setter
    def selected_vg_uuid(self, value: Optional[str]):
        self._selected_vg_uuid = value

    @property
    def password_confirmation(self) -> Optional[str]:
        return self._password_confirmation

    @password_confirmation.setter
    def password_confirmation(self, value: Optional[str]):
        self._password_confirmation = value
