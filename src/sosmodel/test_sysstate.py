import unittest

from sosmodel import sysstate
from sosmodel.sos_enums import SystemMode


class MyTestCase(unittest.TestCase):
    def test_intial_set(self):
        mymodel = sysstate.SysStateResult(SystemMode.DEFAULT, ["1st line"])
        self.assertEqual(mymodel.mode, SystemMode.DEFAULT, "test good")
        self.assertEqual(mymodel.report, ["1st line"], "test good")

    def test_append_result(self):
        mymodel = sysstate.SysStateResult(SystemMode.DEFAULT, ["1st line"])
        mymodel.append("2nd line")
        self.assertEqual(mymodel.report, ["1st line", "2nd line"], "test good")

    def test_extend_result(self):
        mymodel = sysstate.SysStateResult(SystemMode.DEFAULT, ["1st line"])
        mymodel.extend(["2nd line", "3rd line"])
        self.assertEqual(
            mymodel.report, ["1st line", "2nd line", "3rd line"], "test good"
        )


if __name__ == "__main__":
    unittest.main()
