"""
Model for syncosync Housekeeper
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import json
import logging
import os
from typing import List

from sosmodel import sosconstants
from sosmodel.serializerbase import SerializerBase

my_logging = logging.getLogger(__name__)


class MailModel(SerializerBase):
    """
    A mail for an account
    """

    name: str
    address: str
    lines: List[str]
    attachments: List[str]

    def __init__(self):
        self.name = ""
        self.address = ""
        self.lines = []
        # paths to attachments
        self.attachments = []

    def add_line(self, line):
        self.lines.append(line)

    def add_attachment(self, line):
        self.attachments.append(line)


class HousekeeperModel:
    """Housekeeper model class to handle housekeeper tasks"""

    def __init__(self):
        # If housekeeper file does not exist create it
        if not os.path.isfile(sosconstants.SOS_HK_FILE):
            with open(sosconstants.SOS_HK_FILE, "w") as file:
                data = {
                    "admin_last_mail": 0,
                    "admin_last_rmdown_mail": 0,
                    "admin_mail_content": "",
                }
                json.dump(data, file)
            my_logging.log(
                logging.INFO,
                "The housekeeper json file has been created in "
                + sosconstants.SOS_HK_FILE,
            )

        # If last mail file doesnt exist create it
        if not os.path.isfile(sosconstants.LAST_MAIL_CACHE):
            with open(sosconstants.LAST_MAIL_CACHE, "w") as file:
                data = {}
                json.dump(data, file)
            my_logging.log(
                logging.INFO,
                "The last mail json file has been created in "
                + sosconstants.LAST_MAIL_CACHE,
            )

        self.__admin_last_mail = self.get_entry(
            sosconstants.SOS_HK_FILE, "admin_last_mail"
        )
        self.__admin_mail_content = self.get_entry(
            sosconstants.SOS_HK_FILE, "admin_mail_content"
        )
        self.__admin_last_rmdown_mail = self.get_entry(
            sosconstants.SOS_HK_FILE, "admin_last_rmdown_mail"
        )

    def update(self):
        """Updates own values from json file"""

        self.__admin_last_mail = self.get_entry(
            sosconstants.SOS_HK_FILE, "admin_last_mail"
        )
        self.__admin_mail_content = self.get_entry(
            sosconstants.SOS_HK_FILE, "admin_mail_content"
        )
        self.__admin_last_rmdown_mail = self.get_entry(
            sosconstants.SOS_HK_FILE, "admin_last_rmdown_mail"
        )

    @property
    def admin_last_rmdown_mail(self):
        return self.__admin_last_rmdown_mail

    @admin_last_rmdown_mail.setter
    def admin_last_rmdown_mail(self, value):
        """Sets the time for the last mail to an admin due to a down remote host was sent
        :param value: time mail was sent
        :type: float"""
        if self.__admin_last_rmdown_mail < value:
            self.set_entry(sosconstants.SOS_HK_FILE, "admin_last_rmdown_mail", value)
            self.__admin_last_rmdown_mail = value
        else:
            my_logging.log(
                logging.WARNING,
                "There was a problem setting the admin last rmdwon mail value"
                " (new value is older than the old one",
            )

    @property
    def admin_last_mail(self):
        return self.__admin_last_mail

    @admin_last_mail.setter
    def admin_last_mail(self, value):
        """Set the time of the last mail to the admin
        :param value: time the last mail was sent
        :type: float"""
        if self.admin_last_mail < value:
            self.set_entry(sosconstants.SOS_HK_FILE, "admin_last_mail", value)
            self.__admin_last_mail = value
        else:
            my_logging.log(
                logging.WARNING,
                "There was a problem setting the admin last mail value"
                " (new value is older than the old one",
            )

    @property
    def admin_mail_content(self):
        return self.__admin_mail_content

    @admin_mail_content.setter
    def admin_mail_content(self, value):
        """Sets the content of the current mail to the admin and saves it to the file

        :param value: content of the mail
        :type value: str"""
        self.set_entry(sosconstants.SOS_HK_FILE, "admin_mail_content", value)
        self.__admin_mail_content = value

    def admin_mail_append(self, value):
        """Appends given value to the currently existing admin mail

        :param value: additional content for the mail
        :type value: str"""
        # <p> to make a paragraph in the html mail
        self.__admin_mail_content += "<p>" + value + "</p>"
        self.set_entry(
            sosconstants.SOS_HK_FILE, "admin_mail_content", self.__admin_mail_content
        )

    def __str__(self):
        return 'HousekeeperModel(admin_mail_content:"{}", admin_last_mail:"{}", remotehost_down_since: {})'.format(
            self.admin_mail_content, self.admin_last_mail, self.remotehost_down_since
        )

    @staticmethod
    def set_entry(path_to_file, key, value):
        """Sets the last mail of a user

        :param path_to_file: path to the json file from which is loaded
        :type path_to_file: str
        :param key: json key
        :type key: str
        :param value: value the entry is set to
        :type value: float
        """
        with open(path_to_file, "r+") as file:
            data = json.load(file)
            data[key] = value
            file.seek(0)
            json.dump(data, file)
            file.truncate()

    @staticmethod
    def get_entry(path_to_file, key):
        """Loads a dictionary containing key value pairs.

        :param path_to_file: path to the json file from which is loaded
        :type path_to_file: str
        :param key: json key
        :type key: str
        :return: Dictionary with key value pairs
        :rtype: dict
        """
        with open(path_to_file, "r+") as file:
            data = json.load(file)[key]
        return data

    @staticmethod
    def add_entry(path_to_file, key, value):
        """Add entry to json file

        :param path_to_file: path to the json file from which is loaded
        :type path_to_file: str
        :param key: json key
        :type key: str
        :param value: value which is added
        :type: float
        :return: None
        """
        with open(path_to_file, "a+") as file:
            data = json.load(file)
            data.update({key: value})
            file.seek(0)
            json.dump(data, file)
            file.truncate()


if __name__ == "__main__":
    logging.log(
        logging.INFO,
        "Housekeeper module was called as main, please use the according script to call it!",
    )
