"""
This is the model for the syncosync configuration
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2021  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from typing import Optional

from sosmodel.configbasemodel import ConfigBaseModel
from sosmodel.sos_enums import SysType
from sosmodel.uilanguage import UiLanguage


class SosConfigModel(ConfigBaseModel):
    """
    Represents the syncosync config
    """

    systype: SysType
    upnp: bool
    ui_language: UiLanguage
    local: Optional[int]
    remote: Optional[int]
    vg_sos_uuid: str

    def __init__(self):
        super().__init__()
        self.configname = "sosconfig"
        # systype is a string defining what system type to use for this configuration. This should _not_ be
        # changeable via the UI as the systype is hardware dependent
        self.systype = SysType.UNKNOWN
        # a boolean, defining if sos should try to request to the gateway to open the incoming port for
        # syncing via upnp
        self.upnp = False
        # two letter synonym for the ui language - does override browser language - if empty, browser language is
        # used
        self.ui_language = UiLanguage()
        # local drive size
        self.local = None
        # remote drive size
        self.remote = None
        # uuid of sos vg
        self.vg_sos_uuid = ""
