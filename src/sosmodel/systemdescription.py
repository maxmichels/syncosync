"""
This is the model for the syncosync configuration
"""
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2022  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from typing import List

from sosmodel.configbasemodel import ConfigBaseModel
from sosmodel.sos_enums import SysType


class SystemDescriptionModel(ConfigBaseModel):
    systype: SysType
    nics: List[str]
    drives: List[str]

    """
    The system description, which holds static data how some hw ist related to the sysncosync setup
    """

    def __init__(self):
        super().__init__()
        self.configname = "systemdescription"
        # systype is a string defining what system type to use for this configuration. This should _not_ be
        # changeable via the UI as the systype is hardware dependent
        self.systype = SysType.GENERIC
        # array of network interface names for systype configuration - these are the all the allowed nics,
        # they do not necessarily have to be used
        # Note: this array can't be changed by the setter
        self.nics = []
        # array of network drives from systype configuration - these are the all the allowed drives,
        # they do not necessarily have to be used
        # Note: this array can't be changed by the setter!
        self.drives = []
