# Development

We use pycharm for development, but you could use any means you like.
As the philosophy of syncosync is, to use as many Linux features as possible,
testing on your local machine is possible but needs some pre-requirements
and can change some system settings, which you do not like.

So for full functional testing it is easier to use virtual machines.
The setup of those is easy an documented. Have a look at  
[amd_installer_emulation](amd_installer_emulation)

You can start the local scripts (src/soscore/scripts) with your own
user id, but you will receive a lot of exceptions as many of the needed changes
to the local Linux environment need root status.

No comes the list, of what is accessed and needs root or other rights:

- **/etc/passwd** - is used to add, modify and delete sos users. As long as
  you do not have a sosusr group aside from the syncosync idea, it is save to
  use it. Other users won't/can't be overwritten, deleted or modified. root
  privilege is needed.

- **/etc/group** - will get only one more entry: sosusr. root privilege
  is needed

- **/etc/syncosync** holds additional sos configuration stuff. You can
  either add files here manually or change the access to be open for your user

- **/etc/lsyncd/lsyncd.conf.lua** is the lsyncd configuration

- **/mnt/local** This directory is used for the user data so, basically
  it should be save to change it to be accessible by yourself. But as this
  is a combined with adding/deleting users, root access is more helpful
  for sure.

- **/usr/share/syncosync** holds configs and system descriptions. Copy those
  both folders from package-static into that location.

## Demo Mode

Mainly for easier development of the ui, and demonstration purposes there is
a demo mode, which is activated by **setting an empty file
/etc/syncosync/demo**. Take also care to **unpack /usr/share/syncosync/demodata.tar.gz** so that
a folder /usr/share/syncosync/demodata with many files exists afterwards.
This will provide demodata for accounts, syncstat settings etc.
Still the /etc/syncosync structure should exist.

## Environment

The document root for pycharm should be set to the src directory.
To have the right path to the modules while testing from bash, cd to src
and set the PYTHON_PATH:

    export PYTHONPATH=$PWD

If you decide to run SOS in PyCharm and intend to debug "normal" operations, root is required.
In order to not screw with permissions of PyCharm configurations etc, you should look into https://esmithy.net/2015/05/05/rundebug-as-root-in-pycharm/
TL;DR:

```
sudo visudo -f /etc/sudoers.d/python
insert >>>>>>> <user> <host> = (root) NOPASSWD: <full path to python>
nano <CWD>/venv/bin/python-sudo.sh
insert >>>>>>>
#!/bin/bash
sudo <CWD>/venv/bin/python "$@"
<<<<<<<<
chmod +x <CWD>/venv/bin/python-sudo.sh
```

Then just refer PyCharm to use python-sudo.sh as your interpreter (rather than the python-binary itself)

ſ

## Git hooks

Follow setup at https://pre-commit.com/
TL;DR:

```
pip install pre-commit
pre-commit install
```

Then restart IDE if applicable

## Packaging

### Building the debian package

From clean ubuntu 20.04 or 20.10 or debian buster install:

sudo apt install -y npm git parted libparted-dev reprepro rsync devscripts git-buildpackage dh-python python3-all build-essential python3-pip debhelper
sudo npm install -g npm@latest
sudo hash -r
sudo npm install -g @angular/cli
sudo pip3 install --upgrade gnupg flask pyparted paramiko dirsync
hash -r
git clone https://gitlab.com/syncosync/syncosync.git
cd syncosync
./build_deb.sh

### Testing

Use the installation steps from sos-qemu-runner-docker
cd amd_installer_emulation
./full_test.sh
