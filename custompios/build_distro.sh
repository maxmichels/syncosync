#/bin/bash -x -e
# Usage info


VARIANTS="raspios_lite_arm64 armbian-bananapi-m1"

SCRIPT=$(readlink -f $0)
# Absolute path this script is in. /home/user/bin
SCRIPTPATH=$(dirname $SCRIPT)
cd $SCRIPTPATH

show_help() {
  cat <<EOF
Usage: ${0##*/}
build a syncosync image variant

    -v          variants (default = $variant)

    possible variants:

    raspios_lite_arm64
    armbian-bananapi-m1
EOF
}

OPTIND=1 # Reset is necessary if getopts was used previously in the script.  It is a good idea to make this local in a function.
while getopts "v:h" opt; do
  case "$opt" in
  v) VARIANTS=$OPTARG;;
  h)
    show_help
    exit 0
    ;;
  '?')
    show_help >&2
    exit 1
    ;;
  esac
done


# clean up before running
sudo rm -r CustomPiOS
sudo rm Packages

# now check if there is a version which is newer

wget -c -nd http://deb.syncosync.org/dists/bullseye/main/binary-armhf/Packages

VERSION=$(grep -A1 "Package: syncosync$" Packages | tail -1 | sed -n "s/^Version: \(.*\)$/\1/p")

if [ -f old_version ]; then
  OLD_VERSION=`cat old_version`
else
  OLD_VERSION="0"
fi

if [ $VERSION != $OLD_VERSION ] 
  then  
    echo New Version: $VERSION, will build distributions
  else
    echo No new Version, will exit build
    exit 1
fi

if [ ! -f /etc/mtab ]; then ln -s /proc/self/mounts /etc/mtab; else echo "mtab present"; fi
this_pwd=`pwd`
echo "We are in $this_pwd, building variant $VARIANT and version $VERSION"

if [ ! -d CustomPiOS ]; then
  # to have the possibility for debugging, we use a fork from the original project
  git clone https://github.com/steviehs/CustomPiOS.git
fi

cd CustomPiOS
src/make_custom_pi_os syncosync

for VARIANT in $VARIANTS ; do
  cd $this_pwd
  cd CustomPiOS
  if [ ! -d "syncosync/src/image-$VARIANT" ] 
    then
      mkdir "syncosync/src/image-$VARIANT"
    fi

  if [ $VARIANT = "raspios_lite_arm64" ]; then
      echo "Downloading latest Raspbian 64bit image"
      
      CURRENT_RASPBIAN=$(curl https://downloads.raspberrypi.org/raspios_lite_arm64/images/ | grep raspios | tail -n 1 | awk -F "href=\"" '{print $2}' | awk -F "/" '{print $1}')
      if [ $? -ne 0 ]; then
          echo "error getting date"
          exit 0
      fi
      CURRENT_RASPBIAN_FILE=$(curl http://downloads.raspberrypi.org/raspios_lite_arm64/images/${CURRENT_RASPBIAN}/ | grep ".xz\|.zip" | head -n 1 | awk -F "href=\"" '{print $2}' | awk -F "\">" '{print $1}')
      curl -L -o "syncosync/src/image-raspios_lite_arm64/${CURRENT_RASPBIAN_FILE}" https://downloads.raspberrypi.org/raspios_lite_arm64/images/${CURRENT_RASPBIAN}/${CURRENT_RASPBIAN_FILE}
  fi

  if [ $VARIANT = "armbian-bananapi-m1" ]; then
      # echo "Copying Variant bananapi-m1-armbian"
      # cp -r ../bananapi-m1-armbian syncosync/src/variants/
      if [ ! -f syncosync/src/image-armbian-bananapi-m1/bullseye-bananapi-m1-armbian.img ] ; then
        echo "Downloading latest BananaPi M1 Armbian image"
        curl -L -o "syncosync/src/image-armbian-bananapi-m1/bullseye-bananapi-m1-armbian.img.xz" https://redirect.armbian.com/bananapi/Bullseye_current
        pwd
        cd syncosync/src/image-armbian-bananapi-m1/
        # xz -d bullseye-bananapi-m1-armbian.img.xz
        # zip -1 bullseye-bananapi-m1-armbian.zip bullseye-bananapi-m1-armbian.img
        cd ../../../
      fi
  fi

  cp ../start_chroot_script-${VARIANT} syncosync/src/modules/syncosync/start_chroot_script
  cp ../end_chroot_script-${VARIANT} syncosync/src/modules/syncosync/end_chroot_script
  cd syncosync/src
  sudo bash -x ./build_dist $VARIANT
  result_code=$?
  echo Result Code: $result_code
  if [ $result_code -ne 0 ]; then
    echo build_dist was not successful! 
    exit 1
  fi
  cd workspace-$VARIANT
  sudo mv *.img syncosync_${VARIANT}_${VERSION}.img
  sudo zip syncosync_${VARIANT}_${VERSION}.zip syncosync_${VARIANT}_${VERSION}.img
  # take care, that the pub key is on servers site and host key is already accepted (or however :-)
  # hint: add an entry to .ssh/config
  for i in `ssh hosting102239@hosting102239.af9b8.netcup.net ls -t1 /httpdocs/downloads.syncosync.org/syncosync_${VARIANT}_*.zip | tail --lines=+5` ; do ssh hosting102239@hosting102239.af9b8.netcup.net rm $i ; done
  scp syncosync_${VARIANT}_${VERSION}.zip hosting102239@hosting102239.af9b8.netcup.net:/httpdocs/downloads.syncosync.org/
  ssh  hosting102239@hosting102239.af9b8.netcup.net "rm /httpdocs/downloads.syncosync.org/syncosync_${VARIANT}_latest.zip"
  ssh  hosting102239@hosting102239.af9b8.netcup.net "cd /httpdocs/downloads.syncosync.org/ ; ln -s syncosync_${VARIANT}_${VERSION}.zip syncosync_${VARIANT}_latest.zip"
done

cd $this_pwd

# clean this also after running
sudo rm -r CustomPiOS
sudo rm Packages

echo -n $VERSION > old_version
