#!/bin/bash
if [ ! -f  /etc/ssl/private/syncosync-selfsigned.key ]; then
    echo "Generate selfsigned ssl key"
    openssl req -x509 -nodes -days 36500 -newkey rsa:2048 -keyout /etc/ssl/private/syncosync-selfsigned.key -out /etc/ssl/certs/syncosync-selfsigned.crt -subj "/C=DE/ST=Berlin/L=Berlin/O=syncosync/OU=Org/CN=www.syncosync.org"
    service nginx restart || true 
fi
