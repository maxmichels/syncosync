#!/bin/bash
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

set -x
set -e

SCRIPT=$(readlink -f $0)
# Absolute path this script is in. /home/user/bin
SCRIPTPATH=$(dirname $SCRIPT)
echo $SCRIPTPATH
cd $SCRIPTPATH

cd debian/syncosync-manuals/usr/share
lftp -u "sosmanuals,$SOSMANUAL_PASSWD" -e "set ftp:ssl-allow no; set ssl:verify-certificate false; mirror -R syncosync-manuals .; quit" ftp://af9b8.netcup.net
