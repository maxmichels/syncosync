#!/bin/bash
#
# syncosync - secure peer to peer backup synchronization
# Copyright (C) 2020  syncosync.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Absolute path to this script. /home/user/bin/foo.sh
SCRIPT=$(readlink -f $0)
# Absolute path this script is in. /home/user/bin
SCRIPTPATH=$(dirname $SCRIPT)
echo $SCRIPTPATH
cd $SCRIPTPATH

PROJECT="syncosync-manuals"

echo $PWD

VERSION=$(<../version)

echo Version is: $VERSION

echo "processing changelog"
if [ -f debian/changelog ]
then
  rm debian/changelog
fi
dch --create --newversion="$VERSION" --package="$PROJECT" --empty -M "For additional changelog refer to the syncosync package" || exit 1
gbp dch --ignore-branch

echo removing previous builds from here
rm ../${PROJECT}_*_all.deb
rm ${PROJECT}_*_all.deb

# ok. let's do the real stuff
echo building debian package $PROJECT\_$VERSION-$\_all

debuild -i -us -uc -b || exit 1

cp ../${PROJECT}_${VERSION}_all.deb .


