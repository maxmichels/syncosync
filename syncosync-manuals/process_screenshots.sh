#!/bin/bash -e
for j in en-US
do
    pushd $j
    pushd images/screenshots/
    mogrify -define trim:edges=east,south,west -trim -border 20 -bordercolor White *.png
    popd
    popd
done