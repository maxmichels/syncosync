:experimental: true
:toc: left
:icons: font
:icon-set: fa
:doctype: book
:sectnums:
:!chapter-signifier:
:chapter-label:
:imagesdir: ../images/


Besides and including the document you are reading, the following manuals are available:

* link:../syncosync_introduction/syncosync_introduction.html[syncosync introduction] (link:../syncosync_introduction/syncosync_introduction.pdf[as pdf]) understand the principles of syncosync

* link:../syncosync_installation_manual/syncosync_installation_manual.html[syncosync_installation_manual] (link:../syncosync_installation_manual/syncosync_installation_manual.pdf[as pdf]) How to install syncosync

* link:../syncosync_administrator_manual/syncosync_administrator_manual.html[syncosync_administrator_manual] (link:../syncosync_administrator_manual/syncosync_administrator_manual.pdf[as pdf]) How to use syncosync's WebUI

* link:../syncosync_backup_clients/syncosync_backup_clients.html[syncosync_backup_clients] (link:../syncosync_backup_clients/syncosync_backup_clients.pdf[as pdf]) Selection and configuration of backup clients

* link:../syncosync_q_and_a/syncosync_q_and_a.html[syncosync_q_and_a] (link:../syncosync_q_and_a/syncosync_q_and_a.pdf[as pdf]) Questions and Answers

* link:../syncosync_cli_commands/syncosync_cli_commands.html[syncosync_cli_commands] (link:../syncosync_cli_commands/syncosync_cli_commands.pdf[as pdf]) CLI commands (FOR EXPERTS ONLY!!!)
