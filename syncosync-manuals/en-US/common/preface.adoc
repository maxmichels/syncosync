:experimental: true
:toc: left
:icons: font
:icon-set: fa
:doctype: book
:sectnums:
:!chapter-signifier:
:chapter-label:
:imagesdir: ../images/


include::available_manuals.adoc[]
