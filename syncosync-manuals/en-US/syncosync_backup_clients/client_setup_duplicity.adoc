:icons: font
:icon-set: fa
:doctype: book
:sectnums:
:!chapter-signifier:
:chapter-label:
:imagesdir: ../images/

==== Duplicity setup

Duplicity (https://duplicity.gitlab.io/duplicity-web/) is a Free and Open Source command line based backup program for Linux. It works perfectly with syncosync.

For setting up Duplicity refer to it's manual. The important setting to operate with syncosync is the backend URL. The protocol to choose should be rsync.

So, an example URL should look like:

`duplicity collection-status rsync://backup_account@syncosync//data/`

where `backup_account` is the name of the account the backup should use on the syncosync box and `syncosync` the hostname of the syncosync box.

