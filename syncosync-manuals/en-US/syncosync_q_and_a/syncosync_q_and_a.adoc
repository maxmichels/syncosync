:experimental: true
:toc: left
:icons: font
:icon-set: fa
:doctype: book
:sectnums:
:!chapter-signifier:
:chapter-label:
:imagesdir: ../images/
:common: ../common/

ifeval::["{backend}" == "html5"]
image::logos/logo.png[syncosync Logo,300,align="center"]
endif::[]

= Questions and Answers
Version: {VERSION}, Date: {docdate}

include::{common}preface.adoc[]

== General Questions

[qanda]

What is syncosync?::

syncosync or short name sos is not a backup software but a small network attached storage box, which hosts your backups and synchronizes them over the internet with a same system hosted by a friend of you, your syncosync buddy.

What is the main idea?::

Have a local and remote backup of all of your systems at the same time for reasonable costs and have *really* fast access to your remote backup in case of a disaster.

What is a disaster?::

A disaster is a situation, where you lose all your local data. Fire, theft, water, storm... whatever. Imagine the only thing which is left is your pyjama...

What do I do in the disaster case?::

If you do not need your backuped data, stay cool and buy some new cloth. If you need your data, go to your friend,
ask him for money for a new laptop, grab his syncosync box and recover your data from it.


Isn't it easier to backup to a cloud provider?::

Maybe easier, but more expensive and slower to recover. First it is important to understand, that doing a backup *only* to the cloud may not be a good idea: what do you do, if you need to restore and internet is down? How fast will a full restore be? Next, do a calculation how much a terabyte of storage will cost at your cloud provider and compare this to the cost of your harddisks + the pi + the power consumption, you will find out, that your break even will be reached at the end of the first year...

I am a normal day to day PC user, is syncosync something for me?::

It is especially for you! We tried to make installation and administration as easy as possible. And really *everybody* should do backups regulary! So, go ahead, find a syncosync partner and give it a try. We will help you to set everything up e.g. in syncsync's forum: https://forum.syncosync.org


== Prerequisites

[qanda]

Would other single board computers (SBC) also work?::

Basically yes. syncosync is just a software package. But right now we only provide a ready to install image for the Raspberry Pi 4.
Raspberry Pi 3 is not really recommended as it has only very slow USB2 support for HDD drives.

How is syncosync connected to the internet?::

Right now tested and supported is IPv4 access. You have to set up DynDNS and port forwarding (default TCP 55055) on your Internet router to enable your syncosync buddies box to reach yours.

Does syncosync work without a public IP address?::

Not at the point now. You do *not* need a static IP address, but then you need to set up DynDNS.

I don't want to open a port on my router?::

Sorry :-) There could be changes in the future to use alternatives. But the alternatives out there are not as reliable and reliability is first need for syncosync.

== Installation

[qanda]

Is it complicated to install?::

Hmm... that really depends on your technical mileage. If you are able to setup your DSL router at home, you could also most probably install syncosync. If not, maybe ask a friend, or ask us for preconfigured systems.

How to update a running system?::

The system updates it's syncosync packages automatically. In rare cases it might be necessary to update the complete image, just by flashing the SD card with it. 

But does reflashing the SD card not mean, that everyting get's lost?::

No. All configuration is backed up on the harddisk. So, even if you set up a complete new Raspberry Pi: after the first start you will be asked if you would like to use the configuration from the harddisk.

Which ports do I have to forward?::

The syncosync system connects to it's buddy system per default via port 55055. This port you have to forward. For the internal backup clients (your PCs), port 22 is used. You do not need to forward this port - still it is ssh and secure, so you could do if you like.

== Operation

[qanda]

Which passwords do I need in disaster case?::

The sosadmin password and the encryption password for your backups. So keep them in your mind. *Always!*

Can I run syncosync as an additional service on my server, NAS, PC etc.?::

Basically yes, *but*: One of the core ideas of syncosync is, that you can hand over your syncosync box and HDD to your buddy, when es is in the need for it (-> Disaster Case). Would you do that with your server, NAS, PC?
Also be aware, that syncosync would take over some administration parts of your server, e.g. how and when to start the ssh deamon. It does not do any harm, but you have to be aware for that.

Can I backup also my mobile devices?::

This is a sad chapter. These days, mobile devices do not provide access to all data stored on them, therefore no real and easy full backup solutions exist. Some manufacturers offer companion applications you can use to backup your phone to your PC and from there backup the usual way. Still not charming. Maybe the most important thing is to backup and sync your pictures ... always. Take a look at nextcloud for that. And then use syncosync to backup your nextcloud server.

Can I use software ABC for doing backups to my syncosync box?::

The backup software should support encryption and backup to remote systems via ssh but also the blocks used for backups should be small enough to be synced easily between two syncosync boxes. In fact there are not so many solutions out there, but take a look at the syncosync_backup_clients document.

Can I mount the account space via SMB?::

No. Backups are a very important measure against ransomware attacks. If you would mount your backup destination, it would be really easy for the virus to attack also this drive and encrypt it. 
Thus, if you use a backup software which needs SMB, go ahead and ask the software developer to add ssh/rsync support as it is much more secure.