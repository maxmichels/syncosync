:experimental: true
:icons: font
:icon-set: fa
:doctype: book
:sectnums:
:!chapter-signifier:
:chapter-label:
:imagesdir: ../images/

==== syncosync Admin Password

This is now the time to set a new admin password. Staying with the default password is not a good idea as this password is also used for encrypting the configuration backups and for accessing the syncosync box for disaster swap. So choose wisely a password which is easy enough to remember in case you lost everything else!

image::screenshots/esa_admin_password.png[ESA syncosync Admin Password]

include::admin_password.adoc[]

