:experimental: true
:toc: left
:icons: font
:icon-set: fa
:doctype: book
:sectnums:
:!chapter-signifier:
:chapter-label:
:imagesdir: ../images/
:common: ../common/

ifeval::["{backend}" == "html5"]
image::logos/logo.png[syncosync Logo,300,align="center"]
endif::[]

= Installation Manual                              
Version: {VERSION}, Date: {docdate}

include::{common}preface.adoc[]

== Introduction

You are reading the syncosync Installation Manual. 
This manual is for installation of syncosync on a RaspberryPi 4 and the setup of your infrastructure to use syncosync. After installation you should read the syncosync Administration Manual and the syncosync Client Setup Manual. 


include::hardware_components.adoc[]

include::micro_sd_preparation.adoc[]

include::hardware_setup.adoc[]

include::infrastructure_setup.adoc[]

