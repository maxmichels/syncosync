== Setting up the hardware

* Insert the flashed micro SD card into the RaspberryPi

* Connect the USB3 hard disk drive to the RaspberryPi. Be sure to use a USB3 port on the Pi - the blue connector.

* Connect the network cable between a port on your router and the RaspberryPi

* (if necessary) Switch on the hard disk drive

* Connect the power supply to the RaspberryPi

When you see activity lights at the network connector, this means, that the network went physically up. This is a good sign.

